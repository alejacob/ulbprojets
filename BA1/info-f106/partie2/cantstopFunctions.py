# !/usr/bin/python3
# -*- coding: utf-8 -*-

"""
INFO-F106
Projet d'année BA1: Can't Stop
Partie 2
Auteur : JACOBS ALEXANDRE
Matricule : 000408850
Date : 16/12/2016
"""

from random import randint, random
import os, time

HEIGHT = dict()
P = 0
COLOR_PLAYER = ((31, "rouge"), (32, "vert"), (33, "jaune"), (34, "bleu"))  # couleur représentant les joueurs
SYMBOL_PAWNS = "\u2659"  # caractère unicode réprésentant un pion.
SYMBOL_BONZES = "\u24B7" # caractère représentant les bonzes

def init(number_player):
    """
    Fonction initialisant des variables(HEIGHT, p) et les déclarant comme des variables globales.
    HEIGHT : est un dictionnaire qui contient la hauteur maximale des voies en valeurs et le numéro de la voie en clé.
    P: variable contenant la probabilité p de l'IA qui sert à prendre une décision lors de l'arrêt de son tour.
    bonzes: est un dictionnaire qui contient au maximun 3 clés (qui représente les 3 bonzes qu'un joueur a),
    les clées représentent le numéro de voie et la valeur la hauteur à laquel le bonze se trouve sur la voie.
    pawns: idem que bonzes, sauf que celui-ci représent les pion du joueur.
    blocked_routes: est un set qui indique si une voie est fermé, c'est à dire si la voie est occupé
    par un bonze qui est arrivé au sommet de la voie, sinon celle-ci est libre si la voie n'est pas dans le set.
    Cette fonction renvoie les variables locales: pawns, bonzes, players_type, blocked_routes
    """
    global HEIGHT, P  # global permet de changer la valeur d'une variable à n'importe quel niveau dans le code.
    HEIGHT = {2: 3, 3: 5, 4: 7, 5: 9, 6: 11, 7: 13, 8: 11, 9: 9, 10: 7, 11: 5, 12: 3}
    P = 0.55
    pawns = [{} for i in range(number_player)]
    bonzes = dict()
    blocked_routes = set()
    return bonzes, pawns, blocked_routes

def setup_players():
    """
    Fonction permettant de choisir le nombre de joueur et le type de ceux-ci(humain ou IA). 
    Le résultat de cet fonction sera stocké dans une liste de bool.
    La i-ème valeur vaudra True si l’i-ème joueur est contrôlé par l’ordinateur, 
    False s’il est géré par un joueur humain. 
    Le nombre de joueurs sera donc donné par la taille de cette liste.
    Renvoie la liste de type de joueur(players_type)
    """
    number_player = ask_int_between_bounds(prompt="Veuillez entrer un nombre de joueurs entre 2 et 4 joueurs possible: ",
                                           low=2, high=4)
    players_type = list()
    for player_id in range(number_player):
        answer = ask_int_between_bounds(prompt="""Quel type de joueur pour le joueur {}
1) Humain
2) IA
""".format(player_id+1), low=1, high=2)
        players_type.append(answer == 2)
    return players_type


def ask_int_between_bounds(low, high, prompt=""):
    """
    Fonction permettant de vérifier si l'entrée de l'utilisateur correspond bien à un entier
    et qu'il respecte les consignes d'encodage donné lors de la demande d'encodage à l'utilisateur.
    :param prompt: contient le string d'encodage qui est fournit à l'utilisateur lors de la demande d'encodage.
    :param low: borne  basse de l'entier à encoder
    :param high: borne haute de l'entier à encoder
    :return: renvoie l'entier correctement entrée.
    """
    valid = False
    while not valid:
        answer = input(prompt)
        # try/ except permet de capturer une erreur si ce qu'on vérifie dans le try,
        # si cela n'est pas correct et correspond à l'erreur mentioné dans le except
        try:
            answer = int(answer)
        except ValueError:
            print("Erreur d'encodage")
        else:
            # vérifie si l'entier est entre les deux bornes.
            if not low <= answer <= high:
                print("Erreur d'encodage")
                valid = False
            else:
                valid = True
    return answer


def print_color(string, color, sep):
    """
    Fonction permetttant de faire un output en couleur.
    """
    output = colorize_string(string, color)
    print(output, end=sep)

def colorize_string(string, color):
    """
    Fonction permettant de colorier le string qui sera affiché à l'aide de la fonction print_color().
    """
    CSI = "\x1B["
    return CSI + str(color) + "m" + string + CSI + "0m"

def display_board(pawns, bonzes, player_id):
    """
    Fonction permettant l'affichage d'un plateau de jeu(board) avec les bonzes d'un joueur 
    s'il y a des bonzes sur les voies et les pions(pawns) des joueurs. 
    Cette fonction renvoie None par défaut
    """
    time.sleep(1.5)
    os.system("cls" if os.name == "nt" else "clear") # permet de rafraichir la console avant affichage du board
    max_height_way = max(HEIGHT.values())
    pawns_pos = get_pawns_pos(pawns)
    for line_number in reversed(range(1,
                                      max_height_way + 1)):  # parcourt jusqu'à 1 depuis la plus grande hauteur grâce à reversed, boucle créant l'affiche du jeu et des hauteur.
        print("{:^3}".format(line_number),
              end="")  # format permet de bien afficher sur 3 caractère centré: les chiffres,  "-" et "0", end="" permet de continuer sur la même ligne.
        for way in HEIGHT:
            if line_number > HEIGHT[way]:
                print("{:^3}".format("-"), end="")
            elif way in bonzes and bonzes[way] == line_number:
                print_color("{:^3}".format(SYMBOL_BONZES), COLOR_PLAYER[player_id][0], "")
            elif (way, line_number) in pawns_pos and pawns_pos[(way, line_number)] != player_id:
                player_id_pawn = pawns_pos[(way, line_number)]
                color = COLOR_PLAYER[player_id_pawn][0]
                print_color("{:^3}".format(SYMBOL_PAWNS), color, "")
            elif (way, line_number) in pawns_pos and way not in bonzes:
                player_id_pawn = pawns_pos[(way, line_number)]
                color = COLOR_PLAYER[player_id_pawn][0]
                print_color("{:^3}".format(SYMBOL_PAWNS), color, "")
            else:
                print("{:^3}".format(" "), end="")
        print()
    print(" " * 3, end="")  # permet un espace de 3 caractères par rapport à la colonne des hauteurs de voie.
    for way in HEIGHT:  # boucle affichant les numéros de voies.
        print("{:^3}".format(way), end="")
    print()


def get_pawns_pos(pawns):
    """
    Fonction créant un dictionnaire ayant comme clés (way, height) 
    et comme valeur l'indice du joueur à qui appartient ce pion.
    Le but de cet fonction est d'aider lors de l'affichage du plateau
    pour savoir si un pion est présent sur une voie et une hauteur donné.
    """
    pawns_pos = dict()
    for player_id, pawn in enumerate(pawns):
        for way, height in pawn.items():
            pawns_pos[(way, height)] = player_id
    return pawns_pos

def clear_bonzes(bonzes):
    """
    Fonction réinitialisant le dictionnaire bonzes à un dictionnaire vide.
    Cette fonction renvoie None par défaut.
    """
    if bonzes:
        bonzes.clear()

def save_bonzes(pawns, bonzes, player_id):
    """
    Fonction permettant de sauver la progression d'un joueur.
    """
    if bonzes:
        for route in bonzes:
            pawns[player_id][route] = bonzes[route]
        bonzes.clear()

def throw_dice():
    """
    Fonction permettant de simuler le jet de dé en créeant des nombres aléatoires
    à l'aide de la fonction randint de la librairie random
    :return: un tuple de 4 entier correspondant au jet de 4 dés.
    """
    r1 = randint(1, 6)
    r2 = randint(1, 6)
    r3 = randint(1, 6)
    r4 = randint(1, 6)
    return r1, r2, r3, r4

def choose_dice(res_dice, player_id, AI):
    """
    Fonction renvoyant sous la forme d'un tuple la somme des 4 dés pris 2 à 2.
    La variable s1 étant la somme des 2 dés choisit part l'utilisateur 
    par l'intermédiaire de la fonction choose_dice_human ou pour un IA par la fonction choose_dice_AI et
    la variable s2 étant la somme des 2 dés restant.
    :param res_dice: contient ce que renvoie la fonction throw_dice, un tuple de 4 entiers.
    :param player_id: représente l'indice du joueur.
    :param AI: bool qui représente si c'est un humain(false), sinon True pour un IA.
    :return: un tuple de 2 entiers, correspondant à 2 voies du board.
    """
    for dice in range(4):
        print("Dé {} = {}".format(dice + 1, res_dice[dice]))
    if not AI:
        s1,s2 = choose_dice_human(res_dice,player_id)
    else:
        s1,s2 = choose_dice_AI(res_dice, player_id)
    return s1, s2

def choose_dice_human(res_dice, player_id):
    s1, s2 = 0, 0
    valid = False  # variable sentinelle permettant l'arrêt du while.
    # vérification que ce l'utilisateur a entré est correct,
    # sinon on lui redemande jusqu'à ce que l'utilisteur entre correctement les données
    while not valid:
        print_color(
            "Entrer les numéros (entre 1 et 4) des deux premiers dés à "
            "additionner séparés d'un espace:",
            COLOR_PLAYER[player_id][0],
            "\n")
        number_dice = input()
        number_dice = number_dice.split(" ")
        if len(number_dice) != 2:
            print_color("Erreur d'encodage, veuillez respecter les consignes "
                  "d'encodage", COLOR_PLAYER[player_id][0], "\n")
        elif not all(number.isdigit() for number in number_dice):
            print_color("Erreur d'encodage, veuillez respecter les consignes "
                  "d'encodage", COLOR_PLAYER[player_id][0], "\n")
        elif not all(1 <= int(number) <= 4 for number in number_dice):
            print_color("Erreur d'encodage, veuillez respecter les consignes "
                  "d'encodage", COLOR_PLAYER[player_id][0], "\n")
        elif len(set(number_dice)) == 1:
            #regarde si les 2 dés sont différents
            print_color("Erreur d'encodage, veuillez respecter les consignes "
                  "d'encodage", COLOR_PLAYER[player_id][0],"\n")
        else:
            valid = True

    s1 = sum(res_dice[int(idx) - 1] for idx in number_dice)  # sum permet de sommer chaque dé choisit par l'utilisateur
    s2 = sum(res_dice) - s1
    return s1, s2

def choose_dice_AI(res_dice, player_id):
    """
    Fonction ou IA choisit des voies aléatoirement
    """
    s1,s2 = 0,0
    dice_1 = randint(0,3)
    dice_2 =randint(0,3)
    while dice_2 == dice_1:
        dice_2 = randint(0,3)
    print_color("IA choisit les dés {} et {}".format(dice_1+1, dice_2+1), COLOR_PLAYER[player_id][0], "\n")
    s1 = res_dice[dice_1] + res_dice[dice_2]
    s2 = sum(res_dice) -s1
    return s1,s2


def move_bonzes(routes, pawns, bonzes, blocked_routes, player_id, AI):
    """
    Fonction permettant l'avancement/ placements des bonzes d'un joueur selon les règles du jeu.
    Pour plus de détail, se référer à au fichier "rules.txt" joint avec les scriptes.
    :param routes: contient le résultat de la fonction choose_dice, qui correspond à 2 entiers représentant 2 voies.
    :param pawns: liste de dictionnaire contenant des dictionnaires pour chaque joueur.
    :param bonzes: dictionnaire de bonzes du joueurs.
    :param blocked_routes: set qui représente si les voies sont fermés.
    :param player_id: représente l'indice du joueur.
    :param AI: bool qui représente si c'est un humain(false), sinon True pour un IA.
    :return: True si on a bougé/placé un bonze, sinon False.
    """
    res_bool = False
    if routes[0] not in blocked_routes or routes[1] not in blocked_routes:
        # vérifie si aucune des voie est dans le dict de bonze, si c'est un dernier bonze a placé et si la voie n'est pas identique.
        if routes[1] not in blocked_routes and routes[0] not in blocked_routes \
                and all(route not in bonzes for route in routes) and len(bonzes) == 2 and routes[0] != routes[1]:
            if not AI:
                gamer_choose = choose_routes_human(routes)
            else:
                gamer_choose = choose_routes_AI(routes)
                print_color("IA a chosit la voie {}".format(routes[gamer_choose-1]), COLOR_PLAYER[player_id][0], "\n")
            if gamer_choose == 2:
                routes = reversed(routes)
        for route in routes:
            flag = True
            if route not in blocked_routes:
                # La route n'est pas bloquée, différents cas possibles.
                if route in bonzes:
                    # Un bonze est sur cette voie.
                    while flag:
                    # si pion adverse avance jusqu'à une case libre.
                        bonzes[route] += 1
                        if not pawn_at_height(route, bonzes[route], pawns):
                            flag = False
                    res_bool = True
                    if bonzes[route] == HEIGHT[route]:
                        blocked_routes.add(route)
                elif len(bonzes) !=3:
                    if route in pawns[player_id]:
                        # le bonze est mis sur le pion du joueur si un pion existait sur la voie.
                        bonzes[route] = pawns[player_id][route]
                        res_bool = True
                    elif route not in bonzes:
                        #routes pas encore dans bonze
                        bonzes[route] = 0
                        while flag:
                            bonzes[route] += 1
                            if not pawn_at_height(route, bonzes[route], pawns):
                                flag = False
                        if bonzes[route] == HEIGHT[route]:
                            blocked_routes.add(route)
                        res_bool = True
    return res_bool


def pawn_at_height(route, height,pawns):
    """
    Fonction véifiant s'il y a déjà pion à la hauteur height et sur la voie route.
    """
    res_bool = False
    for pawn in pawns:
        if route in pawn and height == pawn[route]:
            res_bool = True
    return res_bool


def choose_routes_human(available_routes):
    """
    Fonction permettant à l'utilisateur de faire un choix pour placer son dernier bonzes.
    """
    answer = ask_int_between_bounds(prompt = "Voulez-vous placer le dernier bonze dans la voie "
                               "\n 1) {} ? \n 2) {} ?\n".format(available_routes[0], available_routes[1]),low=1, high=2)
    return answer


def choose_routes_AI(available_routes):
    """
    Fonction permettant à l'IA de faire un choix aléatoire pour placer son dernier bonzes.
    """
    print("Voulez-vous placer le dernier bonze dans la voie "
                               "\n 1) {} ? \n 2) {} ?\n".format(available_routes[0], available_routes[1]))
    answer = randint(1, 2)
    return answer

def check_top(bonzes):
    """Fonction qui renvoie True si les trois bonzes/pions sont chacun au sommet d’une voie, False sinon."""
    res_bool = False
    count = 0
    for route in bonzes:
        if bonzes[route] == HEIGHT[route]:
            count += 1
            if count == 3:
                res_bool = True
    return res_bool


def decide_stop(player_id, AI):
    """
    Fonctions générale qui fait appel aux fonctions decide_stop_human/AI 
    en fonction que si le paramètre AI est à True(IA) ou False(human)
    Fonction qui renvoie un bool pour signifier l'arrêt d'un tour par True ou False pour continuer.
    """
    if not AI:
        print_color("Voulez vous : \n 1) continuer ? \n 2) arrêter ? \n", COLOR_PLAYER[player_id][0],"\n")
        res_bool = decide_stop_human()
    else:
        res_bool = decide_stop_AI()
    return res_bool


def decide_stop_human():
    """
    Fonction permettant à l'utilisateur d'arrêter.
    """
    answer = ask_int_between_bounds(low=1, high=2)
    return answer == 2


def decide_stop_AI():
    """
    Fonction qui décidé si l'IA arrêt ou non par rapport à la probabilité P. 
    Génère un nombre en 0 et 1 à l'aide de la fonction random de la librairie random
    et compare celui-ci à la probabilité P et si cela est plus grand on s'arrête, sinon elle peut continuer.
    """
    res = random()
    return res > P


def combination_2(iterable):
    """Retourne toutes les combinaisons de 2 éléments parmis les itérables"""
    combinations = []
    for idx, first in enumerate(iterable[:-1]):
        for second in iterable[idx+1:]:
            combinations.append((first, second))
    return combinations

def is_blocked(res_dice, blocked_routes, bonzes):
    """
    Permet de voir si le joueur après un jet de dé serait bloqué 
    si toutes les combinaisons ne permettent pas d'avancer/placer un bonze.
    Return True si bloqué, sinon False.
    """
    res_bool = True
    if len(bonzes) == 3:
        test = lambda route: route in bonzes and route not in blocked_routes
    else:
        test = lambda route: route not in blocked_routes
        #lambda permet de créer des fonctions anonymes, 
        #dont on l'évalue avec test, 
        #pour plus amples information voir doc python.

    for routes in combination_2(res_dice):
        dice_1, dice_2 = routes
        route = dice_1 + dice_2
        if test(route):
            res_bool = False
    return res_bool

def remove_pawns(routes, pawns, player_id):
    """
    Fonction qui permet d'enlever les pions des autres joueurs lorsqu'un joueur ferme un voie.
    """
    for route in routes:
        for idx, pawn in enumerate(pawns):
            if idx != player_id and route in pawn:
                    del pawn[route]

def game_round(pawns, bonzes, blocked_routes, player_id, AI):
    """
    Fonction qui correspond au tour de jeu d'un joueur.
    Renvoie True si le joueur a réussi à mettre trois pions au sommet de 3 voies, False sinon.
    """
    clear_bonzes(bonzes)
    flag = False
    winner = False
    old_blocked_routes = blocked_routes.copy()
    while not flag:
        res_dice = throw_dice()
        if not is_blocked(res_dice, blocked_routes, bonzes):
            routes = choose_dice(res_dice, player_id, AI)
            if move_bonzes(routes, pawns, bonzes, blocked_routes, player_id,AI):
                display_board(pawns, bonzes, player_id)
                if decide_stop(player_id, AI):
                    flag = True
                    save_bonzes(pawns, bonzes, player_id)
                    routes_difference = blocked_routes.difference(old_blocked_routes)
                    if routes_difference:
                        remove_pawns(routes_difference, pawns, player_id)
                        print_color("Vous avez fermé la ou les voie(s) {}".format(str(routes_difference)), COLOR_PLAYER[player_id][0], "\n")
                    if check_top(pawns[player_id]):
                        winner = True
                    else:
                        print("Joeur {} a décidé d'arrêter !, C'est prudent, progression sauvée".format(player_id+1))
            else:
                display_board(pawns, bonzes, player_id)
                print("Dés: {}, {}, {}, {}.".format(res_dice[0], res_dice[1], res_dice[2], res_dice[3]))
                print("Joeur {} est bloqué! Progression perdue".format(player_id+1))
                blocked_routes.intersection_update(old_blocked_routes)
                clear_bonzes(bonzes)
                flag = True
        else:
            display_board(pawns, bonzes, player_id)
            print("Dés: {}, {}, {}, {}.".format(res_dice[0], res_dice[1], res_dice[2], res_dice[3]))
            print("Joeur {} est bloqué car aucune combinaisons de dés favorable! Progression perdue".format(player_id+1))
            blocked_routes.intersection_update(old_blocked_routes)
            clear_bonzes(bonzes)
            flag = True
    if flag and not winner:
        time.sleep(1.5)
        os.system("cls" if os.name == "nt" else "clear") # permet de rafraichir la console avant passage au deuxième joueur
    return winner