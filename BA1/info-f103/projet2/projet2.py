"""
INFO-F103
Titre : Projet 2.
Auteur : JACOBS ALEXANDRE
Matricule : 000408850
Date : 02/05/2016
"""

#!/usr/bin/python3
# -*- coding: utf-8 -*-

from collections import deque, defaultdict
import itertools
import heapq
import logging


class File_a_priorite:

    """
    Queue où les éléments sont toujours triées par priorité.
    Un nombre faible est prioritaire
    Implémentation basé sur docs python.org :
    https://docs.python.org/3.5/library/heapq.html#priority-queue-implementation-notes
    """

    def __init__(self):
        self.elements = []

    def isEmpty(self):
        """Vérifie si la queue est vide"""
        return len(self.elements) == 0

    def put(self, item, priority):
        """Ajoute un élément avec une priorité queue."""
        heapq.heappush(self.elements, (priority, item))

    def get(self):
        """Récupère l'élément avec la plus haute priorité."""
        return heapq.heappop(self.elements)[1]


class Gare():

    """
    Représente une Gare contenant un nom de gare, peut ajouter des
    informations supplémentaires à cette gare tels que des gares accessibles
    à partir de celle-ci et aussi ajouter les distances
    de ces gares accessibles avec elle.
    """

    def __init__(self, nom_gare):
        self.nom_gare = nom_gare
        self.parent = None
        self.gares_accessibles = set()
        self.distance_gares = dict()

    def __eq__(self, other):
        """2 gares sont égales si elles ont le même nom."""
        return self.nom_gare == other.nom_gare

    def __lt__(self, other):
        return self.nom_gare < other.nom_gare

    def __hash__(self):
        """Fonction hash pour que 2 gares ayant le même nom ne produisent
        pas de doublons dans le set de gares_accessibles."""
        return hash(self.nom_gare)

    def ajouter_gare(self, enfant, distance):
        """Ajoute une gare 'enfant' à cette gare.

        Arguments:
            enfant: une instance de Gare
            distance (int): la distance entre ces deux gares.
        """
        enfant.parent = self
        self.gares_accessibles.add(enfant)
        self.distance_gares[enfant] = distance
        enfant.gares_accessibles.add(self)
        enfant.distance_gares[self] = distance

    def __repr__(self):
        """Repésentation d'une gare."""
        return "<Gare({})>".format(self.nom_gare)


class ReseauException(Exception):

    "Exception utilisée par ce module."
    pass


class ReseauFerroviaire():

    """Représente un arbre n-aire de Gares.

    Argument:
        gare (Gare): la gare à la racine de l'arbre.
    """

    def __init__(self, gare):
        self.racine = gare

    def __repr__(self):
        """Représentation du réseau"""
        return "<Reseau {}>".format(self.racine)

    def garesAccessibles(self, ville_depart=None):
        """Retourne toutes les gares accessibles de ce réseaux.

        Argument:
            ville_depart (Gare): la ville à partir de laquelle on recherche les gares accessibles.
                Par défaut, ville_depart commence à la racine du réseau.

        Return:
            set: l'ensemble des gares accessibles.
        """
        if not ville_depart:
            ville_depart = self.racine
        parcour_reseau = set()
        queue = deque()
        queue.append(ville_depart)
        while queue:
            gare = queue.popleft()
            for voisin in gare.gares_accessibles:
                if voisin not in parcour_reseau:
                    queue.append(voisin)
                    parcour_reseau.add(voisin)
        return parcour_reseau

    def trouverParcours(self, destinations):
        """Retourne une liste de listes de gare allant de la racine
        du réseau à chacune des destinations passées en
        paremètre sous forme de liste.
        Note:
            - Si la destination est la racine, le chemin comprendra
            la destination
            - Si la destination n'est pas dans le réseau, cette
            gare sera ignorée et un message de warning s'affichera
            à l'utilisateur lors de l'exécution du programme
        """
        gares_reseau = self.garesAccessibles()
        liste_parcours = []
        for destination in destinations:
            if destination not in gares_reseau:
                logging.warning(
                    "{} n'est pas dans le réseau {}.".format(destination, self))
                continue
            liste_parcours.append(self.trajet(destination))
        return liste_parcours

    def trajet(self, destination):
        "Retourne la liste des gares allant de la racine à la destination."
        # On s'assure qu'on part bien de la gare de NOTRE réseau.
        # Les gares inter-réseaux ont 2 instances avec le même nom.
        for gare in self.garesAccessibles():
            if destination == gare:
                destination = gare
        parcour = [destination]
        while destination.parent:
            parcour.append(destination.parent)
            destination = destination.parent
        parcour.reverse()
        return parcour

    def trouverDistance(self, destinations):
        """
        Retourne les différentes distances (en km) entre la racine principale
        du réseaux et les différentes gare contenue dans destinations
        sous forme de liste.
        """
        parcours = self.trouverParcours(destinations)
        distances = []
        for parcour in parcours:
            distances.append(self.calcul_distance(parcour[-1]))
        return distances

    def calcul_distance(self, gare):
        """Calcul et renvoie la distance entre la gare et la racine du réseau."""
        distance = 0
        while gare.parent:
            distance += gare.parent.distance_gares[gare]
            gare = gare.parent
        return distance

    def trouverParcoursMin(self, villeA, villeB):
        """
        Trouve un parcours entre 2 villes quelconques du réseau et
        renvoie celle-ci sous forme de liste
        """
        parcour_A, parcour_B = self.trouverParcours([villeA, villeB])
        trajet_min = []
        index = 0
        for gare in reversed(parcour_A):
            if gare in parcour_B:
                index = parcour_B.index(gare)
                break
            trajet_min.append(gare)
        trajet_min.extend(parcour_B[index:])
        return trajet_min

    def calcul_cout(self, villeA, villeB):
        """Calul et renvoie combien de sauts il faut effectuer pour aller de VilleA à VilleB"""
        return len(self.trouverParcoursMin(villeA, villeB)) - 1


class GrapheReseauFerroviaire:

    """Un Graphe de plusieurs ReseauFerroviaire.

    Arguments:
        reseaux (list): Une liste de ReseauFerroviaire.
    """

    def __init__(self, reseaux, villeA, villeB, reseauA, reseauB):
        self.villeA, self.villeB = villeA, villeB
        self.reseauA, self.reseauB = reseauA, reseauB
        # Les réseaux passant par une gare
        # Cle: gare inter-réseau, valeur: set de réseaux
        self.gare_reseaux = defaultdict(set)
        # Les gares inter-réseaux de ce réseau
        # Cle: réseau, valeur: set de gares inter-réseaux
        self.reseau_gares = defaultdict(set)

        # On ajoute les villes d'origine et de destination
        self.gare_reseaux[villeA].add(reseauA)
        self.gare_reseaux[villeB].add(reseauB)
        self.reseau_gares[reseauA].add(villeA)
        self.reseau_gares[reseauB].add(villeB)

        for reseau_from, reseau_to in itertools.combinations(reseaux, 2):
            gares = reseau_from.garesAccessibles(
            ) & reseau_to.garesAccessibles()
            if not gares:
                # Pas de gares entre ces 2 réseaux
                continue
            for gare in gares:
                self.gare_reseaux[gare].add(reseau_from)
                self.gare_reseaux[gare].add(reseau_to)
                self.reseau_gares[reseau_from].add(gare)
                self.reseau_gares[reseau_to].add(gare)

        # Les connexions entre les gares inter-réseaux
        self.connexions = defaultdict(set)
        for reseau, gares in self.reseau_gares.items():
            for gare_from, gare_to in itertools.permutations(gares, 2):
                self.connexions[gare_from].add(gare_to)

    def recherche_dijkstra(self):
        """Algorithme de dijkstra pour trouver le parcours le plus court dans
        le graphe de Réseaux.
        Retourne 2 dictionnaire avec lieux visités et les couts pour y arriver.
        Algo réalisé à partir de recherche internet et
        d'information provenant de wikipédia, des slides de INFO-F-203.
        https://en.wikipedia.org/wiki/Dijkstra%27s_algorithm
        https://fr.wikipedia.org/wiki/Algorithme_de_Dijkstra
        """
        villeA, villeB = self.villeA, self.villeB
        reseauA, reseauB = self.reseauA, self.reseauB

        # contient les différents lieux accessible de puis un autre
        # avec le cout comme indice de priorité
        gare_frontiere = File_a_priorite()
        # dictionnaires contenant les lieux visité(valeurs) depuis un lieux (key)
        origine = {}
        # dictionnaire contenant les couts pour se rendre vers un lieux
        # key : lieu de destination. (n'est pas le lieu de destination finale)
        # valeur: cout pour aller vers ce lieux
        cout = {}
        origine[villeA] = None
        cout[villeA] = 0
        gare_frontiere.put(villeA, 0)

        # Algo de Dijkstra
        while not gare_frontiere.isEmpty():
            gare = gare_frontiere.get()

            if gare == villeB:
                # On est arrivé à destination.
                break

            for voisin in self.connexions[gare]:
                reseau = (self.gare_reseaux[
                    gare] & self.gare_reseaux[voisin]).pop()
                nouveau_cout = cout[gare] + \
                    reseau.calcul_cout(gare, voisin)
                if voisin not in cout or nouveau_cout < cout[voisin]:
                    priorite = nouveau_cout
                    gare_frontiere.put(voisin, priorite)
                    cout[voisin] = nouveau_cout
                    origine[voisin] = gare
        return origine, cout

    def reconstruction_chemin(self, origine):
        """
        Reconstruit le chemin à partir de l'endroit
        d'où on vient (origne = dictionnaire)
        """
        villeA, villeB = self.villeA, self.villeB
        actuelle = villeB
        path = [actuelle]
        while actuelle != villeA:
            suivante = origine[actuelle]
            for reseau in self.reseau_gares:
                if {actuelle, suivante} <= reseau.garesAccessibles():
                    reseau_commun = reseau
                    break
            path.extend(reseau_commun.trouverParcoursMin(
                actuelle, suivante)[1:])
            actuelle = suivante
        path.reverse()
        return path

    def trouverParcoursMin(self):
        origine, cout = self.recherche_dijkstra()
        return self.reconstruction_chemin(origine)


def trouverParcoursMin(reseaux, villeA, villeB):
    """Retourne le parcours contenant le moins de gares entre villeA et villeB.

    Les villes ne doivents pas nécessairement se trouver dans le même réseau.
    les réseaux données en paramètre dans reseaux ne doivent pas nécessairement
    être connecté simplement.

    Arguments:
        reseaux (list[ReseauFerroviaire]): une liste de réseaux.
        villeA (Gare) : la ville de départ.
        villeB (Gare) : la ville d'arrivée.
    """
    reseau_origin = None
    reseau_dest = None
    for reseau in reseaux:
        gares_accessibles = reseau.garesAccessibles()
        if villeA in gares_accessibles:
            reseau_origin = reseau
        if villeB in gares_accessibles:
            reseau_dest = reseau
    if not reseau_origin:
        raise ReseauException(
            "{} n'est dans aucun réseau.".format(villeA.nom_gare))
    if not reseau_dest:
        raise ReseauException(
            "{} n'est dans aucun réseau.".format(villeB.nom_gare))
    if reseau_origin == reseau_dest:
        return reseau_origin.trouverParcoursMin(villeA, villeB)
    # VilleA et VilleB dans des réseaux différents
    graphe_reseau_ferroviaire = GrapheReseauFerroviaire(
        reseaux, villeA, villeB, reseau_origin, reseau_dest)
    return graphe_reseau_ferroviaire.trouverParcoursMin()