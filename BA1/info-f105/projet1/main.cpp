/*
INFO-F105
Titre : Projet 1 CPP.  
Auteur : JACOBS ALEXANDRE 
Matricule : 000408850 
Date : 26/02/2016
*/

#include "ES.hpp" // fait appel au fichier contenant les déclarations de certaines fonctions.  

float moyenne (int x, int y ) 
{
	
	/*
	Fonction renvoyant la divison de deux nombres sous forme d'un "float"	
	*/

	return  static_cast<float> (x) / static_cast<float> (y) ; // static_cast<float> permet de convertir le résultat obtenu sous forme d'un nombre à virgul
}

int v_plafond(float x) 
{

	/*
	Renvoi la valeur plafond de la valeur en paramètre, 
	c'est-à-dire la plus petit entier supérieur ou égal à la valeur.
	*/

	if (static_cast<int> (x) == x ) // static_cast<int> permet de convertir la valeur entrée en paramètre en une valeu entière.  
		return  static_cast<int> (x) ; 
	else 
		return  static_cast<int> (x) + 1 ; 
}

int main() 
{	
	print_bienvenue();
	int note,nbre_note=0, nbre_note_ss_zero=0, somme_note = 0 ; // initialise 4 variables "int"
	float moyenne_arith=0 , moyenne_ss_zero=0 ; // initialise 2 variables "float"
	note = ask_note() ;
	while (note != -1)
	{	
		if (note != 0)
			nbre_note_ss_zero += 1;
		somme_note+=note;
		nbre_note += 1;
		note = ask_note();
	}
	moyenne_arith = moyenne(somme_note,nbre_note);
	print_moyenne(moyenne_arith);
	if (somme_note == 0 )
		print_moyenneSansZero(0);
	else 
	{
		moyenne_ss_zero = moyenne(somme_note,nbre_note_ss_zero);
		print_moyenneSansZero(moyenne_ss_zero);
	}
	print_plancher( static_cast<int> (moyenne_arith)); // Renvoie le plus grand entier inférieur ou égal à la valeur
	print_plafond(v_plafond(moyenne_arith));
}