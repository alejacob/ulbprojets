#include "ListDictionary.hpp"


void ListDictionary::insert(const std::string &word) {
    List<std::string>::insert(word);
}

void ListDictionary::erase(const std::string &word) {
   List<std::string>::erase(word);
}

bool ListDictionary::search(const std::string &word) const {
    return List<std::string>::search(word);
}

ListDictionary& ListDictionary::operator+=(const Dictionary& other) {
    for(auto it_string = other.begin(); it_string != other.end(); ++it_string) {
        insert(*it_string);
    }
    return *this;
}

Dictionary::const_iterator ListDictionary::begin() const {
    const_Place p_iter = List<std::string>::begin();
    const_iterator* l_iter = new const_iterator(p_iter);
    return Dictionary::const_iterator(l_iter);
}

Dictionary::const_iterator ListDictionary::end() const {
    const_Place p_iter = List<std::string>::end();
    const_iterator* l_iter = new const_iterator(p_iter);
    return Dictionary::const_iterator(l_iter);
}
