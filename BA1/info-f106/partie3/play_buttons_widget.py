# !/usr/bin/python3
# -*- coding: utf-8 -*-
from PyQt4 import QtGui, QtCore
from ui_play_buttons_widget import Ui_PlayerButton


class PlayerButton(QtGui.QDialog, Ui_PlayerButton):
    """
    Classe affichant le panneau de commande de l'utilisateur 
    pour le choix de stopper ou de lancer les dés.
    """

    #Initialisation de signal(pyqtSignal) qui seront envoyé lors que 
    #des actions spéciale seront effectué tant par l'humain que par l'IA
    #Ces signaux seront connecter à des slot(fonction qui permet de gèrer ces signaux).
    throwDice = QtCore.pyqtSignal()
    stopPlay = QtCore.pyqtSignal()

    def __init__(self,parent=None):
        """
        Fonction qui initialise une instance de la classe lorsqu'elle est utilisé.
        """
        super(PlayerButton, self).__init__(parent)
        self.setupUi(self)
        self.throw_dice_button.clicked.connect(lambda:self.handle_button(self.throw_dice_button))
        self.stop_button.clicked.connect(lambda:self.handle_button(self.stop_button))

    def handle_button(self, button):
        """
        Fonction qui émet un signal en fonction du bouton qui  a été cliqué.
        """
        button_name = button.objectName()
        if button_name == 'throw_dice_button':
            self.throwDice.emit()

        elif button_name == 'stop_button':
            self.stopPlay.emit()
        
    def handle_enable_throw(self, bool):
        """
        Rends le bouton non clickable en fonction du bool donné en paramètre.
        """
        self.throw_dice_button.setEnabled(bool)

    def handle_enable_stop(self, bool):
        """
        Rends le bouton non clickable en fonction du bool donné en paramètre.
        """
        self.stop_button.setEnabled(bool)


    def updateLabelPlayer(self, string):
        """
        Mets à jour le label du player qui doit jouer.
        """
        label = "{} Turn".format(string)
        self.player_turn_label.setText(label)






