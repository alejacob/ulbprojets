#!/bin/bash

# AUTEUR: JACOBS Alexandre
# Projet bash OS
# INFO-F201
# MATRICULE: 408850
# DATE: 08/12/2016

filename="p1_quotes"
replace_quote="Chuck Norris"
parameters=$*
flagNoResultSearch=0 # permet de savoir si un des paramètres donné ne donne aucun résultat.
flagCreate=0 # permet de savoir si des fichiers de citation ont été crée
quote="quote"
count=1
directory_execution="ex_quotes_execution_$(date +"%d%m%Y_%H%M%s%10N")"
mkdir "$directory_execution" 

while read line # permet de lire chaque ligne.
do
	echo "$line" > "$directory_execution/$quote$count" # ">" permet de rediriger le flux stdout vers le fichier 
	#qui sera crée automatiquent comme le fichier n'existe pas encore.
	count=$((count+1)) #incrément du compteur
	flagCreate=1
done < $filename # permet de savoir quel fichier lire, "<" permet de rediriger le flux stdin.

if [ "${#parameters}" -ne "0" ] && [ $flagCreate -eq "1" ]
# vérifie si des mots sont données en paramètre lors de l'exécution et qu'on a crée des fichiers.
# si aucun paramètre donnée ou que fichier de citation vide, affichage d'un message à l'utilisateur.
then
	for param in $parameters
	do 
		if [ -d "$directory_execution/$param" ] #si recherche du mot déjà effectué, affichage d'un message à 'utilisateur'
		then
			echo "Directory $param already in $directory_execution, can not do again a search for $param."
		else
			files=$(grep -wli "$param" $directory_execution/"$quote"*)
			# la commande "grep" permet de chercher si le mot est présent 
			# dans les fichiers donnée en argument,
			# l'option "-w" permet de regarder juste les résultat 
			# contenant une concordance formant un mot complet, l'option "-l" ne garde juste le nom de fichier,
			# l'option "-i" pemet d'ignorer les différences majuscules/minuscules.
			
			if [ "${#files}" -ne "0" ]
			#vérifie si le grep a donnée un résultat, sinon on passe le flagNoResultSearch à 1	
			then
				mkdir "$directory_execution/$param"
				cp $files "$directory_execution/$param"
				
			else
				mkdir "$directory_execution/$param"
				flagNoResultSearch=1
			fi
		fi
	done
	
	if [ "$flagNoResultSearch" -eq "1" ]
		#vérifie s'il y a eu une recherche ne donnant aucun résultat.
	then 
		
		for file in $directory_execution/"$quote"*
		do 
			echo "$replace_quote" > "$file" # replace chaque citation par "Chuck Norris"
		done
		
	fi

elif [ "${#parameters}" -eq "0" ]
then
 	echo "No word given for search"

else
    echo "No files created because p1_quotes is empty"
    for param in $parameters
    do
    	mkdir $directory_execution/$param
    done
    echo "Result of execution is in $directory_execution"
fi

if [ "$flagCreate" -eq "1" ]
then
	echo "Result of execution is in $directory_execution"
fi