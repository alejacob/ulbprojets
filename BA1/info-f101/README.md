# Info-F101

* [Projet 1: M'enfin](./projet1/projet1.pdf) a obtenu une note de 7/10

* [Projet 2: Vasarely doit se retourner dans sa tombe](./projet2/projet2.pdf) a obtenu une note de 8/10

* [Projet 3: Gestion d'agenda](./projet3/projet3.pdf) a obtenu une note de 7/10

* [Projet 4: Résolution de système linéaire](./projet4/projet4.pdf) a obtenu une note de 9/10

# Outils Nécessaires

+ Python3