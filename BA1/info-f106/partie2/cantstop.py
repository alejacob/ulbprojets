#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
INFO-F106
Projet d'année BA1: Can't Stop
Partie 2
Auteur : JACOBS ALEXANDRE
Matricule : 000408850
Date : 16/12/2016
"""
import os
import cantstopFunctions as cf


def main_game_round():
    """
    Fonction qui fait la boucle principale du jeu. 
    Initialise les variables locales et globales à l'aide de init()
    Et on boucle jusqu'à un des joueurs gagne et chaque est fait grâce 
    à l'aide la fonction game_round définie dans cantstopFunctions.py.
    """
    print("Bienvenue dans Can't Stop")
    players_type = cf.setup_players()
    number_player = len(players_type)
    bonzes, pawns, blocked_routes = cf.init(number_player)
    winner = False
    player_id = 0
    while not winner:
        cf.display_board(pawns, bonzes, player_id)
        cf.print_color("Au joueur {} de jouer".format(player_id+1), cf.COLOR_PLAYER[player_id][0], "\n")
        winner = cf.game_round(pawns, bonzes, blocked_routes, player_id, players_type[player_id])
        winner_id = player_id
        player_id = (player_id + 1) % len(players_type)
    cf.display_board(pawns, bonzes, winner_id)
    cf.print_color("Le joueur {} a gagné".format(winner_id + 1), cf.COLOR_PLAYER[winner_id][0],"\n")
    print("Fin de partie.")
if __name__ == "__main__":
    main_game_round()

