#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <string.h>

#define PORT 5555 // the port client will be connecting to 

#define MAXDATASIZE 100 

int main( int argc, char *argv[]){
    int socket_fd, read_size;
    char read_message[MAXDATASIZE];
    char write_message[MAXDATASIZE];
    struct hostent *he;
    struct sockaddr_in server;

    if(argc != 2){
        fprintf(stderr,"usage: client hostname\n");
        exit(1);
    }

    if((he = gethostbyname(argv[1])) == NULL) // get host info
    {
        perror("gethostbyname");
        exit(1);
    }

    //création du socket
    socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if(socket_fd == -1){
        perror("Could not create socket");
        exit(1);
    }

    server.sin_family = AF_INET;    // host byte order 
    server.sin_port = htons(PORT);  // short, network byte order 
    server.sin_addr = *((struct in_addr *)he->h_addr);
    memset(&(server.sin_zero), '\0', 8);  // zero the rest of the struct 

    if (connect(socket_fd, (struct sockaddr *)&server, sizeof(struct sockaddr)) < 0) {
        perror("connect failed. Error");
        exit(1);
    }

    read_size = recv(socket_fd, read_message, MAXDATASIZE, 0);
    if(read_size < 0){
        perror("recv failed");
    }
    read_message[read_size] = '\0';
    if(strcmp(read_message,"full") != 0){ // check si le serveur n'est pas remplit
        printf("%s\n", read_message);
        read_size = recv(socket_fd, read_message, MAXDATASIZE, 0);
        if(read_size < 0){
            perror("recv failed");
        }
        read_message[read_size] = '\0';
        printf("%s", read_message);

        read_size = recv(socket_fd, read_message, MAXDATASIZE, 0);
        if(read_size < 0){
            perror("recv failed");
        }
        read_message[read_size] = '\0';
        printf("%s", read_message);

        int finish = 0;

        while(finish <=4)// 5 fois car doit entrer sujet, adjectif, verbe, complément, adjectif
        {
            read_size = recv(socket_fd, read_message, MAXDATASIZE, 0);
            if(read_size < 0){
                perror("recv failed");
            }

            read_message[read_size] = '\0';
            printf("%s", read_message);
            fgets(write_message, MAXDATASIZE, stdin); // permet de récupérer ce que l'utilisateur entre dans le terminal
            char *remove = strchr(write_message, '\n'); // cherche si y a un \n
            if (remove){ // si remove n'est pas null, alors change le charactère
                *remove = '\0';
            } 

            if(send(socket_fd, write_message, strlen(write_message)+1, 0) < 0){
                perror("send");
                exit(1);
            }
            finish++;
        }
        read_size = recv(socket_fd, read_message, MAXDATASIZE, 0);
        if(read_size < 0){
            perror("recv failed");
        }
        read_message[read_size] = '\0';
        printf("%s", read_message);
        int i = 0;
        while(i < 4){
            read_size = recv(socket_fd, read_message, MAXDATASIZE, 0);
            if(read_size < 0){
                perror("recv failed");
            }
            read_message[read_size] = '\0';
            printf("%s\n", read_message);
            ++i;
        }
        read_size = recv(socket_fd, read_message, MAXDATASIZE, 0);
        if(read_size < 0){
            perror("recv failed");
        }
        read_message[read_size] = '\0';
        printf("%s", read_message);
    }
    else{
        printf("%s\n", "Serveur remplit, déjà une partie en cours");
        printf("%s\n", "Déconnexion");
    }

    close(socket_fd);
}