"""
INFO-F103
Titre : Projet 1: Planning de livraison
Auteur : JACOBS ALEXANDRE
Matricule : 000408850
Date : 21/03/2016
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-


class Stack:

    """
    Imported from the cours and adding of 2 methods : copy() and print_stack()
    """

    def __init__(self):
        self.items = []

    def isEmpty(self):
        return self.items == []

    def push(self, item):
        self.items.append(item)

    def pop(self):
        return self.items.pop()

    def top(self):
        return self.items[self.size()-1]

    def size(self):
        return len(self.items)

    def print_stack(self):
        "Prints the contents of the stack recursively"

        if self.size() == 1:
            print(self.pop())
        else:
            item = self.pop()
            self.print_stack()
            self.push(item)
            print(item)

    def copy(self):
        "Return a copy of the stack in another stack"

        new = Stack()
        new.items = self.items[:]
        return new

    def __repr__(self):
        return str(self.items)


class Planning:

    """
    Backtracking algorithm (based on branch and bound) and finding the best solution 
    to a delivery schedule given by the number of clients, 
    places, and time limits of deliveries to clients.
    All information is given by a file.
    """

    def __init__(self, filename):
        self.filename = filename
        # read of file and take off information
        with open(filename, "r") as file:
            lines = file.readlines()
        self.n = int(lines[0])  # contained the number of client
        self.m = int(lines[1])  # contained the number of node (place)
        # create a list of time limit for each clients
        self.delivery_time = [int(time) for time in lines[2].split(" ")]
        # create a time table
        self.times = [[int(time) for time in line.split(" ")]
                      for line in lines[3:]]
        self.actions = Stack()
        self.parcours = Stack()
        self.best_actions = Stack()
        self.best_parcours = Stack()
        self.contained_truck = Stack()
        # Count how often a place was visited
        self.visits = [0] * self.m
        self.visits[2 * self.n] = 1
        # Hangar full or empty
        self.hangars = [True] * self.n
        # Client delivered or not
        self.clients_delivered = [False] * self.n
        # None means that no solution is found at the beginning or at the end
        self.totaltime = None
        self.count = 0  # count the number of iterations.
        start_node = self.n * 2
        action = ("{node} : {time}, {location}".format(
            node=start_node,
            time=self.conversion_time(0),
            location=self.get_location(start_node)))
        self.actions.push(action)
        self.parcours.push(start_node)
        self.solve(start_node, 0)
        self.parcours = self.best_parcours.copy()
        self.actions = self.best_actions.copy()
        self.print_Res()

    def solve(self, node, total_time):

        self.count += 1
        # Accept case
        if self.solution_accepted():
            self.totaltime = total_time
            self.best_actions = self.actions.copy()
            self.best_parcours = self.parcours.copy()
        else:

            # Backtracking step
            for node, travel_time in self.neighbors(node):
                arrival_time = total_time + travel_time
                if arrival_time > self._deadline():
                    continue
                self.visits[node] += 1

                # If node is a client: try to deliver him
                if node < self.n:
                    self.deliver_client(node, arrival_time)

                # Elif node is a hangar: try to load truck
                elif node < self.n * 2:
                    self.load_truck(node, arrival_time)

                # Only visit this node
                if node < self.n and not self.clients_delivered[node]:
                    # Cannot visit client not yet delivered
                    self.visits[node] -= 1
                    continue
                action = "{node} : {time}, {location}".format(
                    node=node,
                    time=self.conversion_time(arrival_time),
                    location=self.get_location(node))
                self._do_action(action, arrival_time, node)
                # Undo
                self.visits[node] -= 1

    def solution_accepted(self):
        "Return True if all clients are delivered else False"

        return all(self.clients_delivered)

    def get_location(self, node):
        "Return a string with the location of the node"

        if node < self.n:
            return "client {}".format(node)
        elif node < self.n * 2:
            return "depot du client {}".format(node - self.n)
        else:
            return "carrefour"

    def neighbors(self, from_node):
        "Return a tuple with a node and time to go the node from a another node"

        neighbours = []
        for to_node, time in enumerate(self.times[from_node]):
            if time != -1 and self.visits[to_node] < 2:
                neighbours.append((to_node, time))
        return neighbours

    def _deadline(self):
        """
        Find the maximum time elapsed to be still able to deliver everyone.

        Sort all undelivered clients
        For each undelivered client, we must have at least:
            - 10 mins left if hangar is full
            - 5 mins left if parcel is in the truck
            So we can decrease its delivery time by this amount plus the amount
            used at the preceding iteration
        """

        min_time = 0
        # create a list with time and the client index of clients undelivered
        clients_undelivered = [(time, client_id) for client_id, (time, delivered) in
                               enumerate(
                                   zip(self.delivery_time, self.clients_delivered))
                               if not delivered]
        sorted_undelivered = sorted(clients_undelivered)
        max_elapsed_time = []
        for time, client_id in sorted_undelivered:
            if self.hangars[client_id]:
                min_time += 10
            else:
                min_time += 5
            max_elapsed_time.append(time - min_time)
        return min(max_elapsed_time)

    def _do_action(self, action, time, node):
        """
        Push the action to the Stack and 
        solve recursively if time better than best time
        """

        self.actions.push(action)
        self.parcours.push(node)
        if self.totaltime is None or time <= self.totaltime:
            self.solve(node, time)
        # Undo
        self.actions.pop()
        self.parcours.pop()

    def deliver_client(self, node, arrival_time):
        """
        deliver the client and unload of the merchandise 
        of the client is the last load in the truck.
        """

        end_time = arrival_time + 5
        if not(self.contained_truck.isEmpty()) and self.contained_truck.top() == node:
            self.contained_truck.pop()
            self.clients_delivered[node] = True
            action = ("{node} : {time}, client {node}, "
                      "dechargement fini a {time_end}".format(
                          node=node, time=self.conversion_time(arrival_time),
                          time_end=self.conversion_time(end_time)))
            self._do_action(action, end_time, node)
            # Undo
            self.clients_delivered[node] = False
            self.contained_truck.push(node)

    def load_truck(self, node, arrival_time):
        """
        Load marchandise if the hangar is not empty. 
        """

        end_time = arrival_time + 5
        hangar_index_client = node - self.n
        if self.hangars[hangar_index_client]:
            self.hangars[hangar_index_client] = False
            self.contained_truck.push(hangar_index_client)
            action = ("{node} : {time}, depot du client {client}, "
                      "chargement fini a {end_time}".format(
                          node=node, time=self.conversion_time(arrival_time),
                          client=node - self.n,
                          end_time=self.conversion_time(end_time)))
            self._do_action(action, end_time, node)
            # Undo
            self.contained_truck.pop()
            self.hangars[hangar_index_client] = True

    def conversion_time(self, time):
        """
        Return a string of time in hours and minutes
        """

        time += 8*60
        hour = time // 60
        minute = time % 60
        return "{0:02d}h{1:02d}".format(hour, minute)

    def print_Res(self):
        """
        Print the solution; print a delivery schedule, the total time 
        and the number of iterations.
        """

        print('-'*80)
        print(self.filename)
        print('-'*80)
        if self.totaltime is None:
            print("Trajet impossible dans le temps imparti")
        else:
            self.actions.print_stack()
            print("Itérations:", self.count)
            print("Temps total:", self.totaltime)
            print()
