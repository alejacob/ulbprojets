#ifndef _TRIE_HPP
#define _TRIE_HPP
#include <string>
#include <algorithm>    // std::swap

class Trie {
public:
    class const_Iterator;
    typedef const_Iterator const_iterator;
private:
    struct Node {
        const char _character;
        Node* _father;
        Node* _first_child;
        Node* _brother;
        bool _tagASWord;
        Node(): _character('\0'), 
                _father(nullptr), 
                _first_child(nullptr), 
                _brother(nullptr), 
                _tagASWord(false) {}
        Node(char chr): _character(chr), 
                        _father(nullptr), 
                        _first_child(nullptr), 
                        _brother(nullptr), 
                        _tagASWord(false) {}
        Node(const Node &node);
        Node& operator=(const Node& other) = delete;
    };

    Node* _head;

    const Node* find(const std::string &word) const;
    Node* find(const std::string &word);
    void clear(Node* node);
    void copy_nodes_from(Node* from, Node* to);

public:
    Trie(): _head(new Node()) {}
    Trie(const Trie& trie);
    Trie(Trie &&trie);
    Trie& operator=(Trie trie);
    virtual ~Trie();

    void insert(const std::string &word);
    void erase(const std::string &word);
    inline bool search(const std::string &word) const {
        return find(word) ? true : false; 
    }
    friend void swap(Trie& t1, Trie& t2);
    const_Iterator begin() const;
    const_Iterator end() const;
};


class Trie::const_Iterator {
public:
    const_Iterator(): current(nullptr){}

    std::string operator*() const;
    const_Iterator& operator++();
    inline friend bool operator!=(const const_Iterator &i1, 
                                  const const_Iterator &i2) { 
        return i1.current != i2.current;
    }
    inline friend bool operator==(const const_Iterator &i1, 
                                  const const_Iterator &i2) {
        return i1.current == i2.current;
    }

private:
    friend class Trie;
    const Node* current;
    const_Iterator(const Node* n): current(n) {}
};


#endif