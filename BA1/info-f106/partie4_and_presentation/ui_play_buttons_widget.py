# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_play_buttons_widget.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_PlayerButton(object):
    def setupUi(self, PlayerButton):
        PlayerButton.setObjectName(_fromUtf8("PlayerButton"))
        PlayerButton.resize(369, 245)
        self.gridLayout = QtGui.QGridLayout(PlayerButton)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.player_turn_label = QtGui.QLabel(PlayerButton)
        font = QtGui.QFont()
        font.setPointSize(12)
        font.setBold(True)
        font.setItalic(True)
        font.setUnderline(True)
        font.setWeight(75)
        self.player_turn_label.setFont(font)
        self.player_turn_label.setObjectName(_fromUtf8("player_turn_label"))
        self.gridLayout.addWidget(self.player_turn_label, 0, 0, 1, 1)
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.throw_dice_button = QtGui.QPushButton(PlayerButton)
        self.throw_dice_button.setText(_fromUtf8(""))
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(_fromUtf8("throw_dice.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.throw_dice_button.setIcon(icon)
        self.throw_dice_button.setIconSize(QtCore.QSize(50, 50))
        self.throw_dice_button.setObjectName(_fromUtf8("throw_dice_button"))
        self.horizontalLayout.addWidget(self.throw_dice_button)
        self.stop_button = QtGui.QPushButton(PlayerButton)
        self.stop_button.setText(_fromUtf8(""))
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap(_fromUtf8("stop_game.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.stop_button.setIcon(icon1)
        self.stop_button.setIconSize(QtCore.QSize(50, 50))
        self.stop_button.setObjectName(_fromUtf8("stop_button"))
        self.horizontalLayout.addWidget(self.stop_button)
        self.gridLayout.addLayout(self.horizontalLayout, 1, 0, 1, 1)

        self.retranslateUi(PlayerButton)
        QtCore.QMetaObject.connectSlotsByName(PlayerButton)

    def retranslateUi(self, PlayerButton):
        PlayerButton.setWindowTitle(_translate("PlayerButton", "Form", None))
        self.player_turn_label.setText(_translate("PlayerButton", "Player 1 Turn", None))

