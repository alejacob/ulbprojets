#ifndef _DICTIONARY_HPP
#define _DICTIONARY_HPP
#include <string>
#include <algorithm> // std::swap


class Dictionary {
    //Interface abstraite de dictionnaire
public:
    class const_DictIterator;
    typedef const_DictIterator const_iterator;

    virtual ~Dictionary() = default;
    virtual void insert(const std::string &word) = 0;
    virtual void erase(const std::string &word) = 0;
    virtual bool search(const std::string &word) const = 0;
    virtual Dictionary& operator+=(const Dictionary&) = 0;
    virtual const_iterator begin() const = 0;
    virtual const_iterator end() const = 0;
};

class AbstractConstDictIterator {
public:
    virtual std::string operator*() const = 0;
    virtual AbstractConstDictIterator& operator++() = 0;
    virtual ~AbstractConstDictIterator() {}
    virtual bool operator!=(const AbstractConstDictIterator&) const = 0;
    virtual bool operator==(const AbstractConstDictIterator&) const = 0;
    virtual AbstractConstDictIterator* clone() const = 0;
};

class Dictionary::const_DictIterator {
public:
    const_DictIterator(AbstractConstDictIterator* iter):
        m_iter(iter) {}

    ~const_DictIterator() {
        delete m_iter;
    }

    const_DictIterator(const_DictIterator& other): m_iter(nullptr) {
        m_iter = other.clone();
    }

    const_DictIterator(const_DictIterator&& other): m_iter(nullptr) {
        swap(*this, other);
    }

    const_DictIterator& operator=(const_DictIterator other) {
        swap(*this, other);
        return *this;
    }

    friend void swap(const_DictIterator& first, const_DictIterator& second) {
        using std::swap;
        swap(first.m_iter, second.m_iter);
    }

    AbstractConstDictIterator* clone() { return m_iter->clone(); }

    // Iterator interface
    std::string operator*() const { return *(*m_iter); }

    const_DictIterator& operator++() { 
        ++(*m_iter);
        return *this; 
    }

    inline friend bool operator!=(const const_DictIterator &i1, 
                                  const const_DictIterator &i2) { 
        return *(i1.m_iter) != *(i2.m_iter); 
    }

    inline friend bool operator==(const const_DictIterator &i1, 
                                  const const_DictIterator &i2) { 
        return *(i1.m_iter) == *(i2.m_iter); 
    }

private:
    AbstractConstDictIterator* m_iter;
};

#endif