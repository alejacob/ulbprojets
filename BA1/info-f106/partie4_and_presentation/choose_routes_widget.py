from PyQt4 import QtGui, QtCore
from ui_choose_routes_widget import Ui_ChooseRoute

class ChooseRoute(QtGui.QDialog, Ui_ChooseRoute):
    """
    Classe qui crée l'affichage du panneau de commande de sélection des voies.
    """
    #Initialisation de signal(pyqtSignal) qui seront envoyé lors que 
    #des actions spéciale seront effectué tant par l'humain que par l'IA
    #Ces signaux seront connecter à des slot(fonction qui permet de gèrer ces signaux).
    moveBonzes = QtCore.pyqtSignal(list)

    #Message d'information utilisé lors que l'utilisateur commets des erreurs
    #ou lorsque celui à un choix limité.
    message_not_valid = """Not the right number of routes choose!
    Please select only two routes or only one route.
    See the rules when to select one route.
    """
    message_bonze = """ Remains a bonze in place.
    Please select only one way between for place a new bonze and a second if possible for update another bonze.
    If to free way are selected for a new bonze. The first way selected would be use.
    """
    def __init__(self,parent=None):
        """
        Fonction qui initialise une instance de la classe lorsqu'elle est utilisé.
        """
        super(ChooseRoute, self).__init__(parent)
        self.routes = []
        self.setupUi(self) # permet d'initialiser le widget avec le fichier 
        # générer à l'aide de QtDesigner
        self.connect_button()

    def connect_button(self):
        """
        Fonction qui  connecte le signaux des boutons à un slot(fonction) 
        qui sera effectué par le signal lorsque celui-ci est émit.
        """
        self.combi_dice_1.toggled.connect(lambda:self.handle_choose(self.combi_dice_1))
        self.combi_dice_2.toggled.connect(lambda:self.handle_choose(self.combi_dice_2))
        self.combi_dice_3.toggled.connect(lambda:self.handle_choose(self.combi_dice_3))
        self.combi_dice_4.toggled.connect(lambda:self.handle_choose(self.combi_dice_4))
        self.combi_dice_5.toggled.connect(lambda:self.handle_choose(self.combi_dice_5))
        self.combi_dice_6.toggled.connect(lambda:self.handle_choose(self.combi_dice_6))
        self.go_button.clicked.connect(self.play_move)

    def handle_choose(self, button):
        """
        Fonction qui permet de savoir quel bouton a été sélectionné/désélectionner
        et qui permet de mettre à jour la sélection de vois qui est sauvé dans
        self.routes.
        bouton: est le bouton ayant émis le signal.
        """
        button_name = button.objectName()
        route = button.text()
        if button.isChecked():
            if button_name == 'combi_dice_1':
                self.routes.append(int(route))
            elif button_name == 'combi_dice_2':
                self.routes.append(int(route))
            elif button_name == 'combi_dice_3':
                self.routes.append(int(route))
            elif button_name == 'combi_dice_4':
                self.routes.append(int(route))
            elif button_name == 'combi_dice_5':
                self.routes.append(int(route))
            elif button_name == 'combi_dice_6':
                self.routes.append(int(route))
        if not button.isChecked():
            if button_name == 'combi_dice_1' and int(route) in self.routes:
                self.routes .remove(int(route))
            elif button_name == 'combi_dice_2'and int(route) in self.routes:
                self.routes.remove(int(route))
            elif button_name == 'combi_dice_3' and int(route) in self.routes:
                self.routes.remove(int(route))
            elif button_name == 'combi_dice_4' and int(route) in self.routes:
                self.routes.remove(int(route))
            elif button_name == 'combi_dice_5' and int(route) in self.routes:
                self.routes.remove(int(route))
            elif button_name == 'combi_dice_6' and int(route) in self.routes:
                self.routes.remove(int(route))


    def updateCombiDice(self):
        """
        Fonction qui mets à jour les combinaisons de dés qui sont les voies 
        et bloque le choix de celle qui ne sont pas possible. 
        Fonction appelé lors de l'émission d'un signal précis.
        """
        #permet de savoir qui a lancé le signal au quel ce slot est connecté
        sender = self.sender() 
        ways = sender.jeu.combination_2(sender.jeu.res_dices)
        way_possibility = sender.jeu.all_possibility(sender.player_id)
        if len(sender.jeu.player[sender.player_id].bonzes) == 2 \
            and sender.jeu.player[sender.player_id].player_type == "Human":
            self.alert_popup(self.message_bonze)
        all_ways = [ways[i][0]+ways[i][1] for i in range(len(ways))]
        self.combi_dice_1.setText(str(all_ways[0]))
        self.combi_dice_2.setText(str(all_ways[1]))
        self.combi_dice_3.setText(str(all_ways[2]))
        self.combi_dice_4.setText(str(all_ways[3]))
        self.combi_dice_5.setText(str(all_ways[4]))
        self.combi_dice_6.setText(str(all_ways[5]))
        self.reset_enabled()
        self.reset_checked()
        self.go_button.setEnabled(True)
        if all_ways[0] not in way_possibility:
            self.combi_dice_1.setEnabled(False) #permet de ne plus sélectionner la voie
        if all_ways[1] not in way_possibility:
            self.combi_dice_2.setEnabled(False)
        if all_ways[2] not in way_possibility:
            self.combi_dice_3.setEnabled(False)
        if all_ways[3] not in way_possibility:
            self.combi_dice_4.setEnabled(False)
        if all_ways[4] not in way_possibility:
            self.combi_dice_5.setEnabled(False)
        if all_ways[5] not in way_possibility:
            self.combi_dice_6.setEnabled(False)

    def play_move(self):
        """
        Fonction est appélé par le signal émit par le bouton go. 
        Si la sélection est correct, un signal est émis 
        pour faire le déplacement de bonzes.
        """
        if not self.route_valid():
            self.alert_popup(self.message_not_valid)
            self.routes.clear()
            self.reset_checked()

        else:
            self.moveBonzes.emit(self.routes)
            self.routes.clear()

    def route_valid(self):
        """
        Fonction qui vérifie si la sélection des voies est correct.
        """
        return 0 < len(self.routes) <= 2 


    def alert_popup(self, prompt):
        """Fonction qui affiche le popup"""
        QtGui.QMessageBox.warning(self, "Warning", prompt)

    def updateFreeRouteLabel(self, height, pawns, blocked_routes):
        """
        Fonction permettant de mettre à jour le texte 
        qui contient le nombre de voies libres qui n'ont pas de bonzes/pwan dessus.
        """
        all_way = set()
        for pawn in pawns:
            if len(pawn) != 0:
                all_way.update(set(pawn))
        all_way.update(blocked_routes)
        free_routes = set(height).difference(all_way)
        text = "Free: {}".format(len(free_routes))
        self.count_free_route.setText(text)

    def reset_check_box(self):
        """
        Fonction permet de rendre les checkbox de nouveau cliquable 
        et désélectionne les checkbox.
        """
        self.reset_checked()
        self.reset_enabled()

    def reset_checked(self):
        """
        Fonction qui permet de désélectionner les checkbox.
        """
        self.combi_dice_1.setChecked(False)
        self.combi_dice_2.setChecked(False)
        self.combi_dice_3.setChecked(False)
        self.combi_dice_4.setChecked(False)
        self.combi_dice_5.setChecked(False)
        self.combi_dice_6.setChecked(False)

    def reset_enabled(self):
        """Fonction permet de rendre les checkbox de nouveau cliquable."""
        self.combi_dice_1.setEnabled(True)
        self.combi_dice_2.setEnabled(True)
        self.combi_dice_3.setEnabled(True)
        self.combi_dice_4.setEnabled(True)
        self.combi_dice_5.setEnabled(True)
        self.combi_dice_6.setEnabled(True)

    def handle_enable_all_button(self, bool):
        """
        Fonction permet de rendre les boutons de nouveau cliquable.
        """
        self.combi_dice_1.setEnabled(bool)
        self.combi_dice_2.setEnabled(bool)
        self.combi_dice_3.setEnabled(bool)
        self.combi_dice_4.setEnabled(bool)
        self.combi_dice_5.setEnabled(bool)
        self.combi_dice_6.setEnabled(bool)
        self.go_button.setEnabled(bool)








