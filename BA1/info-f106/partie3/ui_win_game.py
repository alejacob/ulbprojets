# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_win_game.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Winning(object):
    def setupUi(self, Winning):
        Winning.setObjectName(_fromUtf8("Winning"))
        Winning.resize(990, 750)
        Winning.setMinimumSize(QtCore.QSize(990, 750))
        Winning.setMaximumSize(QtCore.QSize(1010, 876))
        self.gridLayout = QtGui.QGridLayout(Winning)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.verticalLayout = QtGui.QVBoxLayout()
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.winning_player = QtGui.QLabel(Winning)
        font = QtGui.QFont()
        font.setPointSize(12)
        font.setBold(True)
        font.setUnderline(True)
        font.setWeight(75)
        self.winning_player.setFont(font)
        self.winning_player.setAlignment(QtCore.Qt.AlignCenter)
        self.winning_player.setObjectName(_fromUtf8("winning_player"))
        self.verticalLayout.addWidget(self.winning_player)
        self.picture_label = QtGui.QLabel(Winning)
        self.picture_label.setText(_fromUtf8(""))
        self.picture_label.setPixmap(QtGui.QPixmap(_fromUtf8("winner.jpg")))
        self.picture_label.setAlignment(QtCore.Qt.AlignCenter)
        self.picture_label.setObjectName(_fromUtf8("picture_label"))
        self.verticalLayout.addWidget(self.picture_label)
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        spacerItem = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.play_game = QtGui.QPushButton(Winning)
        self.play_game.setObjectName(_fromUtf8("play_game"))
        self.horizontalLayout.addWidget(self.play_game)
        spacerItem1 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem1)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.gridLayout.addLayout(self.verticalLayout, 0, 0, 1, 1)

        self.retranslateUi(Winning)
        QtCore.QMetaObject.connectSlotsByName(Winning)

    def retranslateUi(self, Winning):
        Winning.setWindowTitle(_translate("Winning", "Game Over", None))
        self.winning_player.setText(_translate("Winning", "Player X Is ", None))
        self.play_game.setText(_translate("Winning", "Play New Game", None))

