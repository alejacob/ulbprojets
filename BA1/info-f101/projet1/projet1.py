"""
INFO-F101
Titre : Projet 1 "M'enfin"
Auteur : JACOBS ALEXANDRE
Matricule : 000408850
Groupe TP : 5
Assitant TP  : Cédric Ternon.
Date : 12/10/2015
"""

""" importe les modules randint et seed de la librairie random """
from random import randint, seed

"""
   Ces  2  fonction permetent la conversion des minutes, aussi l'affichage  au
   format heures : minutes ( et format XX h et YY min pour le temps de travail )  et  perpermet d'afficher des 0 si les minutes et/ou heures ont moins de 2
   caractères.
"""


def conversion_temps(minutes):
    heures = (minutes // 60)
    minutes = minutes % 60

    out = ("{0:02d}:{1:02d}".format(heures, minutes))
    return out


def affichage_temps_travail(minutes):
    heures = (minutes // 60)
    minutes = minutes % 60
    out = ("{0:01d} h et {1:02d} min".format(heures, minutes))
    return out

"""déclarations des variables principales"""

s = int(input("Entrez la seed : "))
seed(s)                             # Détermine le nombre de pause.
heure_debut = 9 * 60
heure_fin = 18 * 60
prunelle_absent = True
temps_total_travaille = 0
heure_fin_surveillance = 0
minutes_avant_prunelle_arrivee = 0
heure_prunelle = 0

while heure_debut < heure_fin:			# Début boucle des pauses et travail de gaston jusqu'à 18h.
    
    if prunelle_absent == True :			# Cas 1 : Prunelle absent donc pause
        if heure_debut > heure_fin:		# Si heure début plus grand que 18h , on sort de cette boucle. 
            print("18:00 Fin du service, dure journée")
        else:
            print(conversion_temps(heure_debut), "OK,pause !")
        heure_debut += 50  			# Incrémente la variable de 50 minutes

    		
    email = randint(1, 3)                       #génère un nombre aléatoire entre 1 et 3
    if email == 1:
        prunelle_absent = False
    else:
        prunelle_absent = True
    
    if prunelle_absent == False:				# Cas 2 Prunelle n'est pas absent, donc différentes situations. 
        heure_prunelle = heure_debut + randint(10, 60)		
        if heure_prunelle > heure_fin:				# si prunelle arrive après 18h		
            print("18:00 Fin du service, dure journée")		# email ignorée car journée de Gaston finie.
        else :						        # Sinon, Gaston lit l'email et voit ce qu'il peut faire.
            print(conversion_temps(heure_debut),
                  "Attention Prunelle arrive à ", conversion_temps(heure_prunelle))
            minutes_avant_prunelle_arrivee = heure_prunelle - heure_debut

            if minutes_avant_prunelle_arrivee < 20: 	# Cas 2.1  Moins 20 minutes avant arrivée de Prunelle
                print(conversion_temps(heure_debut),
                      "Il faut travailler. M’enfin. ")
                temps_total_travaille += heure_prunelle-heure_debut + 90
                heure_fin_surveillance = heure_prunelle + 90
                if heure_fin_surveillance > heure_fin:	    #si départ de prunelle avant 18 h 
                    print("18:00 Prunelle est parti. \\0/ ")
                    temps_total_travaille += (heure_fin-heure_fin_surveillance)
                else:					    #si départ après 18 h
                    print(conversion_temps(heure_fin_surveillance),
                      "Prunelle est parti. \\0/ ")
            
            elif minutes_avant_prunelle_arrivee >= 20 and minutes_avant_prunelle_arrivee <= 39:	    # Cas 2.2  Entre 20 et 39 minutes avant arrivée de Prunelle
                print(conversion_temps(heure_debut),
                      "C’est bon, encore le temps de faire une sieste. Zzz ")
                heure_debut += 20
                print(conversion_temps(heure_debut),
                      "Il faut travailler.M'enfin.")
                temps_total_travaille += heure_prunelle-heure_debut + 90
                heure_fin_surveillance = heure_prunelle + 90
                if heure_fin_surveillance > heure_fin:	    #si départ de prunelle avant 18 h 
                    print("18:00 Prunelle est parti. \\0/ ")
                    temps_total_travaille += (heure_fin-heure_fin_surveillance)
                else:					    #si départ après 18 h
                    print(conversion_temps(heure_fin_surveillance),
                      "Prunelle est parti. \\0/ ")

            elif minutes_avant_prunelle_arrivee >= 40 and minutes_avant_prunelle_arrivee <= 49:	    # Cas 2.3  Entre 40 et 49 minutes avant arrivée de Prunelle
                print(conversion_temps(heure_debut),
                      "C’est bon, encore le temps de faire une sieste. Zzz ")
                heure_debut += 20
                print(conversion_temps(heure_debut),
                      "C’est bon, encore le temps de faire une sieste. Zzz ")
                heure_debut += 20
                print(conversion_temps(heure_debut),
                      "Il faut travailler.M'enfin.")
                temps_total_travaille += heure_prunelle-heure_debut + 90
                heure_fin_surveillance = heure_prunelle + 90
                if heure_fin_surveillance > heure_fin:	    #si départ de prunelle avant 18 h 
                    print("18:00 Prunelle est parti. \\0/ ")
                    temps_total_travaille += (heure_fin-heure_fin_surveillance)
                else:					    #si départ après 18 h
                    print(conversion_temps(heure_fin_surveillance),
                      "Prunelle est parti. \\0/ ")
            else:										    # Cas 2.4  Plus de 50 minutes avant arrivée de Prunelle
                print(conversion_temps(heure_debut), "OK,pause !")
                heure_debut += 50
                print(conversion_temps(heure_debut),
                      "Il faut travailler.M'enfin.")
                temps_total_travaille += 90
                heure_fin_surveillance = heure_prunelle + 90
                if heure_fin_surveillance > heure_fin:	    # si départ de prunelle avant 18 h 
                    print("18:00 Prunelle est parti. \\0/ ")
                    temps_total_travaille += (heure_fin-heure_fin_surveillance)
                else:					    #  si 2.6 Départ après 18 h
                    print(conversion_temps(heure_fin_surveillance),
                      "Prunelle est parti. \\0/ ")
            heure_debut = heure_fin_surveillance

print ("Temps totale travaillé :",
       affichage_temps_travail(temps_total_travaille))
