#ifndef _TRIEDICTIONARY_HPP
#define _TRIEDICTIONARY_HPP
#include "Trie.hpp"
#include "Dictionary.hpp"
#include <iostream>
#include <string>

class TrieDictionary: public Trie, public Dictionary
{
public:
    class const_TrieDictIterator;
    typedef const_TrieDictIterator const_iterator;

    void insert(const std::string &word) override;
    void erase(const std::string &word) override;
    bool search(const std::string &word) const override;

    TrieDictionary& operator+=(const Dictionary& other) override;

    Dictionary::const_iterator begin() const override;
    Dictionary::const_iterator end() const override;
};

class TrieDictionary::const_TrieDictIterator: public AbstractConstDictIterator{
public:
    const_TrieDictIterator(const_Iterator iter):
        m_iter(iter) {}

    std::string operator*() const override { return *m_iter; }
    
    const_TrieDictIterator& operator++() override { 
        ++m_iter;
        return *this; 
    }

    TrieDictionary::const_iterator* clone() const override {
        return new TrieDictionary::const_iterator(m_iter);
    }

    bool operator!=(const AbstractConstDictIterator& other) const override {
        return !(*this == other);
    }
   
    bool operator==(const AbstractConstDictIterator& other) const override {
        const TrieDictionary::const_iterator* iter = 
                dynamic_cast<const TrieDictionary::const_iterator*>(&other);
        if(iter){
            return m_iter == iter->m_iter;
        }
        return false;
    }
    
private:
    const_Iterator m_iter;
};

#endif