# !/usr/bin/python3
# -*- coding: utf-8 -*-
from PyQt4 import QtGui
from os import sys
from mainWindow import MainWindow
from welcome_widget import Welcome
from gui import GUI 
from setup_game_widget import SetupGame 
from win_widget import WinningGame

class CantStop():
	"""
	Classe qui initialiser les différentes classes qui composent 
	les différentes fenêtre et gère l'enchainement de celle-ci.
	"""
	def __init__(self, parent=None):
		self.main_window = MainWindow()
		self.welcome = Welcome()
		self.setup_game = SetupGame()
		self.gui = GUI()
		self.win = WinningGame()
		self.main_window.setCentralWidget(self.welcome)
		self.welcome.showSetupGameWidget.connect(self.modify_central_widget_to_setup_Game)
		self.setup_game.showGUI.connect(self.modify_central_widget_to_gui)
		self.gui.winner.connect(self.modify_central_widget_to_winner)
		self.win.restartGame.connect(self.modify_central_widget_win_to_setup)
		self.main_window.show()


	def modify_central_widget_to_setup_Game(self):
		self.main_window.setCentralWidget(self.setup_game)

	def modify_central_widget_to_gui(self, type_players):
		self.main_window.setCentralWidget(self.gui)
		self.gui.start_game(type_players)

	def modify_central_widget_to_winner(self, string):
		self.main_window.setCentralWidget(self.win)
		self.win.winning_player.setText("{} is".format(string))

	def modify_central_widget_win_to_setup(self):
		self.setup_game = SetupGame()
		self.main_window.setCentralWidget(self.setup_game)
		self.gui = GUI()
		self.win = WinningGame()
		self.setup_game.showGUI.connect(self.modify_central_widget_to_gui)
		self.gui.winner.connect(self.modify_central_widget_to_winner)
		self.win.restartGame.connect(self.modify_central_widget_win_to_setup)

def main():
	app = QtGui.QApplication(sys.argv)
	main_loop = CantStop()
	sys.exit(app.exec_())

if __name__ == '__main__':
	main()