# !/usr/bin/python3
# -*- coding: utf-8 -*-
from PyQt4 import QtGui, QtCore
from board_draw import Board
from choose_routes_widget import ChooseRoute
from play_buttons_widget import PlayerButton
from print_dices_widget import PrintDices
from jeu import Jeu
import time

class GUI(QtGui.QWidget):
    """
    Classe permettant d'afficher la vue du plateau de jeu et 
    aussi controllant les actions que le joueur peut réaliser
    à l'aide des boutons disponible et envoyant des signaux 
    lorsque celle ci doit être rafraichie.
    Aussi c'est classe initie la classe jeu qui est utilisé 
    pour effectuer les mouvements et aussi pour effectuer 
    les différents check sur l'état de jeu.
    """

    #Initialisation de signal(pyqtSignal) qui seront envoyé lors que 
    #des actions spéciale seront effectué tant par l'humain que par l'IA
    #Ces signaux seront connecter à des slot(fonction qui permet de gèrer ces signaux).
    setLabelPlayer = QtCore.pyqtSignal(str)
    setLabelFreeRoute = QtCore.pyqtSignal(dict, list, set)
    dice_throw = QtCore.pyqtSignal()
    refreshBoard = QtCore.pyqtSignal(dict, dict, dict, int)
    enableThrow = QtCore.pyqtSignal(bool)
    enableStop = QtCore.pyqtSignal(bool)
    enableAllChooseButton = QtCore.pyqtSignal(bool)
    winner = QtCore.pyqtSignal(str) 
    
    def __init__(self, parent=None):
        #initalisation du constructeur parent.
        super(GUI, self).__init__(parent)
        #intialisation des différentes classes utiles au fonctionnement d'une partie.
        self.jeu = Jeu()
        self.player_id = 0
        self.board = Board()
        self.board_view = QtGui.QGraphicsView(self.board)
        self.choose_routes = ChooseRoute()
        self.play_buttons = PlayerButton()
        self.print_dices = PrintDices()

        #Placement des éléments qui seront affiché avec le plateau de jeu
        self.grid_layout = QtGui.QGridLayout(self)
        self.horizontal_layout = QtGui.QHBoxLayout()
        self.horizontal_layout.addWidget(self.print_dices)
        self.horizontal_layout.addWidget(self.play_buttons)
        self.horizontal_layout.addWidget(self.choose_routes)
        self.grid_layout.addWidget(self.board_view, 0, 0)
        self.grid_layout.addLayout(self.horizontal_layout, 1, 0)

        #Connection des signaux créer plus haut et des signaux provenant
        #ces classes à des slot de cette classe ou des classes utilisé ici.
        self.setLabelPlayer.connect(self.play_buttons.updateLabelPlayer)
        self.setLabelFreeRoute.connect(self.choose_routes.updateFreeRouteLabel)
        self.play_buttons.throwDice.connect(self.handle_throw)
        self.play_buttons.stopPlay.connect(self.handle_stop)
        self.dice_throw.connect(self.print_dices.updatePrintDices)
        self.dice_throw.connect(self.choose_routes.updateCombiDice)
        self.refreshBoard.connect(self.board.updateBoard)
        self.choose_routes.moveBonzes.connect(self.handle_move_bonzes)
        self.enableThrow.connect(self.play_buttons.handle_enable_throw)
        self.enableStop.connect(self.play_buttons.handle_enable_stop)
        self.enableAllChooseButton.connect(self.choose_routes.handle_enable_all_button)
        self.enableStop.emit(False)
        self.enableAllChooseButton.emit(False)

        #variable pour IA
        self.stop = False
        self.blocked = False
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.turn_AI)

    
    def start_game(self, type_players):
        """
        Fonctions qui permet d'initialiser la classe jeu avec les informations reçu de la fenetre setup_game.
        """
        self.jeu.setup_player(type_players)
        if self.jeu.player[self.player_id].player_type == "AI":
            self.game_round_AI()
        else:
            text = "Player {}".format(self.player_id + 1)
            self.setLabelPlayer.emit(text)

    # Slot pour les différents signaux
    def handle_throw(self):
        """
        Fonction qui est effectuer lors de l'émission de du signal 
        au quel ce slot est connecté. Elle réalise le lancement des dés 
        et envoi un signal pour rafraîchir les dés.
        """
        self.enableAllChooseButton.emit(False)
        self.jeu.throw_dice()
        self.dice_throw.emit()
        self.enableThrow.emit(False)
        self.enableStop.emit(False)
        if self.jeu.is_blocked():
            self.handle_blocked()
    
    def handle_move_bonzes(self, routes):
        """
        Fonction qui est effectuer lors de l'émission de du signal 
        au quel ce slot est connecté. Elle effectue le déplacement des bonzes 
        et envoi un signal pour rafraichir le plateau de jeu.
        """
        self.jeu.moves_bonzes(routes, self.player_id)
        self.refreshBoard.emit(self.jeu.HEIGHT, self.jeu.bonzes, self.jeu.get_pawns_pos(), self.player_id)
        self.setLabelFreeRoute.emit(self.jeu.HEIGHT, self.jeu.pawns, self.jeu.blocked_routes)
        self.enableThrow.emit(True)
        self.enableStop.emit(True)
        self.enableAllChooseButton.emit(False)
        if self.jeu.player[self.player_id].player_type == "AI":
            self.enableThrow.emit(False)
            self.enableStop.emit(False)
        else:
            self.enableThrow.emit(True)
            self.enableStop.emit(True)

    
    def handle_stop(self):
        """
        Fonction qui est effectuer lors de l'émission de du signal 
        au quel ce slot est connecté. Elle effectue la fin de tour du joueur. 
        Et elle est aussi utilisé lorsqu'un AI termine de jouer.
        """
        self.enableStop.emit(False)
        self.enableAllChooseButton.emit(False)
        routes_difference = self.jeu.blocked_routes.difference(self.jeu.old_blocked_routes)
        self.jeu.save_bonzes(self.player_id)
        win = False
        if routes_difference:
            self.jeu.remove_pawns(routes_difference, self.player_id)
        self.refreshBoard.emit(self.jeu.HEIGHT, self.jeu.bonzes, self.jeu.get_pawns_pos(), self.player_id)
        self.setLabelFreeRoute.emit(self.jeu.HEIGHT, self.jeu.pawns, self.jeu.blocked_routes)
        self.jeu.old_blocked_routes = self.jeu.blocked_routes.copy()
        if self.jeu.check_top(self.player_id):
            winner_id = self.player_id
            win = True
        else:
            self.player_id = (self.player_id + 1) % len(self.jeu.player)
            if self.jeu.player[self.player_id].player_type == "AI":
                self.game_round_AI()
            else:
                self.enableThrow.emit(True)
                text = "Player {}".format(self.player_id+1)
                self.setLabelPlayer.emit(text)
        if win:
            self.handle_wining(winner_id)

    def handle_wining(self, winner_id):
        """
        Fonction effectuer lorsqu'un joueur a gagné 
        et envoi un signal pour afficher l'image gagnante.
        """
        if self.jeu.player[self.player_id].player_type == "AI":
            text = "AI {}".format(self.player_id + 1)
        else:
            text = "Player {}".format(self.player_id+1)
        self.popup_win(text)
        self.winner.emit(text)

    def handle_blocked(self):
        """
        Fonction qui est effectuer lors de l'émission de du signal 
        au quel ce slot est connecté. Elle effectue la gestion de blocage, 
        en réinitialisant les différentes variables 
        et envoi un signal pour rafraichir le plateau de jeu.
        """
        if self.jeu.player[self.player_id].player_type == "AI":
            text = "AI {}".format(self.player_id + 1)
        else:
            text = "Player {}".format(self.player_id+1)
        self.popup_alert(text)
        self.jeu.blocked_routes.intersection_update(self.jeu.old_blocked_routes)
        self.jeu.clear_bonzes()
        self.player_id = (self.player_id + 1) % len(self.jeu.player)
        self.refreshBoard.emit(self.jeu.HEIGHT, self.jeu.bonzes, self.jeu.get_pawns_pos(), self.player_id)
        if self.jeu.player[self.player_id].player_type == "AI":
            self.game_round_AI()
        else:
            text = "Player {}".format(self.player_id + 1)
            self.setLabelPlayer.emit(text)
            self.enableThrow.emit(True)
            self.enableAllChooseButton.emit(False)

    #Popup d'informations ou de Warning
    def popup_alert(self, string):
        message = """{} are blocked.
        Progression Failed
        """.format(string)
        QtGui.QMessageBox.warning(self, "Warning", message)

    def popup_win(self, string):
        message = """{} is the winner of the game.
        For Start a new part restart all the program""".format(string)
        QtGui.QMessageBox.information(self, "Information", message)


    def game_round_AI(self):
        """
       Fonctions qui gère les IA pour réaliser les interactions(jet de dé, décision de stop).
       """
        self.enableThrow.emit(False)
        text = "AI {}".format(self.player_id + 1)
        self.setLabelPlayer.emit(text)
        self.stop = False
        self.blocked = False
        self.timer.start(1500)
 
    #Slot pour un tour d'IA        
    def turn_AI(self):
        """
        Fonction qui réalise un tour d'une IA avec un intervalle de temps, 
        pour permettre de voir les déplacements.
        """
        self.jeu.throw_dice()
        self.dice_throw.emit()
        if self.jeu.is_blocked():
            self.blocked = True
            self.timer.stop()
            self.handle_blocked()
        else:
            routes = self.jeu.player[self.player_id].choose_dice_AI(self.jeu.res_dices)
            self.handle_move_bonzes(routes)
            self.stop = self.jeu.player[self.player_id].decide_stop_AI()
            if self.stop:
                self.timer.stop()
                self.handle_stop()