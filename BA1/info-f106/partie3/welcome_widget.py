# !/usr/bin/python3
# -*- coding: utf-8 -*-
from PyQt4 import QtGui, QtCore
from ui_welcome_widget import Ui_Welcome

class Welcome(QtGui.QWidget, Ui_Welcome):
    """
    Classe qui affiche l'image de bienvenu au joueur.
    """

    #Initialisation de signal(pyqtSignal) qui seront envoyé lors que 
    #des actions spéciale seront effectué tant par l'humain que par l'IA
    #Ces signaux seront connecter à des slot(fonction qui permet de gèrer ces signaux).
    showSetupGameWidget = QtCore.pyqtSignal()
    
    def __init__(self,parent=None):
        """
        Fonction qui initialise une instance de la classe lorsqu'elle est utilisé.
        """
        super(Welcome, self).__init__(parent)
        self.setupUi(self)
        self.playButton.clicked.connect(self.play)

    def play(self):
        """
        Fonction effectué lorsque le joueur décide de jouer.
        """
        self.showSetupGameWidget.emit()