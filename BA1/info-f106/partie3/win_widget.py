# !/usr/bin/python3
# -*- coding: utf-8 -*-
from PyQt4 import QtGui, QtCore
from ui_win_game import Ui_Winning

class WinningGame(QtGui.QDialog, Ui_Winning):
    """
    Classe affichant au joueur une image gagnante 
    et laisse la possibilité au joueur de rejouer une partie.
    """
    restartGame = QtCore.pyqtSignal()
    def __init__(self, parent=None):
        """
        Fonction qui initialise une instance de la classe lorsqu'elle est utilisé.
        """
        super(WinningGame, self).__init__(parent)
        self.setupUi(self)
        self.play_game.clicked.connect(self.play_a_new_game)

    def play_a_new_game(self):
        """
        Fonction effectué lorsque le joueur veut rejouer une partie.
        """
        self.restartGame.emit()