# ULBProjets
Répertoire regroupant mes différents projets fait lors de mon Bachelier.

Copyright 2015-2017@ULB
## BA1 2015-2016 & 2016-2017
  * [Info-F101](./BA1/info-f101): Programmation (2015-2016)
  * [Info-F102](./BA1/info-f102): Fonctionnement des ordinateurs (2015-2016)
  * [Info-F103](./BA1/info-f103): Algorithmique 1 (2015-2016)
  * [Info-F105](./BA1/info-f105): Langages De Programmation 1 (2015-2016)
  * [Info-F106](./BA1/info-f106): Projets d'informatique 1 (2016-2017)

## BA2 2016-2017
  * Info-H303: Base De Donnée
    - [IMDB: Internet Movie Database](../../../Projet-BDD-IMDB).
  * [Info-F201](./BA2/info-f201): Operating System (OS)
  * [Info-F202](./BA2/info-f202): Langages De Programmation 2
  * Info-F203: Algorithmique 2
    - [Les bons comptes font les bons amis!](../../../Projet-INFOF203).
  * Info-F209: Projets d'informatique 2
    - [Groupe 8](../../../Group8).

