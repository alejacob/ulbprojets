#include "Bank.hpp"
#include <iostream>

/////////////////////////
///Account implementation
/////////////////////////

//Constructor
Bank::_Account::_Account(std::string name, float amount):
        _client(name), _balance(amount), _next(nullptr) {}

float Bank::_Account::get_balance() const
{
    return _balance;
}

void Bank::_Account::set_balance(float amount)
{
    _balance = amount;
}

const std::string& Bank::_Account::get_client() const
{
    return _client;
}

void Bank::_Account::set_client(std::string name)
{
    _client = name;
}

Bank::_Account* Bank::_Account::get_next_account() const
{
    return _next;
}

void Bank::_Account::set_next_account(_Account* next)
{
    _next = next;
}

/////////////////////////
///Bank implementation
/////////////////////////

//Constructor
Bank::Bank(): 
    _head(nullptr) {}


//Copy Constructor
Bank::Bank(const Bank& other):
    _head()
{
    if (other._head == nullptr) 
        return;

    _Account* current_account = other._head;
    _Account* copy = new _Account(*current_account);
    _head = copy;
    current_account = current_account->get_next_account();

    while(current_account != nullptr)
    {
        copy->set_next_account(new _Account(*current_account));
        copy = copy->get_next_account();
        current_account = current_account->get_next_account();
    }
}

//Assigment operator
Bank& Bank::operator=(Bank other)
{
    _Account* temp = _head;
    _head = other._head;
    other._head = temp; 
    return *this;
}

//Destructor
Bank::~Bank()
{   
    _Account* current_account = _head;
    while (current_account != nullptr)
    {
        _Account* next = current_account->get_next_account();
        delete current_account;
        current_account = next;
    }
}


float Bank::withdraw(const std::string& name, float amount)
{
    _Account* account = getAccountByName(name);
	// if the account don't exist	    
	if (account == nullptr)
    {
        std::cout<<"ERROR: Can't withdraw money on unexisted account."<< std::endl;
        return 0;
    }
	//if too much money to withdraw
    if (amount > account->get_balance())
    {   std::cout<<"DIENED OPERATION --> Not enough money to withdraw."<<std::endl;
        return 0;
    }
	// if a negatif amount to withdraw
    if (amount < 0)
    {
        std::cout<<"DIENED OPERATION --> You can't withdraw a negative amount."<<std::endl;
        return 0;
    }
   
    float balance;
    balance =  account->get_balance();
    balance -= amount;
    account->set_balance(balance);
    return amount;
}

float Bank::deposit(const std::string& name, float amount)
{
    _Account* account = getAccountByName(name);
	// if the account don't exist
    if (account == nullptr)
    {
        std::cout<<"ERROR: Can't deposit money on unexisted account."<< std::endl;
        return 0;
    }
	// if a negatif amount to withdraw
    if (amount < 0)
    {
        std::cout<<"DIENED OPERATION --> You can't deposit a negative amount."<<std::endl;
        return 0;        
    }
    float balance;
    balance =  account->get_balance();
    balance += amount;
    account->set_balance(balance);
    return balance;
}

void Bank::createAccount(std::string name, float amount)
{
    // If bad name or amount
    if (name.empty() || amount <= 0)
    {
        std::cout<<"ERROR: Can’t create account with balance <=0 or with a empty name."<<std::endl;
        return;
    }
    // If account exists
    if (hasAccount(name))
    {
        std::cout<<"DENIED OPERATION! --> "<<name 
        << "’s account already exists."<<std::endl;
        return;
    }
    // Create new account
    _Account* new_account = new _Account(name, amount);
    _Account* last_account = get_last_account();
    if (last_account == nullptr)
        _head = new_account;
    else 
        last_account->set_next_account(new_account);
}

float Bank::deleteAccount(const std::string& name)
{
    _Account* account = getAccountByName(name);

    // if the account don't exist
    if (account == nullptr)
    {
        std::cout<<"ERROR: No account found for "<<name<<"!"<<std::endl;
        return 0;
    }
    _Account* previous_account = getPreviousAccount(account);
	
	// if account it's the first in bank 
    if (previous_account == nullptr)
        _head = account->get_next_account();
	
	// if the account between two others account   
	 else
        previous_account->set_next_account(account->get_next_account());

    float balance = account->get_balance();
    delete account;
    return balance;
}

void Bank::mergeAccounts(const std::string& name_client1, const std::string& name_client2)
{
    _Account* account_client1 = getAccountByName(name_client1);
    _Account* account_client2 = getAccountByName(name_client2);
   	
	// if one of the two or the two don't exist	
	if (!account_client1 || !account_client2)
    {
        std::cout<<"ERROR : One of both don't have an account!"<<std::endl;
        return; 
    }
    float total_balance = account_client1->get_balance() + deleteAccount(name_client2);
    account_client1->set_balance(total_balance);
    account_client1->set_client(name_client1 + " and " + name_client2);
}

void Bank::displayAccounts() const
{   
	//if account in the bank
    if (_head != nullptr)
    {
        for(_Account* current_account = _head; 
            current_account != nullptr; 
            current_account=current_account->get_next_account())
        {
            std::string client_name = current_account->get_client();
            float balance = current_account->get_balance();
            std::cout<<"Name: "<<client_name<< " Balance: "<<balance<<"$"<<std::endl;
        }
    }
    //if no account in bank
    else
        std::cout<<"ERROR: Can’t display accounts of an empty bank !"<<std::endl;
}

Bank::_Account* Bank::get_last_account() const
{
	//if no account
    if(_head == nullptr)
        return _head;
	
	// if account    
	else 
    {
        _Account* account = _head;
        while (account->get_next_account() != nullptr)
            account = account->get_next_account();
        return account;
    }
}

bool Bank::hasAccount(const std::string& name) const
{ 
  
  /*
	Check if the client has account,return true if i has account 
	and return false if no account.
  */

    for(_Account* current_acccount = _head; 
	current_acccount != nullptr; 
	current_acccount = current_acccount->get_next_account())
    {
        if (current_acccount->get_client() == name)
            return true;
    }
    return false;
}

Bank::_Account* Bank::getAccountByName(const std::string& name) const
{	
	/*
	  Return the account by the client's name, 
	  if the client has no account return nullptr.
	*/

    for(_Account* current_acccount = _head; 
	current_acccount != nullptr; 
	current_acccount = current_acccount->get_next_account())
    {
        if (current_acccount->get_client() == name)
            return current_acccount;
    }
    return nullptr;
}

Bank::_Account* Bank::getPreviousAccount(_Account* account) const
{
	/*
	Check if the account has a previous account, 
	if previous account return the previous account 
	and nullptr if no previous account. 
	*/
	
    for(_Account* current_account = _head; 
        current_account != nullptr; 
        current_account = current_account->get_next_account())
    {
        if (current_account->get_next_account() == account)
            return current_account;
    }
    return nullptr;
}