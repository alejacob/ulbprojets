#include "Bank.hpp"
#include <iostream>

class Test
{
public: 
    void main();
};

void Test::main()
{

    Bank b;

    std::cout << "Create Tom’s account with 10.000$"<<std::endl;
    b.createAccount("Tom", 10000);
    std::cout << "Create Dick’s account with 20.000$"<<std::endl;
    b.createAccount("Dick", 20000);
    std::cout << "Create no name account with 20.000$"<<std::endl;
    b.createAccount("", 20000);
    std::cout << "Create Harry’s account with 30.000$"<<std::endl;
    b.createAccount("Harry", 30000);
    std::cout << "Create Tom’s account with 15.000$"<<std::endl;
    b.createAccount("Tom",15000);
    std::cout << "Create Bill’s account with -20000$"<<std::endl;
    b.createAccount("Bill",-20000);
    std::cout << "Print list of accounts:"<<std::endl;
    b.displayAccounts();
    std::cout << "withdraw 5.000$ from Tom’s account."<<std::endl;
    b.withdraw("Tom",5000);
    std::cout << "withdraw 11.000$ from Dick’s account."<<std::endl;
    float amount = b.withdraw("Dick",11000);
    std::cout << "Dick withdrew "<< amount <<"$ from his account."<<std::endl;
    std::cout << "withdraw 21.300$ from Dick’s account."<<std::endl;
    b.withdraw("Dick",21300);
    std::cout << "deposit -21.300$ on Dick’s account."<<std::endl;
    b.deposit("Dick",-21300);
    std::cout<<"deposit 5.000$ on Tom’s account."<<std::endl;
    float balance = b.deposit("Tom",5000);
    std::cout<<"Tom has "<< balance <<"$ on his account."<<std::endl;
    std::cout << "Print list of accounts:"<<std::endl;
    b.displayAccounts();
    std::cout << "Close Dick’s account."<<std::endl;
    balance = b.deleteAccount("Dick");
    std::cout << "Dick’ account closed. He took "<<balance<< "$."<<std::endl;
    std::cout << "Close Jerry’s account."<<std::endl;
    b.deleteAccount("Jerry");
    std::cout << "Merge Tom and Harry’s accounts"<<std::endl;
    b.mergeAccounts("Tom","Harry");
    std::cout << "Merge Tom and Jerry’s accounts"<<std::endl;
    b.mergeAccounts("Tom","Jerry");
    std::cout << "Print list of accounts:"<<std::endl;
    b.displayAccounts();

    
    std::cout<<"Test self assignement. We should still see the same account."<<std::endl; 
    b = b;
    std::cout << "Print list of accounts:"<<std::endl;
    b.displayAccounts();

    std::cout<<"Create new empty bank."<<std::endl;
    Bank copy;
    std::cout << "Print list of accounts of bank copy:"<<std::endl;
    copy.displayAccounts();
    std::cout<<"Add Alex's account."<<std::endl;
    copy.createAccount("Alex",1000);
    std::cout << "Print list of accounts of bank copy:"<<std::endl;
    copy.displayAccounts();
    std::cout<<"Test assignement copy = bank b."<<std::endl;
    copy = b;
    std::cout << "Print list of accounts of bank copy:"<<std::endl;
    copy.displayAccounts();
}

int main()
{
    Test testClasse; 
    testClasse.main();
}