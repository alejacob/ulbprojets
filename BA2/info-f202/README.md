# Info-F202

* [Projet Java: Thread](./Java/LG2_Java_2017-01.pdf)

* [Projet C++: Dictionnary Implementation](./C++/LG2_Cpp_2017-01.pdf)

Projet ayant été évalué lors d'un examen oral et obtenu une note de 18.

# Outils Nécessaire

+ Java version 8 ou  supérieur
+ G++ version 6
