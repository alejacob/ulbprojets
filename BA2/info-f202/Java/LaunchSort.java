/*
 * INFO-F-202
 * Projet JAVA, implémentation d'un algorithme de Tri pair-impair parallélisé.
 * Auteur: JACOBS Alexandre
 * Matricule: 408850
 * BA2-INFO
 */

import java.util.ArrayList;
import java.util.Arrays;
import static java.lang.Math.*;

public class LaunchSort {
    public static void main(String[] args) throws InterruptedException {
        System.out.println("Exemple 1:");
        int[] myArray = MultiThreadSort.arrayGenerator(32,100);
        MultiThreadSort sorter = new MultiThreadSort(2);
        System.out.format("Voici le tableau avant le tri: %s .%n",Arrays.toString(myArray));
        sorter.sort(myArray);
        System.out.format("Tous les Threads ont finis.\nVoici le vecteur trié: %s %n ",Arrays.toString(myArray));
        System.out.println("------------------------------------------------------------------------------------------");
        System.out.println("Exemple 2:");
        myArray = MultiThreadSort.arrayGenerator(10,20);
        sorter = new MultiThreadSort(6);
        System.out.format("Voici le tableau avant le tri: %s .%n",Arrays.toString(myArray));
        sorter.sort(myArray);
        System.out.format("Tous les Threads ont finis.\nVoici le vecteur trié: %s %n ",Arrays.toString(myArray));
    }
}

class MultiThreadSort {
    /*
         * Classe contenant les méthodes permettant d'instancier les threads, de créer aléatoirement des tableaux d'entier,
         * et de les trier en faisant appel à la Classe EvenOddSort.
         */

    private final int desiredThreadCount;
    private int threadCount;
    private int index[][]; // tableau à 2D qui contient en première indice l'indice d'un thread et
    // en deuxième indice contiendra un tableau  de deux éléments:
    //les indices de début et de fins des données d'un thread.
    private ArrayList<EvenOddSort> workersList; // liste contenant les différents runnable crées.
    MultiThreadSort(int threadCount) {
        this.desiredThreadCount = threadCount;
    }

    static int[] arrayGenerator(int arrayLengthDesired, int maxValue){
        /*
         * Fonction générant un tableau d'entier de façon aléatoire
         */
        int array[] = new int[arrayLengthDesired];
        for( int i = 0; i < array.length ; i++){
            array[i] = (int)(Math.random()*maxValue);
        }
        return array;
    }

    void sort(int[] array) throws InterruptedException {

        /*
         * Fonction de tri multi-Thread.
         * Elle crée les différents threads et les lance.
         * Elle prend en paramètre un tableau d'entiers(int).
         * Si le nombre de Threads donné lors de l'appel à la méthode sort est plus grand que le nombre d'éléments
         * contenu dans le tableau, on prendra le minimun entre la taille du tableau et le nombre de threads donné.
         */

        threadCount = min(array.length, desiredThreadCount);
        generateIndices(array);
        LockThread lock = new LockThread(threadCount); // objet permettant de faire des lock avec le nombre de Thread donnée
        ArrayList<Thread> threadList = new ArrayList<>(threadCount);// liste contenant les différents Threads crées.
        workersList = new ArrayList<>(threadCount);

        for (int i = 0; i < threadCount; i++) {
            EvenOddSort worker = new EvenOddSort(i, array, index[i][0], index[i][1], lock);
            workersList.add(i, worker);
            threadList.add(i, new Thread(worker));
        }

        for (Thread thread : threadList)
            thread.start(); // permet de activer les threads et la JVM fait appel à la méthode run du thread.
        for (Thread thread : threadList) {
            thread.join(); // la méthode join() de la classe Thread bloque jusqu'à ce que le thread termine
                          // et ici donc quand tout les threads ont fini.
        }
    }
    private void generateIndices(int[] array) {

        /*
         *Fonction générant les indices des différents Runnable permettant ainsi d'alterner les tailles
         * Par principe, on commence par la plus grande taille puis on alterne si possible.
         */

        index = new int[threadCount][];
        int n_mod_p = array.length % threadCount;
        int floor_size = (int) floor((double) array.length / (double) threadCount);
        int ceil_size = (int) ceil((double) array.length / (double) threadCount);
        int start = 0;
        for (int threadNumber = 0; threadNumber < threadCount; threadNumber++) {
            int length;
            if ((threadNumber % 2 == 0 && threadNumber < 2 * n_mod_p)|| threadNumber > 2*(threadCount - n_mod_p)) {
                length = ceil_size;
            }
            else {
                length = floor_size;
            }
            index[threadNumber] = new int[2];
            index[threadNumber][0] = start;
            index[threadNumber][1] = start + length;
            start += length;
        }
    }

    public int getThreadCount() {
        return threadCount;
    }

    private class EvenOddSort implements Runnable {
        /*
         * Classe implémentant l'interface Runnable, qui traite des Threads de façon parrallélisé au travers du run
         * qui est override de l'interface Runnable
         */
        private int array[];
        private final int begin; // contient l'indice de début des données d'un Thread
        private final int end;   // contient l'indice de fin des données d'un Thread
        private final int myThreadIndex;
        private int[] subArray;
        private LockThread lock;
        // variable permettant de savoir si le trie du vecteur est fini avant 2p étapes (p représentant le nombre de proccessus).
        private boolean idle;

        EvenOddSort(int myThreadIndex, int[] vector, int begin, int end, LockThread lock) {
            this.array = vector;
            this.begin = begin;
            this.end = end;
            this.myThreadIndex = myThreadIndex;
            this.subArray = new int[end - begin];
            this.lock = lock;
            this.idle = true;
        }

        private int getBegin() {
            return begin;
        }

        private int getEnd() {
            return end;
        }

        private void extractLowValues(int beginIndexCurrent,
                                      int beginIndexNext, int endIndexNext) {
            /*
             * Fonction permettant de comparé deux thread, et de rétirées les plus petites valeurs des deux threads.
             * Les valeurs obtenues sont retourné pour le premier Thread dans un tableau temporaire avant d'être recopier.
             * Elle prends en paramètre l'indice de début du premier Thread,
             * l'indice de début et de fin du thread suivant.
             */

            for (int i = 0; i < subArray.length; i++) {
                if (beginIndexNext == endIndexNext || array[beginIndexCurrent] < array[beginIndexNext]) {
                    subArray[i] = array[beginIndexCurrent];
                    beginIndexCurrent++;
                } else {
                    subArray[i] = array[beginIndexNext];
                    beginIndexNext++;
                }
            }
        }

        private void extractHighValues(int endIndexCurrent,
                                       int beginIndexNext, int endIndexNext) {
            /*
             * Fonction permettant de comparé deux thread, et de rétirées les grandes valeurs des deux threads.
             * Les valeurs obtenues sont retourné pour le deuxième Thread dans un tableau temporaire avant d'être recopier.
             * Elle prends en paramètre l'indice de début du premier Thread,
             * l'indice de début et de fin du thread suivant.
             */
            for (int i = subArray.length; i > 0; i--) {
                if (endIndexNext == beginIndexNext || array[endIndexCurrent - 1] > array[endIndexNext - 1]) {
                    subArray[i - 1] = array[endIndexCurrent - 1];
                    endIndexCurrent--;
                } else {
                    subArray[i - 1] = array[endIndexNext - 1];
                    endIndexNext--;
                }
            }
        }

        private void copyBack() {
            /*
             * Fonction recopiant les valeurs des tableaux temporaire des Threads
             * s'ils ont travailler lors de leurs étapes pair ou impair. On sait s'il ont travaillé si idle est à false,
             * après avoir recopier on repasse l'état de idle à true.
             */
            if(!idle) {
                for (int i = 0; i < subArray.length; i++) {
                    array[i + begin] = subArray[i];
                }
                idle = true;
            }
        }

        private void evenStep() {
            /*
             * Fonction faisant une étape paire :les processus d’indice pair (d’indice 2k) fusionnent partiellement
             * leurs données propres avec celles de leur voisin de droite ou suivant (d’indice 2k+1) pour ne conserver
             * que les premières valeurs (les plus petites), tandis que ce voisin de droite effectue la même fusion partielle
             * de ses données avec celles du premier pour ne conserver que les dernières valeurs (les plus grandes).
             * Si le processus courant est un dernier processus p-1 qui est pair (celui-ci n'a pas de voisin de gauche),
             * il ne fera pas d'étape pair.
             */
            if (myThreadIndex % 2 == 0 && myThreadIndex < threadCount - 1) {
                EvenOddSort next = workersList.get(myThreadIndex + 1);
                int beginIndexNext = next.getBegin();
                int endIndexNext = next.getEnd();
                extractLowValues(begin, beginIndexNext, endIndexNext);
                idle = false;
            } else if(myThreadIndex %2 ==1) {
                EvenOddSort next = workersList.get(myThreadIndex - 1);
                int beginIndexNext = next.getBegin();
                int endIndexNext = next.getEnd();
                extractHighValues(end, beginIndexNext, endIndexNext);
                idle = false;
            }
        }

        private void oddStep() {
            /*
             * Fonction faisant une étape paire :les processus d’indice pair (d’indice 2k) fusionnent partiellement
             * leurs données propres avec celles de leur voisin de droite ou suivant (d’indice 2k+1) pour ne conserver
             * que les premières valeurs (les plus petites), tandis que ce voisin de droite effectue la même fusion partielle
             * de ses données avec celles du premier pour ne conserver que les dernières valeurs (les plus grandes).
             * Si le processus courant est le processus 0 (pair , celui-ci n'a pas de voisin de gauche) ou
             * que le processus courant est le dernier processus p-1  qui est impair (celui-ci n'a pas de voisin de droite),
             * aucun des deux ne feront d'étape impair.
             */
            if (myThreadIndex % 2 == 0 && myThreadIndex != 0) {
                EvenOddSort next = workersList.get(myThreadIndex - 1);
                int beginIndexNext = next.getBegin();
                int endIndexNext = next.getEnd();
                extractHighValues(end, beginIndexNext, endIndexNext);
                idle = false;
            } else if (myThreadIndex % 2 == 1 && myThreadIndex < threadCount - 1) {
                EvenOddSort next = workersList.get(myThreadIndex + 1);
                int beginIndexNext = next.getBegin();
                int endIndexNext = next.getEnd();
                extractLowValues(begin, beginIndexNext, endIndexNext);
                //System.out.format("étape impair pour le %s %n",Thread.currentThread().getName());
                idle = false;
            }
        }

        private boolean isSorted(){
            /*
             * Fonction vérifiant si le vecteur de départ est trié.
             * Si oui la fonction renvoie true, false sinon.
             */
            int precedingValue = Integer.MIN_VALUE;
            for(int value : array){
                if(value < precedingValue){
                    return false;
                }
                precedingValue = value;
            }
            return true;
        }
        @Override
        public void run() {
            /*
             * Fonction qui est appelée par la JVM et qui contient le code que les Threads doivent exécuter.
             */

            // Trie des données de chaque thread
            Arrays.sort(array, begin, end);
            // fait un lock pour voir si tout les Thread ont fini.
            // Si oui, Tous les threads (ayant été "réveillé" par le dernier thread ayant eu le lock) continu leurs exécutions .
            try {
                lock.await("Etape de tri");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            // Si oui, Tous les threads (ayant été "réveillé" par le dernier thread ayant eu le lock) continu leurs exécutions .
            // fait les étapes de fusions au plus en 2p étapes, mais peut être fait en moins de 2p étapes.
            for (int step = 0; step < 2*threadCount; step++) {
                if(step >= threadCount && isSorted()) {
                    return;
                }

                if (step % 2 == 0) {
                    evenStep();
                } else {
                    oddStep();

                }
                // fait un lock pour voir si tout les Thread ont fini la même étape.
                try {
                    String currentStep = (step%2==0)? "Etape pair" : "Etape impair";
                    lock.await(currentStep);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                // Si oui, Tous les threads (ayant été "réveillé" par le dernier thread ayant eu le lock) continu leurs exécutions
                // et passe à l'étape de recopier leurs données et font de nouveau un lock.
                copyBack();
                try {
                    lock.await("Etape de replacement dans le tableau");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private class LockThread {

        private int initCountThread;
        private int countThreadAwait;
        private Barrier barrier;

        private LockThread(int threadCount) {
            this.initCountThread = threadCount;
            this.countThreadAwait = threadCount;
            this.barrier = new Barrier(); // objet permettant de bloquer les thread si pas fini.
        }

        synchronized void await(String step) throws InterruptedException {
            /*
            * Fonction qui synchronize les threads grâce à synchronized
            * qui permet que un seul thread exécute le code et ne sois pas interrompu pendant.
            */
            countThreadAwait--;

            if (countThreadAwait == 0) {
                System.out.format("%s dernier à rentrer dans le synchronized et va réveiller les autres Threads. %s fini.%n",Thread.currentThread().getName(),step);
                this.notifyAll(); // réveille tous les thread, mais ne passeras pas la main ( le lock)
                                 // aux autres threads tant que le reste du code de la fonction ne soit exécuté.
                barrier.stop = false; // met stop à false pour que quand les autre threads se réveillent ne puissent plus entrées dans le while
                this.barrier = new Barrier(); // recréer un nouvel objet barrier.
                countThreadAwait = initCountThread; // remets le compteur à l'initial pour pouvoir
                                                   // réutiliser toujours le même lock, pour éviter dans créer p.
            } else {

                Barrier myBarrier = this.barrier; // crée un référence vers barrier
                while (myBarrier.stop) {
                    System.out.format("%s s'endort en attendant que les autres Threads finissent leur travail. %s.%n", Thread.currentThread().getName(),step);
                    this.wait();
                }
                System.out.format("%s se réveille car tous les Threads ont fini. %s fini.%n",Thread.currentThread().getName(),step);
            }
        }


        private class Barrier {
            boolean stop;

            private Barrier() {
                this.stop = true;
            }
        }
    }
}
