# !/usr/bin/python3
# -*- coding: utf-8 -*-
from random import randint, random
class Joueur():
    """
    Classe représentant un joueur et effectue les choix de l'IA 
    grâce au fonction qui lui sont dédié.
    """
    def __init__(self, player_id, player_type):
        self.player_type = player_type
        self.probability_AI = 0.55
        self.player_id = player_id

    def get_player_id(self):
        return self.player_id

    def get_player_type(self):
        return self.player_type

    def choose_dice_AI(self, res_dice):
        """
        Fonction ou IA choisit des voies aléatoirement
        """
        s1, s2 = 0, 0
        dice_1 = randint(0, 3)
        dice_2 = randint(0, 3)
        while dice_2 == dice_1:
            dice_2 = randint(0, 3)
        s1 = res_dice[dice_1] + res_dice[dice_2]
        s2 = sum(res_dice) - s1
        return s1, s2


    def choose_routes_AI(self, available_routes):
        """
        Fonction permettant à l'IA de faire un choix aléatoire pour placer son dernier bonzes.
        """
        answer = randint(1, 2)
        return answer

    def decide_stop_AI(self):
        """
        Fonction qui décidé si l'IA arrêt ou non par rapport à la probabilité self.probability_AI.
        Génère un nombre en 0 et 1 à l'aide de la fonction random de la librairie random
        et compare celui-ci à la probabilité self.probability_AI et si cela est plus grand on s'arrête, sinon il peut continuer.
        """
        res = random()
        return res > self.probability_AI