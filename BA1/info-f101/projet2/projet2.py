"""
INFO-F101
Titre : Projet 2 " Vasarely doit se retourner dans sa tombe"
Auteur : JACOBS ALEXANDRE
Matricule : 000408850
Groupe TP : 5
Assitant TP grouep 5 : Cédric Ternon.
Date : 12/10/2015
"""
import turtle as t
from math import sin, cos, pi
t.speed('fastest')
EPS = 1.0e-5

"""
fonction permettant la déformation du pavage.
"""


def deformation(p, c, r):
    """reçoit le point p =(x_p,y_p,z_p), le centre c=(x_c,y_c,z_c),
    et le rayon r tout les trois avant déformation,
    renvoie un point p2 après déformation.
    """
    def distance(p, q):
        """distance entre les 2 points p et q"""
        return ((q[0]-p[0])**2 + (q[1]-p[1])**2 + (q[2]-p[2])**2)**0.5
    d = distance(p, c)
    if d >= r:
        res = p
    else:
        if d > EPS:
            facteur = r/d
        else:
            facteur = 1
        x2 = c[0] + (p[0]-c[0]) * facteur
        y2 = c[1] + (p[1]-c[1]) * facteur
        z2 = c[2] + (p[2]-c[2]) * facteur
        res = (x2, y2, z2)
    return res


"""
fonction hexagone permettant  à partir de 3 losanges d'afficher
un hexagone déformé peint de 3 couleurs centré en un point appelée "centre" et
de longueur L, cela est fait grâce à la méthode goto de turtle et
à la fonction déformation qui renvoie un point déformé.
Calcul des coordonnées des sommets des losanges réalisés
grâce au théorème de Pythagore et l'aide du cosinus.
"""


def hexagone(t, centre, L, col1, col2, col3, deform):

    t.penup()  # ceci lève et permet à turtle de se déplacer sans tracer
              # jusqu'au centre d'un hexagone
    t.goto(deform((centre[0],
                   centre[1], 0))[0], deform((centre[0], centre[1], 0))[1])
    t.pendown()  # rabaisse turtle pour tracer
    i = 0  # i représente le nombre de losange à tracer.
    while i < 3:
        if i == 0:

            t.color(col1)  # permet de colorer le premier losange
            t.begin_fill()  # avec la 1ère couleur
            t.goto(deform((centre[0], centre[1], 0))[0],
                   deform((centre[0], centre[1], 0))[1])
            t.goto(deform((centre[0]+L, centre[1], 0))[0],
                   deform((centre[0]+L, centre[1], 0))[1])
            t.goto(deform((centre[0]+(L/2), centre[1] + cos(pi/6)*L, 0))
                   [0], deform((centre[0]+(L/2), centre[1] + cos(pi/6)*L, 0))[1])
            t.goto(deform((centre[0]-(L/2), centre[1] + cos(pi/6)*L, 0))[0],
                   deform((centre[0]-(L/2), centre[1] + cos(pi/6)*L, 0))[1])
            t.goto(deform((centre[0], centre[1], 0))[0],
                   deform((centre[0], centre[1], 0))[1])
            t.end_fill()

        elif i == 1:
            t.color(col2)  # permet de colorer le deuxième losange
            t.begin_fill()  # avec la 2ème couleur
            t.goto(deform((centre[0]-(L/2), centre[1] + (-1*cos(pi/6)*L), 0))
                   [0], deform((centre[0]-(L/2), centre[1]+(-1*cos(pi/6)*L), 0))[1])
            t.goto(deform((centre[0]+(L/2), centre[1]+(-1*cos(pi/6)*L), 0))
                   [0], deform((centre[0]+(L/2), centre[1]+(-1*cos(pi/6)*L), 0))[1])
            t.goto(deform((centre[0]+L, centre[1], 0))[0],
                   deform((centre[0]+L, centre[1], 0))[1])
            t.goto(deform((centre[0], centre[1], 0))[0],
                   deform((centre[0], centre[1], 0))[1])
            t.end_fill()

        else:
            t.color(col3)  # permet de colorer le troisème losange
            t.begin_fill()  # avec la 3ème couleur
            t.goto((deform((centre[0]-(L/2), centre[1] + cos(pi/6)*L, 0))[0],
                    deform((centre[0]-(L/2), centre[1] + cos(pi/6)*L, 0))[1]))
            t.goto(deform((centre[0]-L, centre[1], 0))[0],
                   deform((centre[0]-L, centre[1], 0))[1])
            t.goto(deform((centre[0]-(L/2), centre[1]+(-1*cos(pi/6)*L), 0))
                   [0], deform((centre[0]-(L/2), centre[1]+(-1*cos(pi/6)*L), 0))[1])
            t.goto(deform((centre[0], centre[1], 0))[0],
                   deform((centre[0], centre[1], 0))[1])
            t.end_fill()
        i += 1
"""
fonction permettant l'affichage d'un pavage hexagonale 
dont les coins inférieur gauche et supérieur droit représentent
la limite de la fenêtre dont tous les hexagones ont leurs centres
non déformé sont dans cette fenêtre.
"""


def pavage(t, inf_gauche, sup_droit, L, col1, col2, col3, deform):
    centre = (inf_gauche, inf_gauche)  # Centre de départ du pavage.
    compteur_ligne = True  # permet de passer à ligne supérieur
                           # pour afficher les hexagones
    while centre[1] < sup_droit:
        while centre[0] < sup_droit:
            hexagone(t, centre, L, col1, col2, col3, deform)
            # permet d'afficher la 1ere ligne du pavage
            centre = (centre[0]+3*L, centre[1])
        if compteur_ligne == True:  # permet de décaler chaque second ligne
                                    # à partir de la 1ére ligne du bas
            centre = (inf_gauche + 3*L/2, centre[1]+sin(pi/3)*L)
            compteur_ligne = False
        elif compteur_ligne == False:  # sinon recommence une ligne
                                        # comme la première ligne
            centre = (inf_gauche, centre[1] + sin(pi/3)*L)
            compteur_ligne = True

inf_gauche = int(input("Entrée l'abscisse du coin inférieur gauche : "))
sup_droit = int(input("Entrée l'abscisse du coin supérieur droit : "))
L = int(input("Entrée la Longueur d'un côté : "))
col1 = input("Entrée la couleur 1 du 1er losange : ")
col2 = input("Entrée la couleur 2 du 2ème losange : ")
col3 = input("Entrée la couleur 3 du 3ème losange : ")
c1 = int(input("Entrée l'abscisse du centre de déformation : "))
c2 = int(input("Entrée l'ordonnée du centre de déformation : "))
c3 = int(input("Entrée la cote du centre de déformation : "))
c = (c1, c2, c3)
r = int(input("Entrée le rayon de la déformation : "))

# définition de la fonction deform envoyée à pavage
deform = lambda p: deformation(p, c, r)
pavage(t, inf_gauche, sup_droit, L, col1, col2, col3, deform)
