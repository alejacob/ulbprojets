"""
INFO-F103
Titre : Devoir 2  
Auteur : JACOBS ALEXANDRE 
Matricule : 000408850 
Date : 23/02/2016
"""
class SortedList:

    def __init__(self):
        self.head = Node("H", None)

    def insert(self, value):
        """
        Insère un noeud dans la liste de façon que la liste soit triée. 
        """
        to_insert = Node(value, None)
        current_node = self.head.get_next()
        previous = self.head
        while current_node is not None and value > current_node.get_value():
            previous, current_node = current_node, current_node.get_next()
        if current_node is not None:
            previous.set_next(to_insert)
            to_insert.set_next(current_node)
        else:
            previous.set_next(to_insert)

    def remove(self, value):
        """
        Enlève un noeud de la liste triée. 
        """
        current_node = self.head
        found = False
        if self.search(value) is None:
            return False
        while current_node is not None and not found:
            if current_node.get_value() == value:
                found = True
            else:
                previous, current_node = current_node, current_node.get_next()
        if found:
            previous.set_next(current_node.get_next())
            del current_node
            return True

    def search(self, value):
        """
        Renvoie le noeud contenant la valeur ou None.
        """
        current_node = self.head
        next_node = current_node.get_next()
        while next_node is not None and next_node.get_value() is not value:
            current_node = next_node
            next_node = current_node.get_next()
        if next_node is not None and next_node.get_value() == value:
            return next_node
        return None

    def __iter__(self):
        current = self.head.get_next()
        while current is not None:
            yield current.get_value()
            current = current.get_next()


class Node:

    def __init__(self, value, next):
        """
        Initialise le noeud avec une valeur et un noeud suivant dans la liste
        """
        self._value = value
        self._next = next

    def get_next(self):
        """
        Renvoie la référence de l'élément suivant.
        """
        return self._next

    def set_next(self, next):
        """
        Permet de modifier la reference vers l'element suivant.
        """
        self._next = next

    def get_value(self):
        """
        Renvoie la valeur de l'élément suivant.
        """
        return self._value

    def set_value(self, value):
        """
        Permet de modifier la valeur de l'élément.
        """
        self._value = value
