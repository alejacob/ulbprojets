"""
INFO-F102
Titre : Titre : Projet Fonctionnement des ordinateurs.
Auteur : JACOBS ALEXANDRE
Matricule : 000408850
Date : 18/12/2015
"""
import argparse
"""
Tous ce qui suit permet de retirer ce qui est entrée dans la ligne de 
commande par l'utilisateur, lors du lancement du programme. 
"""
parser = argparse.ArgumentParser()
parser.add_argument('file')
parser.add_argument('-o', '--output', default="result.out", help="-o, Output, \
permet de  spécifier le nom du fichier de sortie.\
Par défaut le fichier sera result.out")
parser.add_argument('-c', '--checksum', default='h74', help="Cela permet de preciser \
quelle type de code nous intéresse. Les valeurs possibles sont : {parity, h74, h84} \
Le code par défaut doit être le code de Hamming(7;4) i.e. h74.")

parser.add_argument('-a',action ='store_true' ,help="Cela permet de preciser si on veut ajouter \
    le code détecteur.")
parser.add_argument('-r',action='store_true', help="Cela permet de preciser qu’on veut enlever \
    le code détecteur et recréer le fichier d’origine.")
args = parser.parse_args()


def read(file):
    """
    Cette fonction permet de lire le fichier en mode binaire et de renvoyer
    pour chaque caractère de celui-ci un nombre binaire sur 8 bit soit un byte.
    """

    f = open(file, 'rb')
    octets = f.readlines()[0]
    Bytes = []
    for octet in octets:
        Bytes.append(("0"*8 + bin(octet)[2:])[-8:])
    return Bytes


def written(file, List):
    """
    Cette fonction permet d'écrire dans un fichier un parèmetre en mode 'wb'.
    """

    f = open(file, 'wb')
    f.write(bytes(List))
    f.close()


def decode_binary_to_int(binary):
    """
    Cette fonction permet de décoder liste de nombres binaire en une liste d'entier,
    grâce à la méthode int(x,2) qui transforme le string d'un nombre binaire en entier.
    """

    res = []
    for elem in binary:
        res.append(int(elem, 2))
    return res


def bit_parity(seq):
    """
    Cette fonction permet de calculer le bit de parité d'un nombre binaire
    gràce à la methode seq.count("1") qui renvoie le nombre de "1" présent
    dans seq et aussi gràce modulo 2 qui renvoie 1 ou 0
    """

    parity = seq.count('1') % 2
    return str(parity)


def encode_bit_parity(fileIn, fileOut):
    """
    Cette fonction permet d'ajouter le bit de parité à tout les bytes 
    du fichier d'entrée pour ensuite l'écrire dans un fichier de sortie.
    """

    Bytes = read(fileIn)
    bytes = []
    res = ''
    for octet in Bytes:
        if len(octet) == 8:
            parity = bit_parity(octet)+octet[1:]
        else:
            parity = bit_parity(octet)+octet
        bytes.append(parity)
    res = decode_binary_to_int(bytes)
    written(fileOut, res)


def decode_bit_parity(fileIn, fileOut):
    """
    Cette fonction permet dedecoder un fichier d'entrée après lecture de son contenu, 
    et de vérifier si chaque byte du fichier est correct avec le bon bit de parité. 
    Si ils sont tous correct alors ceux ci sont recopier dans le fichier de sortie. 
    Sinon, il y a un message d'error qui s'affhige à l'utilisateur.
    """

    Bytes = read(fileIn)
    seq = []
    compteur = 0
    for elem in Bytes:
        bitparity = elem[0]
        verifparity = bit_parity(elem[1:])
        if bitparity != verifparity:
            compteur += 1
    if compteur != 0:
        print("Can not recreate the original file;")
        print(str(compteur) + " bytes contain errors")
    else:
        print("Original file was restored")
        for elem in Bytes:
            res = elem[1:]
            res = int(elem[1:], 2)
            seq.append(res)
        written(fileOut, seq)


def hamming(seq):
    """
    Cette fonction permet de calculer les bit de vérification à partir de 4 bits
    de données et cela est  réaliser à l'aide de la fonction bit parité.
    """

    v1 = bit_parity(seq[0]+seq[1]+seq[3])
    v2 = bit_parity(seq[0]+seq[2]+seq[3])
    v3 = bit_parity(seq[1]+seq[2]+seq[3])
    code_hamming = v1+v2+seq[0]+v3+seq[1:]
    return code_hamming


def encode_h74(fileIn, fileOut):
    """
    Cette fonction réalise le code de hamming 7.4 qui à partir d'un byte de 8 bit du 
    renvoie deux bytes de 8 bit, et cela est réaliséer grâce à la fonction hamming().
    Cela fait donc le code de hamming sur un fichier d'entrée et écris la suite dans un fichier de sortie
    """

    Bytes = read(fileIn)
    bytes1 = []
    res = []
    for octet in Bytes:
        octet1 = octet[:4]
        octet2 = octet[4:]
        code_hamming_octet1 = "0"+hamming(octet1)
        code_hamming_octet2 = "0"+hamming(octet2)
        bytes1.append(code_hamming_octet1)
        bytes1.append(code_hamming_octet2)
    for octet in bytes1:
        octet = int(octet, 2)
        res.append(octet)
    written(fileOut, res)


def decode_h74(fileIn, fileOut):
    """
    cette fonction permet à paritr  d'un fichier d'entrée de décoder le code de hamming 
    qui a été fait sur celui-ci, et de remarquer si le fichier contient des erreurs 
    et de détecter celles-ci. Cette fonction decode_h74 permet seulement de 
    corriger une seule erreur. Le résultat du décodage ,si cela est possible, 
    est alors écris dans le fichier de sortie spécifiée par l'utilisateur.
    """

    Bytes = read(fileIn)
    res = []
    compteur_erreur_corrigeable = 0
    compteur_erreur_non_corrigeable = 0
    for octet in Bytes:
        v1 = bit_parity(octet[3]+octet[5]+octet[7])
        v2 = bit_parity(octet[3]+octet[6]+octet[7])
        v3 = bit_parity(octet[5]+octet[6]+octet[7])
        code_hamming = v1+v2+octet[3]+v3+octet[5:]
        indice = 0
        if v1 != octet[1]:
            indice += 1
        if v2 != octet[2]:
            indice += 2
        if v3 != octet[4]:
            indice += 4
        if indice > 0 and indice != 4 and indice != 1 and indice != 2:
            if octet[indice] == "1":
                octet = octet[:indice]+"0"+octet[indice+1:]
                compteur_erreur_corrigeable += 1
            else:
                octet = octet[:indice]+"1"+octet[indice+1:]
                compteur_erreur_corrigeable += 1
        elif indice == 4 or indice == 2 or indice == 1:
            compteur_erreur_non_corrigeable += 1
    if compteur_erreur_corrigeable != 0 and compteur_erreur_non_corrigeable == 0:
        print("Original file was restored\n", str(
            compteur_erreur_corrigeable)+"single errors were corrected")
        for elem in Bytes:
            elem = int(elem, 2)
            res.append(elem)
        written(fileOut, res)
    elif compteur_erreur_non_corrigeable != 0:
        print("Can not recreate the original file")
    else:
        print('No single errors in the file')
        for elem in Bytes:
            elem = int(elem, 2)
            res.append(elem)
        written(fileOut, res)


def encode_h84(fileIn, fileOut):
    """
    Cette fonction réalise le code de hamming 8.4 qui tout comme hamming 7.4 
    permet à partir d'un byte de 8 bit renvoie deux bytes de 8 bit dont  
    le bit d'indice zéro est un bit de parité par rapport aux 7 autres 
    bits du code de hamming 8.4 , et cela est réaliséer grâce à la fonction hamming().
    Cela fait donc le code de hamming sur un fichier d'entrée et écris la suite dans un fichier de sortie.
    """

    Bytes = read(fileIn)
    bytes = []
    res = ""
    for octet in Bytes:
        octet1 = octet[:4]
        octet2 = octet[4:]
        code_hamming_octet1 = bit_parity(hamming(octet1)) + hamming(octet1)
        code_hamming_octet2 = bit_parity(hamming(octet2)) + hamming(octet2)
        bytes.append(code_hamming_octet1)
        bytes.append(code_hamming_octet2)
    res = decode_binary_to_int(bytes)
    written(fileOut, res)


def decode_h84(fileIn, fileOut):
    """
    cette fonction permet à paritr  d'un fichier d'entrée de décoder le code de hamming 
    qui a été fait sur celui-ci, et de remarquer si le fichier contient des erreurs 
    et de détecter celles-ci. Cette fonction decode_h84 ne permet seulement de 
    corriger une seule erreur de transmissions tout comme decode_h74, mais peut 
    par contre détecter jusqu'à trois erreurs de transmissions. 
    Le résultat du décodage ,si cela est possible, est alors écris dans 
    le fichier de sortie spécifiée par l'utilisateur.
    """
    compteur_erreur_corrigeable = 0
    compteur_erreur_non_corrigeable=0
    Bytes = read(fileIn)
    res = []
    for octet in Bytes:
        octet_parity = octet[0]
        v1 = bit_parity(octet[3]+octet[5]+octet[7])
        v2 = bit_parity(octet[3]+octet[6]+octet[7])
        v3 = bit_parity(octet[5]+octet[6]+octet[7])
        code_hamming = v1+v2+octet[3]+v3+octet[5:]
        bit_parity_hamming = bit_parity(octet[1:])
        indice = 0
        if v1 != octet[1]:
            indice += 1
        if v2 != octet[2]:
            indice += 2
        if v3 != octet[4]:
            indice += 4
        if indice > 0 and indice != 4 and indice != 1 and indice != 2:
            if octet[indice] == "1":
                octet = octet[:indice]+"0"+octet[indice+1:]
                compteur_erreur_corrigeable += 1
            else:
                octet = octet[:indice]+"1"+octet[indice+1:]
                compteur_erreur_corrigeable += 1
        elif indice == 4 or indice == 2 or indice == 1:
            compteur_erreur_non_corrigeable += 1

        if bit_parity_hamming != octet_parity and compteur_erreur_non_corrigeable != 0:
            compteur_erreur_non_corrigeable += 1
        if bit_parity_hamming != octet_parity and compteur_erreur_non_corrigeable == 0 and compteur_erreur_corrigeable == 0:
            compteur_erreur_corrigeable += 1
            octet = bit_parity_hamming+octet[1:]
        res.append(octet)

    if compteur_erreur_corrigeable != 0 and compteur_erreur_non_corrigeable == 0:
        print("Original file was restored\n", str(
            compteur_erreur_corrigeable)+"single errors were corrected")
        res = decode_binary_to_int(res)
        written(fileOut, res)
    elif compteur_erreur_non_corrigeable != 0:
        print('Can not reacreate the original file')
    else:
        print('Original file was restored.\n No single errors detected.')
        res = decode_binary_to_int(res)
        written(fileOut, res)

fileIn = args.file  # permet de retirer le nom du fichier d'entrée
fileOut = args.output  # permet de retirer le nom du fichier de sortie

# test pour vérifier ce qu'entre l'utilisateur.

if args.a and args.r:
    print('Error -a and -r can not be used together')
else:
    if args.checksum == 'h74' and args.r:
        decode_h74(fileIn, fileOut)
    elif args.checksum == 'h74' and args.a:
        encode_h74(fileIn, fileOut)
    elif args.checksum == 'h84' and args.r:
        decode_h84(fileIn, fileOut)
    elif args.checksum == 'h84' and args.a:
        encode_h84(fileIn, fileOut)
    elif args.checksum == 'parity' and args.r:
        decode_bit_parity(fileIn, fileOut)
    elif args.checksum == 'parity' and args.a:
        encode_bit_parity(fileIn, fileOut)
