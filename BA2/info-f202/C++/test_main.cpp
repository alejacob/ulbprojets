#include "Trie.hpp"
#include "TrieDictionary.hpp"
#include "ListDictionary.hpp"
#include <string>
#include <iostream>
#include <assert.h>
using std::string;

template <typename Iter>
void print(Iter first, Iter last){
    std::cout << "[" << std::endl;
    while (first != last){
        std::cout << "\t" << *first << std::endl;
        ++first;
    }
    std::cout << "]" << std::endl;
}

void test_trie(){
  Trie aTrie = Trie();
  aTrie.insert("bastien");
  aTrie.insert("alexandre");
  aTrie.insert("amandine");
  aTrie.insert("amande");
  aTrie.insert("bon");
  aTrie.insert("bonjour");
  std::cout << "Mots contenus dans le Trie:" << std::endl;
  print(aTrie.begin(), aTrie.end());
  std::cout << "Est-ce que arret est dans Trie:" << 
      (aTrie.search("arret")? "True" : "False") <<std::endl;
  std::cout << "On efface bon." << std::endl;
  aTrie.erase("bon");
  std::cout << "Est-ce que bon est dans Trie:" << 
      (aTrie.search("bon")? "True" : "False") <<std::endl;
  
  std::cout<<"Utilisation de l'itérateur du Trie"<<std::endl;
  Trie::const_Iterator iter;
  iter = aTrie.begin();
  std::cout << "Premier mot suivant l'ordre lexicographique: " << *iter << std::endl;
  ++iter;
  std::cout << "Deuxième mot suivant l'ordre lexicographique: " << *iter << std::endl;  ++iter;
}

void test_string_list(){
    List<string> L;
    List<string>::Place p;
    L.insert("pomme");
    L.insert("annanas");
    L.insert("banane");
    L.insert("poire");
    L.insert("cerise");
    L.insert("avocat");
    std::cout << "Contenu de List<string> L: "<<std::endl; 
    print(L.begin(), L.end());
    
    std::cout << "Est-ce que framboise est dans List<string> L:" << 
      (L.search("framboise")? "True" : "False") <<std::endl;
    std::cout << "On efface poire." << std::endl;
    L.erase("poire");
    std::cout << "Est-ce que poire est dans List<string> L:" << 
      (L.search("poire")? "True" : "False") <<std::endl;

    std::cout<<"Utilisation de l'itérateur de List<string>"<<std::endl;
    p = L.begin();
    ++p;
    std::cout << "Premier mot suivant l'ordre lexicographique: " << *p << std::endl;
    ++p;
    std::cout << "Deuxième mot suivant l'ordre lexicographique: " << *p << std::endl;
}

void test_TrieDictionary(){
  TrieDictionary trie_dict;
  trie_dict.insert("bastien");
  trie_dict.insert("alexandre");
  trie_dict.insert("amandine");
  trie_dict.insert("amande");
  trie_dict.insert("bon");
  trie_dict.insert("bonjour");
  std::cout << "Mots contenus dans le TrieDictionary:" << std::endl;
  print(trie_dict.begin(), trie_dict.end()); //fonctionne car TrieDictionary hérite de Trie
  std::cout << "Est-ce que arret est dans TrieDictionary:" << 
      (trie_dict.search("arret")? "True" : "False") <<std::endl;
  std::cout << "On efface bon." << std::endl;
  trie_dict.erase("bon");
  std::cout << "Est-ce que bon est dans TrieDictionary:" << 
      (trie_dict.search("bon")? "True" : "False") <<std::endl;
  
  std::cout<<"Utilisation de l'itérateur sur TrieDictionary"<<std::endl;
  Dictionary::const_iterator iter = trie_dict.begin();
  std::cout << "Premier mot suivant l'ordre lexicographique: " << *iter << std::endl;
  ++iter;
  std::cout << "Deuxième mot suivant l'ordre lexicographique: " << *iter << std::endl;  ++iter;
}

void test_ListDictionary(){
    ListDictionary L;
    L.insert("pomme");
    L.insert("annanas");
    L.insert("banane");
    L.insert("poire");
    L.insert("cerise");
    L.insert("avocat");
    std::cout << "Contenu de ListDictionary L: "<<std::endl; 
    print(L.begin(), L.end());
    
    std::cout << "Est-ce que framboise est dans ListDictionary L:" << 
      (L.search("framboise")? "True" : "False") <<std::endl;
    std::cout << "On efface poire." << std::endl;
    L.erase("poire");
    std::cout << "Est-ce que poire est dans ListDictionary L:" << 
      (L.search("poire")? "True" : "False") <<std::endl;

    std::cout<<"Utilisation de l'itérateur de ListDictionary"<<std::endl;
    Dictionary::const_iterator p = L.begin();
    ++p;
    std::cout << "Premier mot suivant l'ordre lexicographique: " << *p << std::endl;
    ++p;
    std::cout << "Deuxième mot suivant l'ordre lexicographique: " << *p << std::endl;
}

void test_Fusion_between_TrieDictionary()
{
    TrieDictionary t1;
    TrieDictionary t2;
    t1.insert("bastien");
    t1.insert("alexandre");
    t1.insert("amandine");
    t1.insert("amande");
    t1.insert("bon");
    t1.insert("bonjour");
    std::cout << "Mots contenus dans le TrieDictionary t1:" << std::endl;
    print(t1.begin(), t1.end()); //fonctionne car TrieDictionary hérite de Trie

    t2.insert("pomme");
    t2.insert("annanas");
    t2.insert("banane");
    t2.insert("poire");
    t2.insert("cerise");
    t2.insert("avocat");
    std::cout << "Mots contenus dans le TrieDictionary t2:" << std::endl;
    print(t2.begin(), t2.end()); //fonctionne car TrieDictionary hérite de Trie
    std::cout << "Fusion de t1+= t2" << std::endl;
    t1 += t2;
    std::cout << "Mots contenus dans t1 après fusion" << std::endl;
    print(t1.begin(), t1.end());
}

void test_Fusion_between_TrieDictionary_and_ListDictionary(){
    TrieDictionary t1;
    ListDictionary l1;
    t1.insert("bastien");
    t1.insert("alexandre");
    t1.insert("amandine");
    t1.insert("amande");
    t1.insert("bon");
    t1.insert("bonjour");
    std::cout << "Mots contenus dans le TrieDictionary t1:" << std::endl;
    print(t1.begin(), t1.end()); //fonctionne car TrieDictionary hérite de Trie
    l1.insert("pomme");
    l1.insert("annanas");
    l1.insert("banane");
    l1.insert("poire");
    l1.insert("cerise");
    l1.insert("avocat");
    std::cout << "Mots contenus dans le ListDictionary l1:" << std::endl;
    print(l1.begin(), l1.end());  //fonctionne car ListDictionary hérite de List
    std::cout << "Fusion de t1+= l1" << std::endl;
    t1 += l1;
    std::cout << "Mots contenus dans t1 après fusion" << std::endl;
    print(t1.begin(), t1.end());
}

void test_Fusion_between_ListDictionary()
{
    ListDictionary l1;
    ListDictionary l2;
    l1.insert("bastien");
    l1.insert("alexandre");
    l1.insert("amandine");
    l1.insert("amande");
    l1.insert("bon");
    l1.insert("bonjour");
    std::cout << "Mots contenus dans le ListDictionary l1:" << std::endl;
    print(l1.begin(), l1.end()); //fonctionne car ListDictionary hérite de List
    l2.insert("pomme");
    l2.insert("annanas");
    l2.insert("banane");
    l2.insert("poire");
    l2.insert("cerise");
    l2.insert("avocat");
    std::cout << "Mots contenus dans le ListDictionary l2:" << std::endl;
    print(l2.begin(), l2.end()); //fonctionne car ListDictionary hérite de List
    std::cout << "Fusion de l1+= l2" << std::endl;
    l1 += l2;
    std::cout << "Mots contenus dans l1 après fusion" << std::endl;
    print(l1.begin(), l1.end());
}

void test_Fusion_between_ListDictionary_and_TrieDictionary(){
    ListDictionary l1;
    TrieDictionary t1;
    t1.insert("bastien");
    t1.insert("alexandre");
    t1.insert("amandine");
    t1.insert("amande");
    t1.insert("bon");
    t1.insert("bonjour");
    std::cout << "Mots contenus dans le TrieDictionary t1:" << std::endl;
    print(t1.begin(), t1.end()); //fonctionne car TrieDictionary hérite de Trie
    l1.insert("pomme");
    l1.insert("annanas");
    l1.insert("banane");
    l1.insert("poire");
    l1.insert("cerise");
    l1.insert("avocat");
    std::cout << "Mots contenus dans le ListDictionary l1:" << std::endl;
    print(l1.begin(), l1.end()); //fonctionne car ListDictionary hérite de List
    std::cout << "Fusion de l1+= t1" << std::endl;
    l1 += t1;
    std::cout << "Mots contenus dans l1 après fusion" << std::endl;
    print(l1.begin(), l1.end());
}


void test_Fusion_between_Dictionary_and_Dictionary(){
    ListDictionary l1;
    TrieDictionary t1;
    t1.insert("bastien");
    t1.insert("alexandre");
    t1.insert("amandine");
    t1.insert("amande");
    t1.insert("bon");
    t1.insert("bonjour");
    std::cout << "Mots contenus dans le TrieDictionary t1:" << std::endl;
    print(t1.begin(), t1.end()); //fonctionne car TrieDictionary hérite de Trie
    l1.insert("pomme");
    l1.insert("annanas");
    l1.insert("banane");
    l1.insert("poire");
    l1.insert("cerise");
    l1.insert("avocat");
    std::cout << "Mots contenus dans le ListDictionary l1:" << std::endl;
    print(l1.begin(), l1.end()); //fonctionne car ListDictionary hérite de List
    std::cout << "Fusion de l1+= t1 après en avoir fait des références de Dictionary" << std::endl;
    Dictionary& copy_l1 = l1;
    Dictionary& copy_t1 = t1;
    copy_l1 += copy_t1;
    std::cout << "Mots contenus dans l1 après fusion" << std::endl;
    print(l1.begin(), l1.end());
}

int main() {
    

    std::cout << "Test sur la structure de Trie" << std::endl;
    test_trie();

    std::cout << "Test sur la structure de List<string>" << std::endl;
    test_string_list();

    std::cout << "Test sur la structure de TrieDictionary" << std::endl;
    test_TrieDictionary();

    std::cout << "Test sur la structure de ListDictionary" << std::endl;
    test_ListDictionary();

    std::cout << "Fusion entre 2 TrieDictionary" << std::endl;
    test_Fusion_between_TrieDictionary();

    std::cout << "Fusion entre un TrieDictionary et une ListDictionary" << std::endl;
    test_Fusion_between_TrieDictionary_and_ListDictionary();

    std::cout << "Fusion entre 2 ListDictionary" << std::endl;
    test_Fusion_between_ListDictionary();

    std::cout << "Fusion entre ListDictionary et TrieDictionary" << std::endl;
    test_Fusion_between_ListDictionary_and_TrieDictionary();

    std::cout << "Fusion entre ListDictionary et TrieDictionary utilisant des références de Dictionary"
        << std::endl;
    test_Fusion_between_Dictionary_and_Dictionary();    


}