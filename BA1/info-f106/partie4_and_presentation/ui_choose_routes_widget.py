# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_choose_routes_widget.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_ChooseRoute(object):
    def setupUi(self, ChooseRoute):
        ChooseRoute.setObjectName(_fromUtf8("ChooseRoute"))
        ChooseRoute.resize(430, 252)
        self.gridLayout_2 = QtGui.QGridLayout(ChooseRoute)
        self.gridLayout_2.setObjectName(_fromUtf8("gridLayout_2"))
        self.gridLayout = QtGui.QGridLayout()
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.horizontalLayout_2 = QtGui.QHBoxLayout()
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.verticalLayout = QtGui.QVBoxLayout()
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.combi_dice_1 = QtGui.QCheckBox(ChooseRoute)
        font = QtGui.QFont()
        font.setStrikeOut(False)
        font.setStyleStrategy(QtGui.QFont.PreferDefault)
        self.combi_dice_1.setFont(font)
        self.combi_dice_1.setLayoutDirection(QtCore.Qt.RightToLeft)
        self.combi_dice_1.setObjectName(_fromUtf8("combi_dice_1"))
        self.verticalLayout.addWidget(self.combi_dice_1)
        self.combi_dice_2 = QtGui.QCheckBox(ChooseRoute)
        self.combi_dice_2.setLayoutDirection(QtCore.Qt.RightToLeft)
        self.combi_dice_2.setObjectName(_fromUtf8("combi_dice_2"))
        self.verticalLayout.addWidget(self.combi_dice_2)
        self.combi_dice_3 = QtGui.QCheckBox(ChooseRoute)
        self.combi_dice_3.setLayoutDirection(QtCore.Qt.RightToLeft)
        self.combi_dice_3.setObjectName(_fromUtf8("combi_dice_3"))
        self.verticalLayout.addWidget(self.combi_dice_3)
        self.horizontalLayout.addLayout(self.verticalLayout)
        self.verticalLayout_2 = QtGui.QVBoxLayout()
        self.verticalLayout_2.setObjectName(_fromUtf8("verticalLayout_2"))
        self.combi_dice_4 = QtGui.QCheckBox(ChooseRoute)
        self.combi_dice_4.setObjectName(_fromUtf8("combi_dice_4"))
        self.verticalLayout_2.addWidget(self.combi_dice_4)
        self.combi_dice_5 = QtGui.QCheckBox(ChooseRoute)
        self.combi_dice_5.setObjectName(_fromUtf8("combi_dice_5"))
        self.verticalLayout_2.addWidget(self.combi_dice_5)
        self.combi_dice_6 = QtGui.QCheckBox(ChooseRoute)
        self.combi_dice_6.setObjectName(_fromUtf8("combi_dice_6"))
        self.verticalLayout_2.addWidget(self.combi_dice_6)
        self.horizontalLayout.addLayout(self.verticalLayout_2)
        self.verticalLayout_3 = QtGui.QVBoxLayout()
        self.verticalLayout_3.setObjectName(_fromUtf8("verticalLayout_3"))
        self.go_button = QtGui.QPushButton(ChooseRoute)
        self.go_button.setObjectName(_fromUtf8("go_button"))
        self.verticalLayout_3.addWidget(self.go_button)
        self.count_free_route = QtGui.QLabel(ChooseRoute)
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.count_free_route.setFont(font)
        self.count_free_route.setObjectName(_fromUtf8("count_free_route"))
        self.verticalLayout_3.addWidget(self.count_free_route)
        self.horizontalLayout.addLayout(self.verticalLayout_3)
        self.horizontalLayout_2.addLayout(self.horizontalLayout)
        self.gridLayout.addLayout(self.horizontalLayout_2, 1, 0, 1, 1)
        self.label_2 = QtGui.QLabel(ChooseRoute)
        font = QtGui.QFont()
        font.setPointSize(12)
        font.setBold(True)
        font.setWeight(75)
        self.label_2.setFont(font)
        self.label_2.setAlignment(QtCore.Qt.AlignCenter)
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.gridLayout.addWidget(self.label_2, 0, 0, 1, 1)
        self.gridLayout_2.addLayout(self.gridLayout, 0, 0, 1, 1)

        self.retranslateUi(ChooseRoute)
        QtCore.QMetaObject.connectSlotsByName(ChooseRoute)

    def retranslateUi(self, ChooseRoute):
        ChooseRoute.setWindowTitle(_translate("ChooseRoute", "Form", None))
        self.combi_dice_1.setText(_translate("ChooseRoute", "combi_1", None))
        self.combi_dice_2.setText(_translate("ChooseRoute", "combi_2", None))
        self.combi_dice_3.setText(_translate("ChooseRoute", "combi_3", None))
        self.combi_dice_4.setText(_translate("ChooseRoute", "combi_4", None))
        self.combi_dice_5.setText(_translate("ChooseRoute", "combi_5", None))
        self.combi_dice_6.setText(_translate("ChooseRoute", "combi_6", None))
        self.go_button.setText(_translate("ChooseRoute", "GO", None))
        self.count_free_route.setText(_translate("ChooseRoute", "FREE: 11", None))
        self.label_2.setText(_translate("ChooseRoute", "Choose routes", None))

