# !/usr/bin/python3
# -*- coding: utf-8 -*-
from PyQt4 import QtGui
from ui_rules import Ui_Rules


class Rules(QtGui.QDialog, Ui_Rules):
    """
    Classe qui permet l'instanciation du menu Rules 
    qui contient les règles du jeu.
    """
    def __init__(self,parent=None):
        """
        Fonction qui initialise une instance de la classe lorsqu'elle est utilisé.
        """
        super(Rules, self).__init__(parent)
        self.setupUi(self)
        self.label.setText("""
Rules of the game:
For each way X in the list of the active ways:
— If way X is occupied by a bonze who is not yet at the top,
 the bonze advances of a box.
— If way X is free and that there remains at least 
a bonze to be placed,the player places a bonze 
on the first box of way X.
— Remark 1: If s1 = s2 then this way will be considered twice above.
— Remark 2: The player can choose in which order he considers 
the active ways initially (s1 then s2 or the reverse). 
In particular, if s1 and s2 are all the two free ones
and that the player does not have any more that one bonze 
to be placed, it will be able to choose
if it engages its bonze in the way s1 or s2 sees it.
Stop:
— If the player succeeded in advancing at least a bonze 
or placing at least a bonze, its launched dice is successful,
and it can decide either to continue with new launched or to stop.
— So by-against the player did not advance any bonze nor did not 
place from there, the player is then blocked, and it must stop.
— If when a player palce a bonze in a way X and that 
there was already a pawn with him in this way, 
instead of placing its bonze on the first box, 
it places it where was its pawn.
— If when that the player advances a bonze of a box, 
if it atterit on a box where there was already an unfavourable pawn, 
it advances with the following box, and so on to the first box of free.""")


""" Règle du jeu:
Pour chaque voie x dans la liste des voies actives :
— Si la voie x est occupée par un bonze qui n’est pas encore au sommet,
 le bonze avance d’une case.
— Si la voie x est libre et qu’il reste au moins un bonze à placer,
 le joueur place un bonze sur la première case de la voie x.
— Remarque 1: Si s1 = s2 alors cette voie sera considérée deux fois ci-dessus.
— Remarque 2 : Le joueur peut choisir dans quel ordre il considère les voies actives (d’abord
s1 puis s2 ou l’inverse). En particulier, si s1 et s2 sont toutes les deux libres
et que le joueur n’a plus qu’un bonze à placer, il va pouvoir choisir
 s’il engage son bonze dans la voie s1 ou la voie s2.
Arrêt:
— Si le joueur a réussi à faire avancer au moins un bonze ou à placer au moins un bonze,
son lancé de dés est réussi, et il peut décider de soit continuer avec un nouveau lancé soit
s’arrêter.
— Si par-contre le joueur n’a fait avancer aucun bonze ni n’en a placé, le joueur est alors
bloqué, et il doit s’arrêter.
— Si lorsqu'un joueur palce un bonze dans une voie X et qu'il y avait déjà un pion à lui
 dans cette voie, au lieu de placer son bonze sur la première case, il le place là où était son pion.
— Si lorsque que le joueur avance un bonze d'une case, s'il atterit sur une case
 où il y avait déjà un pion adverse, il avance à la case suivante,
 et ainsi de suite jusqu'à la première case de libre.
"""
