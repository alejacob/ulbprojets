#ifndef _LISTDICTIONARY_HPP
#define _LISTDICTIONARY_HPP

#include <string>
#include "List.hpp"
#include "Dictionary.hpp"

class ListDictionary: public List<std::string>, public Dictionary {
public:
    class const_ListDictIterator;
    typedef const_ListDictIterator const_iterator;

    void insert(const std::string &word) override;
    void erase(const std::string &word) override;
    bool search(const std::string &word) const override;

    ListDictionary& operator+=(const Dictionary& dict) override;

    Dictionary::const_iterator begin() const override;
    Dictionary::const_iterator end() const override;
};

class ListDictionary::const_ListDictIterator: public AbstractConstDictIterator {
public:
    const_ListDictIterator(const_Place iter):
        m_iter(iter) {}

    std::string operator*() const override { return *m_iter; }
    
    const_ListDictIterator& operator++() override { 
        ++m_iter;
        return *this; 
    }

    ListDictionary::const_iterator* clone() const override {
        return new ListDictionary::const_iterator(m_iter);
    }

    bool operator!=(const AbstractConstDictIterator& other) const override {
        return !(*this == other);
    }

    bool operator==(const AbstractConstDictIterator& other) const override {
        const ListDictionary::const_iterator* iter = 
                dynamic_cast<const ListDictionary::const_iterator*>(&other);
        if(iter){
            return m_iter == iter->m_iter;
        }
        return false;
    }
private:
    const_Place m_iter;
};

#endif
