# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_print_dices_widget.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_PrintDices(object):
    def setupUi(self, PrintDices):
        PrintDices.setObjectName(_fromUtf8("PrintDices"))
        PrintDices.resize(175, 208)
        font = QtGui.QFont()
        font.setBold(False)
        font.setWeight(50)
        PrintDices.setFont(font)
        self.gridLayout = QtGui.QGridLayout(PrintDices)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.label = QtGui.QLabel(PrintDices)
        font = QtGui.QFont()
        font.setPointSize(12)
        font.setBold(True)
        font.setWeight(75)
        self.label.setFont(font)
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        self.label.setObjectName(_fromUtf8("label"))
        self.gridLayout.addWidget(self.label, 0, 0, 1, 1)
        self.verticalLayout = QtGui.QVBoxLayout()
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.dice_1 = QtGui.QLabel(PrintDices)
        self.dice_1.setText(_fromUtf8(""))
        self.dice_1.setPixmap(QtGui.QPixmap(_fromUtf8("model_dice_to_place.png")))
        self.dice_1.setObjectName(_fromUtf8("dice_1"))
        self.horizontalLayout.addWidget(self.dice_1)
        self.dice_2 = QtGui.QLabel(PrintDices)
        self.dice_2.setText(_fromUtf8(""))
        self.dice_2.setPixmap(QtGui.QPixmap(_fromUtf8("model_dice_to_place.png")))
        self.dice_2.setObjectName(_fromUtf8("dice_2"))
        self.horizontalLayout.addWidget(self.dice_2)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.horizontalLayout_2 = QtGui.QHBoxLayout()
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))
        self.dice_3 = QtGui.QLabel(PrintDices)
        self.dice_3.setText(_fromUtf8(""))
        self.dice_3.setPixmap(QtGui.QPixmap(_fromUtf8("model_dice_to_place.png")))
        self.dice_3.setObjectName(_fromUtf8("dice_3"))
        self.horizontalLayout_2.addWidget(self.dice_3)
        self.dice_4 = QtGui.QLabel(PrintDices)
        self.dice_4.setText(_fromUtf8(""))
        self.dice_4.setPixmap(QtGui.QPixmap(_fromUtf8("model_dice_to_place.png")))
        self.dice_4.setObjectName(_fromUtf8("dice_4"))
        self.horizontalLayout_2.addWidget(self.dice_4)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        self.gridLayout.addLayout(self.verticalLayout, 1, 0, 1, 1)

        self.retranslateUi(PrintDices)
        QtCore.QMetaObject.connectSlotsByName(PrintDices)

    def retranslateUi(self, PrintDices):
        PrintDices.setWindowTitle(_translate("PrintDices", "Form", None))
        self.label.setText(_translate("PrintDices", "Dices", None))

