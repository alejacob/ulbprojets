Règle du jeu:
    Pour chaque voie x dans la liste des voies actives :
    — Si la voie x est occupée par un bonze qui n’est pas encore au sommet, le bonze avance d’une
    case.
    — Si la voie x est libre et qu’il reste au moins un bonze à placer, le joueur place un bonze sur
    la première case de la voie x.
    Remarque 1: Si s1 = s2 alors cette voie sera considérée deux fois ci-dessus.
    Remarque 2 : Le joueur peut choisir dans quel ordre il considère les voies actives (d’abord
    s1 puis s2 ou l’inverse). En particulier, si s1 et s2 sont toutes les deux libres
    et que le joueur n’a plus qu’un bonze à placer, il va pouvoir choisir s’il engage son bonze dans la voie
    s1 ou la voie s2.
    Arrêt ?
    — Si le joueur a réussi à faire avancer au moins un bonze ou à placer au moins un bonze,
    son lancé de dés est réussi, et il peut décider de soit continuer avec un nouveau lancé soit
    s’arrêter.
    — Si par-contre le joueur n’a fait avancer aucun bonze ni n’en a placé, le joueur est alors
    bloqué, et il doit s’arrêter.
    — Si lorsqu'un joueur palce un bonze dans une voie X et qu'il y avait déjà un pion à lui dans cette voie, au lieu de placer son bonze sur la première case, il le place là où était son pion.
    — Si lorsque que le joueur avance un bonze d'une case, s'il atterit sur une case où il y avait déjà un pion adverse, il avance à la case suivante, et ainsi de suite jusqu'à la première case de libre.