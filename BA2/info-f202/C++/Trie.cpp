#include "Trie.hpp"
#include <iostream>

// Node Copy Constructor
Trie::Node::Node(const Node &node):
    _character(node._character),
    _father(nullptr),
    _first_child(nullptr),
    _brother(nullptr),
    _tagASWord(node._tagASWord) {}

//Copy Constructor
Trie::Trie(const Trie& trie):
    Trie()
{
    std::cout<<"copy constructor"<<std::endl;
    copy_nodes_from(trie._head, _head);
}

void Trie::copy_nodes_from(Node* from, Node* to) {
    /*  Copie tous les noeuds sous le node "from" 
     *  et de les attacher au node "to"
     */
    Node* left_child = from->_first_child;
    if (left_child) {
        Node* new_node = new Node(*left_child);
        new_node->_father = to;
        to->_first_child = new_node;
        copy_nodes_from(left_child, new_node);
    }
    Node* right_child = from->_brother;
    if (right_child) {
        Node* new_node = new Node(*right_child);
        new_node->_father = to->_father;
        to->_brother = new_node;
        copy_nodes_from(right_child, new_node);
    }
}

//Move contructor
Trie::Trie(Trie && trie):
    _head(new Node())
{
    swap(*this, trie);
}

//Destructor
Trie::~Trie(){
    clear(_head);
}

void Trie::clear(Node* node)
{
    while (node->_first_child) {
        clear(node->_first_child);
    }
    if(node != _head) node->_father->_first_child = node->_brother;
    delete node;
}
//Assignement operator
Trie& Trie::operator=(Trie trie){
    swap(*this, trie);
    return *this;  
} 

// Methods
void Trie::insert(const std::string &word) {
    Node* current_node = _head;
    Node* to_insert;
    for (auto iter = word.begin(); iter != word.end(); ++iter){
        // Voir où placer le caractère
        Node* child = current_node->_first_child;
        Node* previous = nullptr;
        
        while ( (child != nullptr) && (child->_character < *iter) ){
            // Avance jusqu'au point d'insertion
            previous = child;
            child = child->_brother;
        }

        if (child == nullptr || child->_character != *iter){
            // Caractère inexistant; insertion nouveau noeud
            to_insert = new Node(*iter);
            to_insert->_father = current_node;
            to_insert->_brother = child;
            if(previous){
                previous->_brother = to_insert;
            }
            else{
                // pas de previous, donc premier enfant
                current_node->_first_child = to_insert;
            }
            child = to_insert;
        }
        current_node = child;
    }
    current_node->_tagASWord = true;
}

void Trie::erase(const std::string &word){
    Node* end = this->find(word);
    if (end)
    {
        if (end->_first_child)
        {
            //cas où le mot est dans un autre mot
            end->_tagASWord = false;
        }
        else
        {
            for (Node* father = end->_father; 
                 end != _head;
                 end = father, father = end->_father)
            {
                if (end->_first_child) return;

                if (father->_first_child == end){
                    //cas si premier fils
                    father->_first_child = end->_brother;
                }
                else{
                    //cas si dernier fils ou fils au milieu 
                    Node* child = father->_first_child;
                    while (child->_brother != end){
                        child = child->_brother;
                    }
                    child->_brother = end->_brother;
                }
                delete end;
            }
        }
    }
}

const Trie::Node* Trie::find(const std::string &word) const {
    Node* child;
    Node* current_node = _head;
    for(auto iter = word.begin(); iter != word.end(); iter++){
        child = current_node->_first_child;
        while ((child != nullptr) && (child->_character != *iter))
            // on se place sur le dernier caractère du mot à chercher si il existe
            child = child->_brother;
        if(child == nullptr)
            //si on ne trouve pas on break pour sortir de la boucle
            break;
        current_node = child;
    }
    if (current_node->_tagASWord != true)
        // si le dernier caractère n'est pas un mot
        current_node = nullptr;
    return current_node;
}

Trie::Node* Trie::find(const std::string &word) {
    return const_cast<Node*>(
        static_cast<const Trie&>(*this).find(word));
}

// External Methods
void swap(Trie& t1, Trie& t2) {
    using std::swap;
    swap(t1._head, t2._head);
}

std::string Trie::const_Iterator::operator*() const {
    size_t length = 0;
    const Node* copy = current;
    while (copy->_father) {
        ++length;
        copy = copy->_father;
    }
    std::string word(length, '\0'); // remplit avec des 0
    copy = current;
    while (copy->_father) {
        word[--length] = copy->_character; // Met les char dans le bon ordre, de la fin jusqu'au début
        copy = copy->_father;
    }
    return word;
}

Trie::const_Iterator& Trie::const_Iterator::operator++() {
    /*permet de trouver le mot suivant
    */
    if(current->_first_child){
        // cas si il ya un fils, donc appel récursif
        current= current->_first_child;
        if(current->_tagASWord)
            //return si c'est un mot
            return *this;
        return ++(*this);
    }
    if(current->_brother){
        // cas si il a un frère, appel récursif.
        current = current->_brother;
        if(current->_tagASWord)
            return *this;
        return ++(*this);
    }

    while(current->_father){
        // cas si pas de fils, pas de frère, on remonte jusqu'à ce qu'on
        // trouve un frère, car on ne peut pas regarder le fils car sinon on cycle
        current = current->_father;
        if (!current->_father){
            // Back to HEAD
            current = nullptr;
            return *this;
        }
        if(current->_brother){
            current = current->_brother;
            if(current->_tagASWord)
                return *this;
            return ++(*this);
        }
    }
    //Ne devrait jamais arriver ici
    return *this;
}            

Trie::const_Iterator Trie::begin() const { return ++const_Iterator(_head);}
Trie::const_Iterator Trie::end() const { return const_Iterator();}