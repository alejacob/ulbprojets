#ifndef _BANK_HPP_
#define _BANK_HPP_

#include <string>

class Bank
{
private:
    class _Account
    {
    public:
        _Account(std::string name, float amount);
        _Account(const _Account& other) = default;
        _Account& operator=(const _Account& other) = default;
        float get_balance () const ;
        void set_balance (float amount);
        const std::string& get_client() const;
        void set_client(std::string name);
        _Account* get_next_account() const;
        void set_next_account(_Account* next);
    
    private:
        std::string _client;
        float _balance;
        _Account* _next;
    };

    _Account* _head;

    _Account* get_last_account() const;
    bool hasAccount(const std::string& name) const;
    _Account* getAccountByName(const std::string& name) const;
    _Account* getPreviousAccount(_Account* account) const;

public:
    Bank();
    Bank(const Bank& other);
    Bank& operator=(Bank other);
    ~Bank();

    float withdraw(const std::string& name, float amount);
    float deposit(const std::string& name, float amount);
    void createAccount(std::string name, float amount);
    float deleteAccount(const std::string& name);
    void mergeAccounts(const std::string& name_client1, const std::string& name_client2);
    void displayAccounts() const;
};
    
#endif