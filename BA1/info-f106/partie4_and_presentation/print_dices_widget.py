# !/usr/bin/python3
# -*- coding: utf-8 -*-
from PyQt4 import QtGui
from ui_print_dices_widget import Ui_PrintDices


class PrintDices(QtGui.QDialog, Ui_PrintDices):
    """
    Classe permettant l'affichage des dés, en fonction du résultat du jet.
    """
    def __init__(self,parent=None):
        """
        Fonction qui initialise une instance de la classe lorsqu'elle est utilisé.
        """
        super(PrintDices, self).__init__(parent)
        self.setupUi(self)

    def updatePrintDices(self):
        """
        Mets à jour l'affichage en fonction de résultat de jets de dés.
        """
        sender = self.sender()
        dices_icon = {1: "dice_one.png", 2: "dice_two.png", 3: "dice_three.png", 4: "dice_four.png", 5: "dice_five.png", 6: "dice_six.png"}
        res_dices = sorted(sender.jeu.res_dices)
        self.dice_1.setPixmap(QtGui.QPixmap(dices_icon[res_dices[0]]))
        self.dice_2.setPixmap(QtGui.QPixmap(dices_icon[res_dices[1]]))
        self.dice_3.setPixmap(QtGui.QPixmap(dices_icon[res_dices[2]]))
        self.dice_4.setPixmap(QtGui.QPixmap(dices_icon[res_dices[3]]))
