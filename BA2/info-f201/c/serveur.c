#include <unistd.h>
#include <error.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <signal.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>

#define MYPORT 5555
#define BACKLOG 0 //no limit connection
#define MAX_CONNECTIONS 4

void sigchld_handler(int s){
    while(wait(NULL)>0);
}

typedef struct thread_data_t{
    int id_player;
    int socket_fd;
} thread_data_t;

int totalconnections = 0;
int total_count_reply[MAX_CONNECTIONS+1] = {0,0,0,0,0};
int end_game_count = 0;
char sentences[MAX_CONNECTIONS][2000];
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t condition = PTHREAD_COND_INITIALIZER;

void* game(void* args);

int main(void){
    int server_socket_fd, client_socket_fd;
    struct sockaddr_in server, client;
    struct sigaction sa;
    socklen_t sin_size;
    int yes = 1;
    pthread_t thread_client[MAX_CONNECTIONS];
    thread_data_t tdata[MAX_CONNECTIONS];

    //socket initialisation
    server_socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if( server_socket_fd == -1){
        perror("Could not create socket");
        exit(1);
    }
    printf("%s\n", "Socket created");

    // setsockopt pour ne pas attendre un second bind
    if(setsockopt(server_socket_fd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1){
        perror("setsockopt");
        exit(1);
    }

    server.sin_family = AF_INET; // host byte order
    server.sin_port = htons(MYPORT); // short, network byte order
    server.sin_addr.s_addr = INADDR_ANY;  // automatically fill with my IP
    memset(&(server.sin_zero), '\0', 8);

    //bind avec le socket
    if( bind(server_socket_fd, (struct sockaddr *)&server, sizeof(server)) < 0){
        perror("bind failed. ERROR");
        exit(1);
    }
    printf("%s\n","Bind done");

    //listen si il y a des connexions par le socket
    if(listen(server_socket_fd, BACKLOG) == -1){
        perror("listen");
        exit(1);
    }

    //supprime les processus mort(inactif)
    sa.sa_handler = sigchld_handler;
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = SA_RESTART;
    if(sigaction(SIGCHLD, &sa, NULL) == -1){
        perror("sigaction");
        exit(1);
    }

    //main accpet() loop
    while(1)
    {
        sin_size = sizeof(struct sockaddr_in);
        client_socket_fd = accept(server_socket_fd, (struct sockaddr *)&client, &sin_size);
        if(client_socket_fd < 0){
            perror("accept failed");
            continue;
        }
        else{
            if(totalconnections < MAX_CONNECTIONS){ // check si nombre de joueur souhaité est atteint
                tdata[totalconnections].id_player = totalconnections;
                tdata[totalconnections].socket_fd = client_socket_fd;
                if(pthread_create(&thread_client[totalconnections], NULL, game, (void*)&tdata[totalconnections]) < 0){
                    perror("Thread creation failed");
                }
                totalconnections++;
            }
            else{ // sinon, on envoie un signal au client pour lui dire que c'est complet.
                sleep(1);
                if(send(client_socket_fd,"full", strlen("full"),0) < 0){
                    perror("send");
                }
            }
        }
    }
    pthread_mutex_destroy(&mutex);
    pthread_cond_destroy(&condition);
    close(server_socket_fd);
    return 0;
}

//Fonction qui gère le thread d'un client en jeu
void* game(void* args){

    thread_data_t *tdata = (thread_data_t *)args;
    int client_socket_fd = tdata->socket_fd;
    int player_id = tdata->id_player;
    char* message;
    char reply_client[2000];
    int read_size, stage = 0;
    const char * message_to_send[6] = {
    "Veuillez entrer le premier mot(sujet) \n",
    "Veuillez entrer le deuxième mot(adjectif)\n",
    "Veuillez entrer le troisième mot(verbe) \n",
    "Veuillez entrer le quatrième mot(complément de phrase) \n",
    "Veuillez entrer le cinquième mot(adjectif)\n",
    "Partie terminée \nVoici les résultats \n"
    };
    message = "Bienvenue dans le jeu du cadavre exquis \n";
    if(send(client_socket_fd, message, strlen(message), 0) == -1){
        perror("send");
    }
    message = "Attends d'autres joueurs(4 joueurs requis) avant le début d'une partie.\n";
    if(send(client_socket_fd, message, strlen(message), 0) == -1){
        perror("send");
    } 
    pthread_mutex_lock(&mutex);
    while(totalconnections < MAX_CONNECTIONS){ // endors les thread tant que le nombre joueur requis n'est pas atteint  
        pthread_cond_wait(&condition, &mutex);
    }
    if(totalconnections == MAX_CONNECTIONS){
        printf("%s\n", "Partie en cours");
    }
    pthread_cond_broadcast(&condition);
    pthread_mutex_unlock(&mutex);
    sleep(1);
    message = "Le jeu va commencer! \n";
    if(send(client_socket_fd, message, strlen(message), 0) == -1){
        perror("send");
    }
    sleep(1);

    while(stage <= MAX_CONNECTIONS) // car 5 choses à donner(sujet, adjectif,verbe, complément de phrase, adjectif)
    {
        // envoie de ce qu'on veut demander au client en fonction de l'étape
        if(send(client_socket_fd, message_to_send[stage], strlen(message_to_send[stage]), 0) == -1){
            perror("send");
        }
        // récupère ce que le client a entré
        read_size = recv(client_socket_fd, reply_client, 2000, 0);
        if(read_size < 0){
            perror("recv failed");
            exit(1);
        }
        reply_client[read_size] = '\0';
        int idx = (player_id+stage)%MAX_CONNECTIONS; // permet de savoir l'index du tableau où on doit place le mot
        strcat(sentences[idx], reply_client); // concaténation de ce que le client a entré avec la phrase ou le mot doit etre mis
        strcat(sentences[idx], " ");
        pthread_mutex_lock(&mutex);
        total_count_reply[stage]++;
        while(total_count_reply[stage] < MAX_CONNECTIONS){ // tant que nombre de réponse souhaité pas atteint, on endors le thread courant
            pthread_cond_wait(&condition, &mutex);
        }
        pthread_cond_broadcast(&condition); // réveil les threads lorsque le nombre souhaité est atteint.
        pthread_mutex_unlock(&mutex);
        stage++;
    }
    //envoie du message de fin de partie
    if(send(client_socket_fd, message_to_send[stage], strlen(message_to_send[stage]), 0) == -1){
        perror("send");
    }
    sleep(1);
    int i;
    for(i = 0; i <= MAX_CONNECTIONS; i++){// envoie chaque phrase crée au client.
        if(send(client_socket_fd,sentences[i],strlen(sentences[i]), 0) < 0){
            perror("send");
        }
        sleep(1);
    }
    pthread_mutex_lock(&mutex);
    ++end_game_count;
    if(send(client_socket_fd, "Déconnexion \nLe serveur va être fermé\n",strlen("Déconnexion \nLe serveur va être fermé\n"), 0) < 0){
        perror("send");
    }
    if(end_game_count == MAX_CONNECTIONS){
        printf("%s\n", "Partie terminée");
        printf("%s\n%s\n", "Déconnexion", "Le serveur est fermé");
        exit(0);
    }
    pthread_mutex_unlock(&mutex);
    pthread_exit(NULL);
}

