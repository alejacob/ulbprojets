# !/usr/bin/python3
# -*- coding: utf-8 -*-
from PyQt4 import QtGui
from ui_about import Ui_About


class About(QtGui.QDialog, Ui_About):
    """
    Classe qui permet l'instanciation du menu About 
    qui contient les crédits/copyright des images et une courte présentation
    du jeu.
    """
    def __init__(self,parent=None):
        """
        Fonction qui initialise une instance de la classe lorsqu'elle est utilisé.
        """
        super(About, self).__init__(parent)
        self.setupUi(self) # permet d'initialiser le widget avec le fichier 
        # générer à l'aide de QtDesigner
        self.label.setText("""
"Can't Stop is a board game designed by Sid Sackson 
originally published by Parker Brothers in 1980, 
and was long out of print in the United States.
The goal of the game is to "claim" (get to the top of) 
three of the columns before any of the other players can. 
But the more that the player risks rolling 
the dice during a turn, the greater the risk 
of losing the advances made during that turn.
The game equipment consists of four dice, a board, 
a set of eleven markers for each player, 
and three neutral-colored markers.
The board consists of eleven columns of spaces, 
one column for each of the numbers 2 through 12. 
The columns (respectively) have 3, 5, 7, 9, 11, 13, 11, 9, 7, 5 
and 3 spaces each.
The number of spaces in each column roughly corresponds 
to the likelihood of rolling them on two dice."
(From https://en.wikipedia.org/wiki/Can't_Stop_(board_game))
All icons are taken from https://www.easyicon.net/language.en/
The image of welcome is taken from https://www.jeuxdenim.be/jeu-CantStop
The win image is taken from https://twitter.com/contestalert (profil picture)"""
)