"""
INFO-F106
Projet d'année BA1: Can't Stop
Partie 1
Auteur : JACOBS ALEXANDRE
Matricule : 000408850
Date : 17/11/2016
"""
#!/usr/bin/python3
# -*- coding: utf-8 -*-
from random import randint
import os

height = dict()
bonze = dict()
state_way = dict()


def init():
    """
    Fonction initialisant des variables(height, bonze, state_way) et les déclarant comme des variables globales.
    height : est un dictionnaire qui contient la hauteur maximale des voies en valeurs et le numéro de la voie en clé.
    bonze: est un dictionnaire qui contient au maximun 3 clés (qui représente les 3 bonzes qu'un joueur a),
    les clées représentent le numéro de voie et la valeur la hauteur à laquel le bonze se trouve sur la voie.
    state_way: est un dictionnaire d'état qui indique si une voie est fermé, c'est à dire si la voie est occupé
    par un bonze qui est arrivé au sommet de la voie, sinon celle ci est libre qui est représenté par la valeur 0
    et une voie fermée par la valeur 1.
    Cette fonction renvoie None par défaut.
    """
    global height, bonze, state_way  # global permet de changer la valeur d'une variable à n'importe quel niveau dans le code.
    height = {2: 3, 3: 5, 4: 7, 5: 9, 6: 11, 7: 13, 8: 11, 9: 9, 10: 7, 11: 5, 12: 3}
    bonze = {}
    state_way = {2: 0, 3: 0, 4: 0, 5: 0, 6: 0, 7: 0, 8: 0, 9: 0, 10: 0, 11: 0,
                 12: 0}  # dict qui représent si voie libre(0) ou fermé(1)


def display_board():
    """
    Fonction permettant l'affichage d'un plateau de jeu(board) avec les bonzes d'un joueur s'il y a des bonzes sur les voies.
    Cette fonction renvoie None par défaut
    """
    os.system("cls" if os.name == "nt" else "clear") # permet de rafraichir la console avant affichage du board
    max_height_way = max(height.values())
    for line_number in reversed(range(1,
                                      max_height_way + 1)):  # parcourt jusqu'à 1 depuis la plus grande hauteur grâce à reversed, boucle créant l'affiche du jeu et des hauteur.
        print("{:^3}".format(line_number),
              end="")  # format permet de bien afficher sur 3 caractère centré: les chiffres,  "-" et "0", end="" permet de continuer sur la même ligne.
        for way in height:
            if line_number > height[way]:
                print("{:^3}".format("-"), end="")
            elif way in bonze and bonze[way] == line_number:
                print("{:^3}".format(0), end="")
            else:
                print("{:^3}".format(" "), end="")
        print()
    print(" " * 3, end="")  # permet un espace de 3 caractères par rapport à la colonne des hauteurs de voie.
    for way in height:  # boucle affichant les numéros de voies.
        print("{:^3}".format(way), end="")
    print()


def reset_bonzes():
    """
    Fonction réinitialisant le dictionnaire bonze à un dictionnaire vide.
    Cette fonction renvoie None par défaut.
    """
    if bonze:  # vérifie si le dictionnaire contient des éléments.
        bonze.clear()  # clear permet de retirer tout les clé-valeur du dictionnaire en rendant un dictionnaire vide.


def throw_dice():
    """
    Fonction permettant de simuler le jet de dé en créeant des nombres aléatoires
    à l'aide de la fonction randint de la librairie random
    :return: un tuple de 4 entier correspondant au jet de 4 dés.
    """
    r1 = randint(1, 6)
    r2 = randint(1, 6)
    r3 = randint(1, 6)
    r4 = randint(1, 6)
    return r1, r2, r3, r4


def choose_dice(res_dice):
    """
    Fonction renvoyant sous la forme d'un tuple la somme des 4 dés pris 2 à 2.
    La variable s1 étant la somme des 2 dés choisit part l'utilisateur et
    la variable s2 étant la somme des 2 dés restant.
    :param res_dice: contient ce que renvoie la fonction throw_dice, un tuple de 4 entiers.
    :return: un tuple de 2 entiers, correspondant à 2 voies du board.
    """
    s1 = 0
    s2 = 0
    # affiche le jet de chaque dé.
    for dice in range(4):
        print("Dé {} = {}".format(dice + 1, res_dice[dice]))
    valid = False  # variable sentinelle permettant l'arrêt du while.
    # vérification que ce l'utilisateur a entré est correct,
    # sinon on lui redemande jusqu'à ce que l'utilisteur entre correctement les données
    while not valid:
        number_dice = input(
            "Entrer les numéros (entre 1 et 4) des deux premiers dés à additionner séparés d'un espace:\n")
        number_dice = number_dice.split(" ")
        if len(number_dice) != 2:
            print("Erreur d'encodage, veuillez respecter les consignes d'encodage")
        elif not all(number.isdigit() for number in number_dice):
            print("Erreur d'encodage, veuillez respecter les consignes d'encodage")
        elif not all(1 <= int(number) <= 4 for number in number_dice):
            print("Erreur d'encodage, veuillez respecter les consignes d'encodage")
        elif len(set(number_dice)) == 1:
            print("Erreur d'encodage, veuillez respecter les consignes d'encodage")
        else:
            valid = True

    s1 = sum(res_dice[int(idx) - 1] for idx in number_dice)  # sum permet de sommer chaque dé choisit par l'utilisateur
    s2 = sum(res_dice) - s1
    return s1, s2


def ask_int_between_bounds(prompt, low, high):
    """
    Fonction permettant de vérifier si l'entrée de l'utilisateur correspond bien à un entier
    et qu'il respecte les consignes d'encodage donné lors de la demande d'encodage à l'utilisateur.
    :param prompt: contient le string d'encodage qui est fournit à l'utilisateur lors de la demande d'encodage.
    :param low: borne  basse de l'entier à encoder
    :param high: borne haute de l'entier à encoder
    :return: renvoie l'entier correctement entrée.
    """
    valid = False
    while not valid:
        answer = input(prompt)
        # try/ except permet de capturer une erreur si ce qu'on vérifie dans le try,
        # si cela n'est pas correct et correspond à l'erreur mentioné dans le except
        try:
            answer = int(answer)
        except ValueError:
            print("Erreur d'encodage")
        else:
            # vérifie si l'entier est entre les deux bornes.
            if not low <= answer <= high:
                print("Erreur d'encodage")
                valid = False
            else:
                valid = True
    return answer


def move_bonzes(voies):
    """
    Fonction permettant l'avancement/ placements des bonzes d'un joueur selon les règles du jeu.
    Règle du jeu:
    Pour chaque voie x dans la liste des voies actives :
    — Si la voie x est occupée par un bonze qui n’est pas encore au sommet, le bonze avance d’une
    case.
    — Si la voie x est libre et qu’il reste au moins un bonze à placer, le joueur place un bonze sur
    la première case de la voie x.
    Remarque 1: Si s1 = s2 alors cette voie sera considérée deux fois ci-dessus.
    Remarque 2 : Le joueur peut choisir dans quel ordre il considère les voies actives (d’abord
    s1 puis s2 ou l’inverse). En particulier, si s1 et s2 sont toutes les deux libres
    et que le joueur n’a plus qu’un bonze à placer, il va pouvoir choisir s’il engage son bonze dans la voie
    s1 ou la voie s2.
    Arrêt ?
    — Si le joueur a réussi à faire avancer au moins un bonze ou à placer au moins un bonze,
    son lancé de dés est réussi, et il peut décider de soit continuer avec un nouveau lancé soit
    s’arrêter.
    — Si par-contre le joueur n’a fait avancer aucun bonze ni n’en a placé, le joueur est alors
    bloqué, et il doit s’arrêter.
    :param voies: contient le résultat de la fonction choose_dice, qui correspond à 2 entiers représentant 2 voies.
    :return: True si on a bougé/placé un bonze, sinon False.
    """
    res_bool = False
    # vérifie si aucune des voie est dans le dict de bonze, si c'est un dernier bonze a placé et si la voie n'est pas identique.
    if all(voie not in bonze for voie in voies) and len(bonze) == 2 and voies[0] != voies[1]:
        gamer_choose = ask_int_between_bounds("Voulez-vous placer le dernier bonze dans la voie "
                                              "\n 1) {} ? \n 2) {} ? \n".format(voies[0], voies[1]), low=1, high=2)
        if gamer_choose == 2:
            voies = reversed(voies)

    for voie in voies:
        if not state_way[voie] and voie in bonze and bonze[voie] != height[voie]:
            bonze[voie] += 1
            res_bool = True
            if bonze[voie] == height[voie]:
                state_way[voie] = 1
        elif voie not in bonze and len(bonze) != 3:
            bonze[voie] = 1
            res_bool = True
    return res_bool


def check_top():
    """Fonction qui renvoie True si les trois bonzes sont chacun au sommet d’une voie, False sinon."""
    res_bool = True
    for way in bonze:
        if bonze[way] != height[way] or len(bonze) != 3:
            res_bool = False
    return res_bool


def game_round():
    """
    Fonction qui correspond au premier tour de jeu du premier joueur.
    Renvoie True si le joueur arrive à gagner au premier round, False sinon.
    Lors d’un round, des jets de dés seront effectués jusqu’à ce que le
    joueur soit bloqué, décide de s’arrêter ou gagne le jeu.
    """
    init()
    reset_bonzes()
    display_board()
    res_dice = throw_dice()
    voies = choose_dice(res_dice)
    res_move_bonze = move_bonzes(voies)
    res_bool = res_move_bonze
    bonzes_on_top = False
    valid = False
    display_board()
    while res_move_bonze and not bonzes_on_top and ask_int_between_bounds("Voulez vous : \n 1) continuer ? \n 2) arrêter ? \n", low=1,
                                 high=2) == 1:
        res_dice = throw_dice()
        voies = choose_dice(res_dice)
        res_move_bonze = move_bonzes(voies)
        bonzes_on_top = check_top()
        display_board()

    if bonzes_on_top:
        print("Vous avez gagné")
    elif not res_move_bonze:
        print("Vous êtes bloqué !")
    else:
        print("Vou avez décidé d'arrêter !")
    return res_bool


# condition pour dire si ce code est exécuté en tant que script principal (appelé directement avec Python et pas importé), alors exécuter cette fonction.
if __name__ == "__main__":
    game_round()
