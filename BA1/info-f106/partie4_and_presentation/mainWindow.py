# !/usr/bin/python3
# -*- coding: utf-8 -*-
from PyQt4 import QtGui
from about_gui import About
from help_gui import Rules
from ui_main_window import Ui_MainWindow


class MainWindow(QtGui.QMainWindow, Ui_MainWindow):
    """
    Classe représentant la fenêtre principale qui contiendra 
    les différents vues(widget) que le programme utilisera.
    """

    def __init__(self, parent=None):
        """
        Fonction qui initialise une instance de la classe lorsqu'elle est utilisé.
        """
        super(MainWindow, self).__init__(parent)
        self.setupUi(self)
        self.actionAbout.triggered.connect(self.about)
        self.actionRules.triggered.connect(self.rules)


    def closeEvent(self,event):
        """
        Fonction overload qui permet de faire un popup pour avertir le client.
        """
        message = "Are you sure you want to exit the game?"
        answer= QtGui.QMessageBox.question(self, 'Quit Message',
                                           message, QtGui.QMessageBox.Yes, QtGui.QMessageBox.No)
        if answer == QtGui.QMessageBox.No:
            event.ignore()

    def about(self):
        """
        Fonction qui affiche la fenêtre pour le menu About.
        """
        about_dlg = About(self)
        about_dlg.show()


    def rules(self):
        """Fonctio qui affiche la fenêtre pour le menu Rules."""
        rules_dlg = Rules(self)
        rules_dlg.show()