# !/usr/bin/python3
# -*- coding: utf-8 -*-
from random import randint, random, uniform
class Joueur():
    """
    Classe représentant un joueur et effectue les choix de l'IA 
    grâce au fonction qui lui sont dédié.
    """
    HEIGHT = {2: 3, 3: 5, 4: 7, 5: 9, 6: 11, 7: 13, 8: 11, 9: 9, 10: 7, 11: 5, 12: 3}    
    proba_way = {2: 1/36, 3: 1/18, 4: 1/12, 5: 1/9, 6: 5/36, 7: 1/6, 8: 5/36, 9: 1/9, 10: 1/12, 11: 1/18, 12: 1/36}
    score_to_stop_case = {2: 12, 3: 1, 4: 6, 5: 7, 6: 6, 7: 3, 8: 10, 9: 7, 10: 11, 11: 15, 12: 1}
    
    def __init__(self, player_id, player_type):
        if player_type == "Human":
            self.player_type = player_type
        else:
            self.player_type = player_type[0]
            self.level_player = player_type[1]
            self.probability_dumb_AI = 0.55
        self.bonzes = dict()
        self.pawn = dict()
        self.player_id = player_id
        self.count_jet = 0
        self.count_move = 0


    def get_player_id(self):
        return self.player_id

    def get_player_type(self):
        return self.player_type

    def choose_dice_AI(self, res_dice, blocked_routes):
        self.count_jet += 1
        if self.level_player == 0:
            return self.dumb_choose_dice_AI(res_dice)
        elif self.level_player == 1:
            return self.clever_choose_dice_AI(res_dice, blocked_routes)
        else:
            return self.genius_choose_dice_AI(res_dice, blocked_routes)
    
    def dumb_choose_dice_AI(self, res_dice):
        """
        Fonction ou IA choisit des voies aléatoirement
        """
        s1, s2 = 0, 0
        dice_1 = randint(0, 3)
        dice_2 = randint(0, 3)
        while dice_2 == dice_1:
            dice_2 = randint(0, 3)
        s1 = res_dice[dice_1] + res_dice[dice_2]
        s2 = sum(res_dice) - s1
        return s1, s2

    def clever_choose_dice_AI(self, res_dice, blocked_routes):
        """
        Fonction permettant le choix des voies par rapport 
        à une fonction de score et par une distribution de probabilité.
        """
        all_possibilities = self.all_possibility(res_dice, blocked_routes)
        score_all_ways = self.score_function_choose_2_dice(all_possibilities)
        score_all_ways = self.transform_to_list_heap(score_all_ways)
        sum_score_total = 0
        for i in range(len(score_all_ways)):
            sum_score_total += score_all_ways[i][0]
        choice = uniform(0, sum_score_total)
        if 0 < choice <= score_all_ways[0][0]:
            return score_all_ways[0][1]
        for i in range(1,len(score_all_ways)):
            sum = 0
            for item in score_all_ways[0:i]:
                sum += item[0]
            if i == len(score_all_ways) - 1 and sum < choice <= sum_score_total:
                return score_all_ways.pop()[1]
            else:
                if sum < choice <= sum + score_all_ways[i][0]:
                    return score_all_ways.pop(i)[1]

    def genius_choose_dice_AI(self, res_dice, blocked_routes):
        """
        Fonction permettant le choix de voies à l'aide d'une fonction de score 
        dont le couple de voie choisit maximise la fonction de score.
        """
        all_possibilities = self.all_possibility(res_dice, blocked_routes)
        score_all_ways = self.score_function_choose_2_dice(all_possibilities)
        score_all_ways = self.transform_to_list_heap(score_all_ways)
        return score_all_ways.pop()[1]

    def choose_routes_AI(self, available_routes):
        if self.level_player == 0:
            return self.dumb_choose_routes_AI(available_routes)
        elif self.level_player == 1:
            return self.clever_choose_routes_AI(available_routes)
        else:
            self.genius_choose_routes_AI(available_routes)

    def dumb_choose_routes_AI(self, available_routes):
        """
        Fonction permettant à l'IA de faire un choix aléatoire pour placer son dernier bonzes.
        """
        answer = randint(1, 2)
        return (available_routes[answer-1],)

    def clever_choose_routes_AI(self, available_routes):
        height_way1 = self.height_diff(available_routes[0])
        height_way2 = self.height_diff(available_routes[1])
        score_ways = {}
        score_ways[self.proba_way[available_routes[0]]**height_way1] = available_routes[0]
        score_ways[self.proba_way[available_routes[1]]**height_way2] = available_routes[1]
        max_key = max(score_ways) 
        return (score_ways[max_key],)

    def genius_choose_routes_AI(self, available_routes):
        height_way1 = self.height_diff(available_routes[0])
        height_way2 = self.height_diff(available_routes[1])
        score_ways = {}
        score_ways[self.proba_way[available_routes[0]]**height_way1] = available_routes[0]
        score_ways[self.proba_way[available_routes[1]]**height_way2] = available_routes[1]
        max_key = max(score_ways) 
        return (score_ways[max_key],)

    def decide_stop_AI(self):
        if self.level_player == 0:
            return self.dumb_decide_stop_AI()
        else:
            return self.smart_decide_stop_AI()

    def dumb_decide_stop_AI(self):
        """
        Fonction qui décidé si l'IA arrêt ou non par rapport à la probabilité self.probability_dumb_AI.
        Génère un nombre en 0 et 1 à l'aide de la fonction random de la librairie random
        et compare celui-ci à la probabilité self.probability_dumb_AI et si cela est plus grand on s'arrête, sinon il peut continuer.
        """
        res = random()
        return res > self.probability_dumb_AI

    def smart_decide_stop_AI(self):
        """
        Fonction d'arret se basant sur une fonction de score.
        """
        scoreStop = self.score_stop()
        if scoreStop > 35:
            return True
        elif self.count_jet > 4:
            return True
        return False

    def combination_2(self, iterable):
        """Retourne toutes les combinaisons de 2 éléments parmis les itérables"""
        combinations = []
        for idx, first in enumerate(iterable[:-1]):
            for second in iterable[idx + 1:]:
                combinations.append((first, second))
        return combinations

    def score_function_choose_2_dice(self, available_routes):
        score_all_ways = {}
        for available_route in available_routes:
            w1, w2 = available_route
            height_way1 = self.height_diff(available_route[0])
            height_way2 = self.height_diff(available_route[1])
            score_all_ways[available_route] = (self.proba_way[w1]**height_way1,\
                        self.proba_way[w2]**height_way2)
        return score_all_ways

    def transform_to_list_heap(self, score_all_ways):
        list_heap = []
        for key in score_all_ways:
            s = sum(score_all_ways[key])
            t = (s, key)
            list_heap.append(t)
        list_heap.sort()
        return list_heap

    def height_diff(self, available_route):
        """
        calcul la différence de hauteur
        """
        if available_route in self.pawn:
            height_way = self.HEIGHT[available_route] - self.pawn[available_route]
        else:
            height_way = self.HEIGHT[available_route]
        return height_way

    def all_possibility(self, res_dices, blocked_routes):
        """
        Fonction qui évalue tout les possibilité possible par rapport au lancé de dé et élémine les couples de voies bloquer.
        """
        possibility = []
        if len(self.bonzes) == 3:
            test = lambda route: route in self.bonzes and route not in blocked_routes
        else:
            test = lambda route: route not in blocked_routes
        for dices in self.combination_2(res_dices):
            route_1 = dices[0]+dices[1]
            route_2 = sum(res_dices) - route_1
            if test(route_1) or test(route_2) and (route_1, route_2 not in possibility):
                possibility.append((route_1, route_2))
        return possibility

    def bonze_on_top(self):
        for route in self.bonzes:
            if self.bonzes[route] == self.HEIGHT[route]:
                return True
        return False

    def bonze_one_case_from_top(self):
        for bonze in self.bonzes:
            if self.HEIGHT[bonze] - self.bonzes[bonze] == 1:
                return True
        return False

    def score_stop(self):
        """
        Fonction de score se bassant sur la distance parcourue et sur le fait qu'il ai eu amélioration ou non.
        """
        score_bonze = 0
        if self.count_move == 0:
            score_bonze = 0
            return score_bonze
        elif self.count_jet > 3:
            score_stop = 1000
            return score_stop
        for way in self.bonzes:
            if way in self.pawn:
                dist_from_last_position = self.pawn[way] - self.bonzes[way]
            else:
                dist_from_last_position = self.bonzes[way]
            score_bonze += dist_from_last_position*self.score_to_stop_case[way]/self.HEIGHT[way]
            if self.bonzes[way] == self.HEIGHT[way]:
                score_bonze = 1000
        if self.bonze_one_case_from_top():
            score_bonze = 500
        return score_bonze