# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_rules.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Rules(object):
    def setupUi(self, Rules):
        Rules.setObjectName(_fromUtf8("Rules"))
        Rules.setWindowModality(QtCore.Qt.NonModal)
        Rules.setEnabled(True)
        Rules.resize(700, 700)
        Rules.setMinimumSize(QtCore.QSize(700, 700))
        Rules.setMaximumSize(QtCore.QSize(700, 700))
        Rules.setModal(False)
        self.gridLayout = QtGui.QGridLayout(Rules)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        spacerItem = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem, 0, 0, 1, 1)
        self.label = QtGui.QLabel(Rules)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Garamond"))
        self.label.setFont(font)
        self.label.setObjectName(_fromUtf8("label"))
        self.gridLayout.addWidget(self.label, 0, 1, 1, 1)
        self.pushButton = QtGui.QPushButton(Rules)
        self.pushButton.setObjectName(_fromUtf8("pushButton"))
        self.gridLayout.addWidget(self.pushButton, 1, 1, 1, 1)
        spacerItem1 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem1, 0, 2, 1, 1)

        self.retranslateUi(Rules)
        QtCore.QObject.connect(self.pushButton, QtCore.SIGNAL(_fromUtf8("clicked()")), Rules.close)
        QtCore.QMetaObject.connectSlotsByName(Rules)

    def retranslateUi(self, Rules):
        Rules.setWindowTitle(_translate("Rules", "Dialog", None))
        self.label.setText(_translate("Rules", "TextLabel", None))
        self.pushButton.setText(_translate("Rules", "Ok", None))

