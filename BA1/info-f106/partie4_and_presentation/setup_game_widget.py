# !/usr/bin/python3
# -*- coding: utf-8 -*-
from PyQt4 import QtGui, QtCore
from ui_setup_game import Ui_SetupGame

class SetupGame(QtGui.QWidget, Ui_SetupGame):
    """
    Classe affichant le menu de sélection des joueurs.
    """

    #Initialisation de signal(pyqtSignal) qui seront envoyé lors que 
    #des actions spéciale seront effectué tant par l'humain que par l'IA
    #Ces signaux seront connecter à des slot(fonction qui permet de gèrer ces signaux).
    showGUI = QtCore.pyqtSignal(list)
    def __init__(self, parent=None):
        """
        Fonction qui initialise une instance de la classe lorsqu'elle est utilisé.
        """
        super(SetupGame, self).__init__(parent)
        self.setupUi(self)
        self.type_players = [-1, -1, -1, -1]
        self.connect_button()

    def connect_button(self):
        """
        Fonction qui  connecte le signaux des boutons à un slot(fonction) 
        qui sera effectué par le signal lorsque celui-ci est émit.
        """
        self.playButton.clicked.connect(self.play)
        self.player_1_button.toggled.connect(lambda:self.setup_player(self.player_1_button))
        self.player_2_button.toggled.connect(lambda:self.setup_player(self.player_2_button))
        self.player_3_button.toggled.connect(lambda:self.setup_player(self.player_3_button))
        self.player_4_button.toggled.connect(lambda:self.setup_player(self.player_4_button))
        self.computer_1_button.toggled.connect(lambda:self.setup_player(self.computer_1_button))
        self.computer_2_button.toggled.connect(lambda:self.setup_player(self.computer_2_button))
        self.computer_3_button.toggled.connect(lambda:self.setup_player(self.computer_3_button))
        self.computer_4_button.toggled.connect(lambda:self.setup_player(self.computer_4_button))
        self.none_1_button.toggled.connect(lambda:self.setup_player(self.none_1_button))
        self.none_2_button.toggled.connect(lambda:self.setup_player(self.none_2_button))
        self.none_3_button.toggled.connect(lambda:self.setup_player(self.none_3_button))
        self.none_4_button.toggled.connect(lambda:self.setup_player(self.none_4_button))
        self.level_AI_1.currentIndexChanged.connect(lambda:self.handle_level_AI(self.level_AI_1))
        self.level_AI_2.currentIndexChanged.connect(lambda:self.handle_level_AI(self.level_AI_2))
        self.level_AI_3.currentIndexChanged.connect(lambda:self.handle_level_AI(self.level_AI_3))
        self.level_AI_4.currentIndexChanged.connect(lambda:self.handle_level_AI(self.level_AI_4))

        

    def setup_player(self, button):
        """
        Fonction qui gère la sélection des joueurs, 
        lorsque le bouton donné en paramètre et cliqué.
        """
        button_name = button.objectName()
        if button.isChecked():
            if button_name == 'player_1_button':
                self.type_players[0] = "Human"
                self.level_AI_1.setEnabled(False)
            elif button_name == 'computer_1_button':
                self.type_players[0] = ["AI",0]
                self.level_AI_1.setEnabled(True)
            elif button_name == 'none_1_button':
                self.type_players[0] = None
                self.level_AI_1.setEnabled(False)
            elif button_name == 'player_2_button':
                self.type_players[1] = "Human"
                self.level_AI_2.setEnabled(False)
            elif button_name == 'computer_2_button':
                self.type_players[1] = ["AI",0]
                self.level_AI_2.setEnabled(True)
            elif button_name == 'none_2_button':
                self.type_players[1] = None
                self.level_AI_2.setEnabled(False)
            elif button_name == 'player_3_button':
                self.type_players[2] = "Human"
                self.level_AI_3.setEnabled(False)
            elif button_name == 'computer_3_button':
                self.type_players[2] = ["AI",0]
                self.level_AI_3.setEnabled(True)
            elif button_name == 'none_3_button':
                self.type_players[2] = None
                self.level_AI_3.setEnabled(False)
            elif button_name == 'player_4_button':
                self.type_players[3] = "Human"
                self.level_AI_4.setEnabled(False)
            elif button_name == 'computer_4_button':
                self.type_players[3] = ["AI",0]
                self.level_AI_4.setEnabled(True)
            elif button_name == 'none_4_button':
                self.type_players[3] = None
                self.level_AI_4.setEnabled(False)

    def handle_level_AI(self, button):
        button_name = button.objectName()
        if button_name == 'level_AI_1':
            self.type_players[0][1] = int(button.currentText())
        elif button_name == 'level_AI_2':
            self.type_players[1][1] = int(button.currentText())
        elif button_name == 'level_AI_3':
            self.type_players[2][1] = int(button.currentText())
        elif button_name == 'level_AI_4':
            self.type_players[3][1] = int(button.currentText())
             

    def play(self):
        """
        Fonction est appélé par le signal émit par le bouton play. 
        Si la sélection est correct, un signal est émis 
        pour faire apparaitre le plateau de jeu et permettre au 1er joueur de jouer.
        """
        valid = self.is_valid()
        if not valid:
            self.alert_popup()
        else:
            self.remove_unplayer()
            self.showGUI.emit(self.type_players)

    def is_valid(self):
        """
        Fonction vérifiant si la sélection de joueurs est correct.
        """
        count_valid = 0
        valid = True
        for type_player in self.type_players:
            if type_player is not None and type_player != -1:
                count_valid += 1
        if count_valid <= 1:
            valid = False
        return valid

    def remove_unplayer(self):
        """
        Fonction qui enleve tout les valeurs qui ne sont pas Humain ou IA de la sélection de joueur.
        """
        temp_type_players = []
        for type_player in self.type_players:
            if type_player is not None and type_player != -1:
                temp_type_players.append(type_player)
        self.type_players = temp_type_players

    def alert_popup(self):
        message = """ Not the right number of Players / AI!
Please select at least 2 Players / AI.
"""
        QtGui.QMessageBox.warning(self, "Warning",message)
