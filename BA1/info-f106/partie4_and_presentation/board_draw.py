# !/usr/bin/python3
# -*- coding: utf-8 -*-
from PyQt4 import QtGui, QtCore


class Board(QtGui.QGraphicsScene):
    """
    Classe qui contient ce qui faut pour instancier le plateau de jeu et aussi ce qui faut pour le mettre à jour.
    """
    def __init__(self, parent=None):
        """
        Fonction qui initialise une instance de la classe lorsqu'elle est utilisé.
        """
        super(Board, self).__init__(parent) #Appel au constructeur parent.
        #variable représentant les voies et qui contiendra une liste de rectangles
        self.columns = [] 
        self.draw()


    def draw(self):
        """
        Fonction dessinant le plateau de jeu.
        """
        background_color = QtGui.QBrush(QtGui.QColor(190, 190, 190), QtCore.Qt.SolidPattern)
        labels = ["C", "A", "N", "T", "", "medal_icon.png", "S", "T", "O", "P", "!"]
        for i in range(11):
            if i < 6:
                nbre_cases = (2 * i) + 3 + 2 # Ajout 2 rectangles pour du texte

            else:
                nbre_cases = 11 - 2 * (i - 6) + 2 # Ajout 2 rectangles pour du texte

            rects = []
            for j in range(nbre_cases):
                #Formule permettant de créer chaque rectangle formant les voies
                rect = QtGui.QGraphicsRectItem(75 * i, 420 - j * 30, 50, 30)
                rects.append(rect)
                self.addItem(rect)
            self.columns.append(rects)
            text = QtGui.QGraphicsTextItem(str(i + 2))
            font = QtGui.QFont("Helvetica")
            font.setWeight(50)
            text.setFont(font)
            self.place_item(self.columns[i][0], text)
            if i == 5:
                picture = QtGui.QGraphicsPixmapItem(QtGui.QPixmap(labels[i]))
                self.place_item(self.columns[i][nbre_cases - 1], picture)
            else:
                text = QtGui.QGraphicsTextItem(labels[i])
                text.setFont(font)
                self.place_item(self.columns[i][nbre_cases - 1], text)
        #Change la couleur du fond.
        background_color = QtGui.QBrush(QtGui.QColor(229, 229, 229), QtCore.Qt.SolidPattern)
        self.setBackgroundBrush(background_color)

    def place_item(self, container, item):
        """
        Fonction permettant de centrer un objet(item) dans un autre objet(container).
        """
        rect_item = item.boundingRect()
        height_item = rect_item.height()
        width_item = rect_item.width()
        rect_container = container.boundingRect()
        x_text = rect_container.x() + (rect_container.width() - width_item) / 2
        y_text = rect_container.y() + (rect_container.height() - height_item) / 2
        item.setPos(x_text, y_text) # Modifie la position de l'objet(item)
        self.addItem(item)# Ajout de l'objet à la scene.

    def updateBoard(self, height, bonzes, pawns_pos, player_id):
        """
        Fonction permettant de rafraîchir 
        l'affichage suite aux actions du joueur ou du jeu.
        height: dictionnaire contenant les hauteurs de voies.
        bonzes: dictionnaire contenant les bonzes du joueur qui sont sur les voies.
        pwans_pos: dictionnaire contenant les positions des pions en clés 
                   et comme valeur le id du joueur.
        player_id: entier permettant d'identifier le joueur jouant.
        """
        self.clear()
        self.columns = []
        self.draw()
        color_player = ("red", "yellow","green","blue")
        for line_number in range(14):
            for way in height:
                pawn_icon = QtGui.QGraphicsPixmapItem(QtGui.QPixmap("pawn.png"))
                bonze_icon = QtGui.QGraphicsPixmapItem(QtGui.QPixmap("bonze.png"))
                if way in bonzes and bonzes[way] == line_number:
                    self.place_item(self.columns[way-2][line_number], bonze_icon)
                elif (way, line_number) in pawns_pos and pawns_pos[(way, line_number)] != player_id:
                    player_id_pawn = pawns_pos[(way, line_number)]
                    self.place_item(self.columns[way-2][line_number], pawn_icon)
                    background_color_pawn = QtGui.QBrush(QtGui.QColor(color_player[player_id_pawn]), QtCore.Qt.SolidPattern)
                    self.columns[way-2][line_number].setBrush(background_color_pawn)
                elif (way, line_number) in pawns_pos and way not in bonzes:
                    player_id_pawn = pawns_pos[(way, line_number)]
                    self.place_item(self.columns[way-2][line_number], pawn_icon)
                    background_color_pawn = QtGui.QBrush(QtGui.QColor(color_player[player_id_pawn]), QtCore.Qt.SolidPattern)
                    self.columns[way-2][line_number].setBrush(background_color_pawn)
