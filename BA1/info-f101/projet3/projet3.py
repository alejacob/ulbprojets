"""
INFO-F101
Titre : Projet 3 " Gestion d'agenda"
Auteur : JACOBS ALEXANDRE
Matricule : 000408850
Groupe TP : 5
Assistant TP groupe 5 : Cédric Ternon.
Date : 27/11/2015
"""

import codecs
import sys

search_criteria = sys.argv[3:] #sys.argv [] permet de retirer. 
                            #les critère de recherche de la ligne de commande.

fileIN = sys.argv[1] #retire de la ligne commande, le nom du fichier d'entrée.
fileOUT = sys.argv[2] #retire de la ligne de commande, le nom du fichier de sortie
fd = codecs.open(fileIN, "r", "utf-8") # codecs.open permet d'ouvrir
                                       # le fichier d'entrée en mode lecture.

line = fd.readline() # variable qui a chaque line lu du fichier fd.
head = "" # variable crée pour contenir le début du fichier d'entrée. 
transcript = ''# variable crée pour contenir ce qui doit 
            #être recopié dans le fichier de sortie.

"""
Cet fonction permet de créer une liste de dictionnaires
à partir de search criteria. Et si le dictionnaire contient les mots Summary 
et Location ceux-ci sont mis en majuscules grâce à la méthode upper. 
"""


def transformation_search_criteria_to_dico(search):
    dictionnary = []
    for elem in search: # Parcourt chaque element du paramètre search. 
        elem = elem.replace(" ", "")  # permet de supprimer les esspaces. 
        dico = {}
        elem = elem.split("and") # permet d'enlever les and qui sont dans les string et crée une liste de string.
        for word in elem:
            j = word.find(":") # find permet de trouver la carectère ":" et de retenir l'indice où se trouve l'élément.
            key = word[:j] # place le mot avant ":" comme clée.
            value = word[j+1:] # place le mot après ":" comme valeur.
            if key == "Summary" or key == "Location":
                key = key.upper()
            dico[key] = value # place la valeur à l'emplacement de la clée dans le dictionnaire.
        dictionnary.append(dico)  # rajoute le dico à la liste dictionnary.
    return dictionnary

"""
Cet fonction permet aussi de créer un dictionnaire
à partir d'une liste d'évément du fichier d'entrée. Et si une des clées est 
le mot "DESCRIPTION" celle ci est ignorée et non mise dans le dictionnaire. 
"""

def transformation_event_to_dico(search):
    dico = {}
    for elem in search:
        j = elem.find(":")
        key = elem[:j]
        value = elem[j+1:]
        if key == 'DESCRIPTION':
            k = elem[j+1:].find(":")
            key = elem[j+1:][:k+1] 
            value = elem[j+1:][k+1:]
        dico[key] = value
    return dico

"""
Cet fonction permet de comparer les deux paramètre qui sont 2 dictionnaire
pour voir si ceux-ci sont identiques. Si ils sont tous deux identiques
cela renverra true et permettra d'écrire l'événement dans le fichier de sortie
grâce la fonction writing_in_file.
"""

def comparison(criteriaIN, criteriaOUT):
    flag=True #compteur booléen permettant de voir si les 2 dictionnaires sont semblables.
    dico_search_criteria = transformation_search_criteria_to_dico(criteriaIN)
    dico_event = transformation_event_to_dico(criteriaOUT)
    for keys in dico_search_criteria:
        for key in keys:
            if keys[key] not in dico_event[key]:# compare la valeur de la clée du dico_search_criteria se trouve à celle dans dico_event.
                flag=False# mis à False si la valeur de la clé ne 
                            # se trouve pas dans dico_event.

        res=flag
    return res

"""
Cet fonction permet l'écriture du paramètre dans le fichier de sortie. 
"""

def writing_in_file(file, transcription1, transcription2, transcription3):
    file = open(file, "w") # ouvre le fichier de sortie en mode écriture.
    file.write(transcription1) 
    file.write(transcription2)
    file.write(transcription3)
    file.close()

# définit l'en-tête du fichier qui est mis dans la variable head.
while (line != "BEGIN:VEVENT\r\n"):
    head += line
    line = fd.readline()

# définit les événements dans une liste d'événements.
while (line != "END:VCALENDAR\r\n"):
    newEvent = line 
    listEvent = line.replace(" ", "") 
    listEvent = listEvent.strip().split("\\n") # crée une liste d'événement contenant des strings.
    line = fd.readline()
    NewEvent = [] # crée pour contenir un événement sous forme de liste.

    while (line != "END:VEVENT\r\n"):
        newEvent += line
        NewEvent += listEvent
        line = fd.readline()
        listEvent = line.replace(" ", "")
        listEvent = listEvent.strip().split("\\n")

    newEvent += line # variable qui contient un événement sous forme de string.
    NewEvent += listEvent # contient un événement sous forme de liste.
    
    if comparison(search_criteria, NewEvent[4:]): # Compare search_criteria par
    # rapport à NewEvent à partir de l'indce 4 jusqu'à lin fin de cette liste.

            transcript += newEvent # ajoute à transcript, l'événement 
                                   # si comparaison renvoie true.
    line = fd.readline()
    writing_in_file(fileOUT, head, transcript, line) # écrit dans le fichier de sortie.
fd.close() 
