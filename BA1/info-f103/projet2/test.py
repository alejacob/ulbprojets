#!/usr/bin/python3
#-*- coding: utf-8 -*-

from projet2 import Gare, ReseauFerroviaire, trouverParcoursMin

# Réseau de Bruxelles
charleroi = Gare("Charleroi")
arlon = Gare("Arlon")
mons = Gare("Mons")
londres = Gare("Londres")
paris = Gare("Paris")
florence = Gare("Florence")
lyon = Gare("Lyon")
orleans = Gare("Orleans")
bourges = Gare("Bourges")
geneve = Gare("Genève")
milan = Gare("Milan")
liege = Gare("Liege")
lille = Gare("Lille")
bruxelles = Gare("Bxl")
spa = Gare("Spa")
namur = Gare("Namur")
namur.ajouter_gare(charleroi, 50)
namur.ajouter_gare(arlon, 128)
charleroi.ajouter_gare(mons, 50)
lille.ajouter_gare(londres, 292)
lille.ajouter_gare(paris, 223)
paris.ajouter_gare(lyon, 465)
paris.ajouter_gare(orleans, 129)
paris.ajouter_gare(bourges, 245)
paris.ajouter_gare(florence, 490)
lyon.ajouter_gare(geneve, 150)
lyon.ajouter_gare(milan, 245)
liege.ajouter_gare(spa, 39)
liege.ajouter_gare(namur, 65)
bruxelles.ajouter_gare(liege, 96)
bruxelles.ajouter_gare(lille, 120)
reseau_bruxelles = ReseauFerroviaire(bruxelles)

# Réseau de Rome
roma = Gare("Roma")
florence = Gare("Florence")
pisa = Gare("Pisa")
bologna = Gare("Bologna")
venice = Gare("Venice")
milan = Gare("Milan")
vicenza = Gare("Vicenza")
naples = Gare("Naples")
bari = Gare("Bari")
messina = Gare("Messina")
catania = Gare("Catania")
palermo = Gare("Palermo")
trapani = Gare("Trapani")
roma.ajouter_gare(florence, 278)
roma.ajouter_gare(naples, 225)
florence.ajouter_gare(pisa, 82)
florence.ajouter_gare(bologna, 104)
bologna.ajouter_gare(venice, 145)
bologna.ajouter_gare(milan, 222)
venice.ajouter_gare(vicenza, 75)
naples.ajouter_gare(bari, 257)
naples.ajouter_gare(messina, 493)
messina.ajouter_gare(catania, 98)
messina.ajouter_gare(palermo, 224)
messina.ajouter_gare(trapani, 333)
reseau_roma = ReseauFerroviaire(roma)

# Réseau de Londres

london = Gare("London")
oxford = Gare("Oxford")
nottingham = Gare("Nottingham")
bristol = Gare("Bristol")
liverpool = Gare("Liverpool")
leeds = Gare("Leeds")
southampton = Gare("Southampton")
manchester = Gare("Manchester")
glasgow = Gare("Glasgow")
edinburgh = Gare("Edinburgh")
aberdeen = Gare("Aberdeen")
london.ajouter_gare(oxford, 134)
london.ajouter_gare(nottingham, 120)
oxford.ajouter_gare(bristol, 100)
nottingham.ajouter_gare(liverpool, 50)
nottingham.ajouter_gare(leeds, 200)
bristol.ajouter_gare(southampton, 60)
leeds.ajouter_gare(manchester, 150)
leeds.ajouter_gare(glasgow, 100)
leeds.ajouter_gare(edinburgh, 79)
edinburgh.ajouter_gare(aberdeen, 220)
reseau_london = ReseauFerroviaire(london)

# Réseau A intermédiaire
a1 = Gare("A1")
a2 = Gare("A2")
a3 = Gare("A3")
a4 = Gare("A4")
messina = Gare("Messina")
a6 = Gare("A6")
a7 = Gare("A7")
a8 = Gare("A8")
a9 = Gare("A9")
pisa = Gare("Pisa")
a11 = Gare("A11")
a12 = Gare("A12")
a13 = Gare("A13")
a1.ajouter_gare(a2, 278)
a1.ajouter_gare(a8, 225)
a2.ajouter_gare(a3, 82)
a2.ajouter_gare(a4, 104)
a4.ajouter_gare(messina, 145)
a4.ajouter_gare(a6, 222)
messina.ajouter_gare(a7, 75)
a8.ajouter_gare(a9, 257)
a8.ajouter_gare(pisa, 493)
pisa.ajouter_gare(a11, 98)
pisa.ajouter_gare(a12, 224)
pisa.ajouter_gare(a13, 333)
reseau_a = ReseauFerroviaire(a1)

# Test pour voir si cela fonctionne.


def test_gareAccessible():
    reseau_attendu = {london, oxford, nottingham, bristol, liverpool,
                      leeds, southampton, glasgow, manchester, edinburgh, aberdeen}
    return reseau_london.garesAccessibles(london) == reseau_attendu


def test_parcour():
    dest = [milan, mons]
    trajets_attendu = [[bruxelles, lille, paris, lyon, milan],
                       [bruxelles, liege, namur, charleroi, mons]]
    return trajets_attendu == reseau_bruxelles.trouverParcours(dest)


def test_trouverDistance():
    dest = [milan, trapani]
    distances_attendues = [278+104+222, 225+493+333]
    return reseau_roma.trouverDistance(dest) == distances_attendues


def test_trajetmin_un_reseau():
    trajet_attendu = [londres, lille, paris, orleans]
    return reseau_bruxelles.trouverParcoursMin(
        londres, orleans) == trajet_attendu


def test_trajetmin_deux_reseaux():
    # trajet_attendu = [bruxelles, lille, paris, lyon, milan, bologna,
    # florence, roma] # si simple connexion.
    trajet_attendu = [bruxelles, lille, paris, florence, roma]
    return trouverParcoursMin([reseau_bruxelles, reseau_roma], bruxelles, roma) == trajet_attendu


def test_trajetmin_trois_reseaux():
    trajet_attendu = [
        namur, liege, bruxelles, lille, paris, florence, pisa, a11]
    return trouverParcoursMin([reseau_bruxelles, reseau_roma, reseau_a],
                              namur, a11) == trajet_attendu


def test_trajetmin_4_reseaux():
    b1 = Gare("B1")
    pisa = Gare("Pisa")
    bruxelles.ajouter_gare(pisa, 1200)
    mons = Gare("Mons")
    b1.ajouter_gare(mons, 100)
    a9 = Gare("A9")
    b1.ajouter_gare(a9, 200)
    reseau_b = ReseauFerroviaire(b1)
    trajet_attendu = [namur, liege, bruxelles, pisa, a11]
    return trouverParcoursMin([reseau_bruxelles, reseau_roma, reseau_a, reseau_b],
                              namur, a11) == trajet_attendu

if __name__ == "__main__":
    all_test = test_parcour() and test_trouverDistance() and test_gareAccessible() and test_trajetmin_un_reseau(
    ) and test_trajetmin_deux_reseaux() and test_trajetmin_trois_reseaux() and test_trajetmin_4_reseaux()
    if all_test:
        print("Tous les tests ont été réussis :-) ")
    else:
        print("Un test ou plusieurs tests ne sont pas corrects :-(")
