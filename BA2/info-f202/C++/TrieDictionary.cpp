#include "TrieDictionary.hpp"


void TrieDictionary::insert(const std::string &word) {
    Trie::insert(word);
}

void TrieDictionary::erase(const std::string &word) {
    Trie::erase(word);
}

bool TrieDictionary::search(const std::string &word) const {
    return Trie::search(word);
}


TrieDictionary& TrieDictionary::operator+=(const Dictionary& other) {
    for(auto it_string = other.begin(); it_string != other.end(); ++it_string) {
        insert(*it_string);
    }
    return *this;
}

Dictionary::const_iterator TrieDictionary::begin() const {
    const_Iterator trie_iter = Trie::begin();
    const_iterator* iter = new const_iterator(trie_iter);
    return Dictionary::const_iterator(iter);
}

Dictionary::const_iterator TrieDictionary::end() const {
    const_Iterator trie_iter = Trie::end();
    const_iterator* iter = new const_iterator(trie_iter);
    return Dictionary::const_iterator(iter);
}