# Info-F106

Projet d'année, permettant d'apprendre et d'approfondir un premier langage. Plus amples informations voir [énoncé](./enonce.pdf).

Projet ayant obtenu une note totale de 15,5 après une défense de projet.

Pour exécuter le programme, exécuter la commande suivante python3 canstop.py . Commande marchant pour peut importe la partie.

# Outils Nécessaires

+ Python3
+ PyQt4