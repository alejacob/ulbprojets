/*
"""
INFO-F-105
Titre : Projet 1 ASM : Automate cellulaire simple
Auteur : JACOBS ALEXANDRE
Matricule : 000408850
Date : 25/04/2016
"""
*/


#include <iostream>
#include <cstring>

#define ALIVE_CELL_SYM "\u25A0"
#define DEAD_CELL_SYM "\u25A1"

void simulation_step(unsigned char[], unsigned char[]);
void print_world(unsigned char[]);
extern "C" {unsigned char simulation_cell(unsigned char,unsigned char,unsigned char,unsigned char[],unsigned char);}
const unsigned char RULE_SIZE=8;
const unsigned int WORLD_SIZE=8;

int main(){
  unsigned char world[WORLD_SIZE] = {0,1,0,0,1,0,1,1};
  unsigned char rule[RULE_SIZE] = {1,0,0,1,0,0,1,0};
  unsigned int simulation_steps = 10;

  for(unsigned int i=0; i < simulation_steps; i++){
    print_world(world);
    simulation_step(world,rule);
  }

} 

void print_world(unsigned char world[]){
  for(unsigned int i=0; i<WORLD_SIZE; i++){
     std::cout << (world[i] ? ALIVE_CELL_SYM : DEAD_CELL_SYM);
  }
  std::cout << std::endl;
}

void simulation_step(unsigned char world[], unsigned char rule[]){
  unsigned char left_neighbour, cell, right_neighbour;
  unsigned char world_bis[WORLD_SIZE];
  for(unsigned int i=0; i<WORLD_SIZE; i++){
      left_neighbour = world[ (i-1) % WORLD_SIZE] ; // permet de calculer le voisin à l'aide d'un modulo avec la taille de world.
      right_neighbour = world[ (i+1) % WORLD_SIZE];
      cell = world[i];
      world_bis[i] = simulation_cell(left_neighbour,cell,right_neighbour,rule,RULE_SIZE);

  }
  std::memcpy(world,world_bis,WORLD_SIZE);  // Permet de faire une deepcopy du vecteur world_bis dans world. 
}
