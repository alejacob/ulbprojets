# Info-F103
 
* [Devoir 1: Skip List](./devoir1/devoir1.pdf), petit devoir ayant obtenu la note de 10.

* [Devoir 2: La magie de l'encapsulation](./devoir2/enonce.pdf), petit devoir ayant obtenu une note de 8,5.

* [Projet 1: Planning de livraison](./projet1/EnonceProjet1.pdf), projet ayant obtenu une note de 9.

* [Projet 2: Reseau Ferroviaire](./projet2/ReseauFerroviaire.pdf), projet ayant obtenu une note de 8.

# Outils Nécessaires

+ Python3
