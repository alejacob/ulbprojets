;;INFO-F-105
;;Titre : Projet 1 ASM : Automate cellulaire simple
;;Auteur : JACOBS ALEXANDRE
;;Matricule : 000408850
;;Date : 25/04/2016


;; FUNCTION PROTOTYPE:
;; unsigned char simulation_cell_asm(unsigned char left_neighbour, unsigned char cell, unsigned_char right_neighbour, unsigned char rule[], unsigned char rule_size);

;; DESCRIPTION:
;; La fonction calcule l'etat suivant d'une cellule d'un automate cellulaire elementaire, etant donnes les valeurs des voisins et la regle de ;; mise à jour. 
;; La valeur du retour est stocke dans le registre EAX.
	
;; PARAMETRES:
;; 	- [EBP+8] - Valeur du voisin de gauche - 8 bit
;; 	- [EBP+12] - Valeur de la cellule - 8 bit
;; 	- [EBP+16] - Valeur du voisin de droit - 8 bit
;; 	- [EBP+20] - Adresse du vecteur rule - 32 bit
;; 	- [EBP+24] - Taille du vecteur rule - 8 bit 	

	
CPU 386
GLOBAL simulation_cell

; j'ai enlevé la SECTION .data car au donnée à initialisé en mémoire. 

SECTION .text
	
simulation_cell:
			PUSH EBP 								; Prologue de la fonction - Stockage du valeur de EBP dans la pile et copie du ESP dans EBP. 
															; (ESP pourrait être changé par des autre fonctions pendant l'execution de la fonction.) 
			MOV EBP, ESP						; N.B: [EBP+4] correspond à la vielle valeur de EIP donc il ne doit pas être lu.
			PUSH EDX								; Sauvegarde sur le stack pour éviter de détruire ce que contenait le registre avant l'appel à la fonction
			PUSH EBX
			PUSH ESI

			MOV EDX, [EBP+8]
			SHL EDX,1  							; shift à gauche pour permettre de faire un or avec la valeur de la cellule
			OR EDX,[EBP+12]					
			SHL EDX,1               ; shift à gauche pour permettre de faire un or avec la valeur du voisin 
			OR EDX,[EBP+16]					; cela permet d'obtenir par exemple 00000101, si voisin gauche =1, cellule=0 et voisin de droite = 1

			MOV EBX, [EBP+20]	
			MOV ESI, [EBP+24]

			; calcule l'adresse ou se trouve l'état suivant de la cellule
			SUB ESI,EDX							; comme les indices du vecteur rule vont de 0 à taille du vecteur - 1 et que le règle admet n possibilité = taille du vecteur
															; allant de la reglè de taille vecteur - 1  à 0. 
															; On peut en déduire la relation mathématique suivante que indices du vecteur est égale à (taille du vecteur - 1) - le nombre contenu dans EDX 						
			SUB ESI, 1
			MOV EAX,[EBX+ESI]				; mets la valeur trouvé à l'indice ESI du vecteur rule dans le registre EAX pour être retourner.
			
end:				
						
			POP ESI									; restaure le registre à l'état qu'il était avant l'appel à la fonction. 
			POP EBX				
			POP EDX																		
			MOV ESP, EBP						; Epilogue de la fonction - Restaurer ESP et EBP aux valeurs anciennes
			POP EBP
			RET
