# Info-F105

* [Projet 1: Moyenne](./projet1/projet1_infof105.pdf) projet ayant obtenu une note binaire de 1.

* [Projet 2: Ultra Light Blockcipher ](./projet2/projet_asm1.pdf) projet ayant obtenu une note de 8.

* [Projet 3: Simulation Cell ](./projet3/projet_asm2.pdf) projet ayant obtenu une note de 8.

* [Projet 4: ADT Bank](./projet4/projet_adt.pdf) projet ayant obtenu une note de 9,5.

# Outils Nécessaires

+ GCC/G++ version 6
+ Make
+ Nasm