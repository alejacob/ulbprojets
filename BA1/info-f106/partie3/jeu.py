# !/usr/bin/python3
# -*- coding: utf-8 -*-
from joueur import *
from random import randint


class Jeu():
    """
    Classe qui contient tout les fonctions qui sont nécessaires aux 
    """
    def __init__(self):
        self.HEIGHT = {2: 3, 3: 5, 4: 7, 5: 9, 6: 11, 7: 13, 8: 11, 9: 9, 10: 7, 11: 5, 12: 3}
        
        self.bonzes = dict()
        self.blocked_routes = set()
        self.old_blocked_routes = set()
        self.res_dices = tuple()

    def setup_player(self, player_type):
        """
        Fonction qui initialise chaque joueur.
        """
        self.pawns = [{} for i in range(len(player_type))]
        self.player = []
        for i, type in enumerate(player_type):
            self.player.append(Joueur(i, type))

    def get_pawns_pos(self):
        """
        Fonction créant un dictionnaire ayant comme clés (way, HEIGHT)
        et comme valeur l'indice du joueur à qui appartient ce pion.
        Le but de cet fonction est d'aider lors de l'affichage du plateau
        pour savoir si un pion est présent sur une voie et une hauteur donné.
        """
        pawns_pos = dict()
        for player_id, pawn in enumerate(self.pawns):
            for way, height in pawn.items():
                pawns_pos[(way, height)] = player_id
        return pawns_pos

    def clear_bonzes(self):
        """
        Fonction réinitialisant le dictionnaire bonzes à un dictionnaire vide.
        Cette fonction renvoie None par défaut.
        """
        if self.bonzes:
            self.bonzes.clear()

    def save_bonzes(self, player_id):
        """
        Fonction permettant de sauver la progression d'un joueur.
        """
        if self.bonzes:
            for route in self.bonzes:
                self.pawns[player_id][route] = self.bonzes[route]
            self.clear_bonzes()

    def throw_dice(self):
        """
        Fonction permettant de simuler le jet de dé en créeant des nombres aléatoires
        à l'aide de la fonction randint de la librairie random
        :return: un tuple de 4 entier correspondant au jet de 4 dés.
        """
        r1 = randint(1, 6)
        r2 = randint(1, 6)
        r3 = randint(1, 6)
        r4 = randint(1, 6)
        self.res_dices = r1, r2, r3, r4

    def moves_bonzes(self, routes, player_id):
        res_bool = False
        # vérifie si aucune des voie est dans le dict de bonzes, si c'est un dernier bonzes a placé et si la voie n'est pas identique.
        if len(self.bonzes) == 2 and len(routes) == 2 and routes[1] not in self.blocked_routes and routes[0] not in self.blocked_routes \
            and all(route not in self.bonzes for route in routes) and routes[0] != routes[1]:
            if self.player[player_id].player_type == "AI":
                gamer_choose = self.player[player_id].choose_routes_AI(routes)
                if gamer_choose == 2:
                    routes = reversed(routes)
            else:
                routes.pop()
        for route in routes:
            flag = True
            if route not in self.blocked_routes:
                # La route n'est pas bloquée, différents cas possibles.
                if route in self.bonzes:
                    # Un bonzes est sur cette voie.
                    while flag:
                        # si pion adverse avance jusqu'à une case libre.
                        self.bonzes[route] += 1
                        if not self.pawn_at_height(route, self.bonzes[route]):
                            flag = False
                    res_bool = True
                    if self.bonzes[route] == self.HEIGHT[route]:
                        self.blocked_routes.add(route)
                elif len(self.bonzes) != 3:
                    if route in self.pawns[player_id]:
                        # le bonzes est mis sur le pion du joueur si un pion existait sur la voie.
                        self.bonzes[route] = self.pawns[player_id][route]
                        res_bool = True
                    elif route not in self.bonzes:
                        # routes pas encore dans bonzes
                        self.bonzes[route] = 0
                        while flag:
                            self.bonzes[route] += 1
                            if not self.pawn_at_height(route, self.bonzes[route]):
                                flag = False
                        if self.bonzes[route] == self.HEIGHT[route]:
                            self.blocked_routes.add(route)
                        res_bool = True
        return res_bool

    def pawn_at_height(self, route, height_bonze):
        """
        Fonction véifiant s'il y a déjà pion à la hauteur HEIGHT et sur la voie route.
        """
        res_bool = False
        for pawn in self.pawns:
            if route in pawn and height_bonze == pawn[route]:
                res_bool = True
        return res_bool

    def check_top(self, player_id):
        """Fonction qui renvoie True si les trois bonzes/pions sont chacun au sommet d’une voie, False sinon."""
        res_bool = False
        count = 0
        pawn = self.pawns[player_id]
        for route in pawn:
            if pawn[route] == self.HEIGHT[route]:
                count += 1
                if count == 3:
                    res_bool = True
        return res_bool

    def combination_2(self, iterable):
        """Retourne toutes les combinaisons de 2 éléments parmis les itérables"""
        combinations = []
        for idx, first in enumerate(iterable[:-1]):
            for second in iterable[idx + 1:]:
                combinations.append((first, second))
        return combinations

    def is_blocked(self):
        """
        Permet de voir si le joueur après un jet de dé serait bloqué
        si toutes les combinaisons ne permettent pas d'avancer/placer un bonzes.
        Return True si bloqué, sinon False.
        """
        res_bool = True
        if len(self.bonzes) == 3:
            test = lambda route: route in self.bonzes and route not in self.blocked_routes
        else:
            test = lambda route: route not in self.blocked_routes
            # lambda permet de créer des fonctions anonymes,
            # dont on l'évalue avec test,
            # pour plus amples information voir doc python.

        for routes in self.combination_2(self.res_dices):
            dice_1, dice_2 = routes
            route = dice_1 + dice_2
            if test(route):
                res_bool = False
        return res_bool

    def all_possibility(self):
        """
        Fonction qui évalue tout les possibilité possible par rapport au lancé de dé.
        """
        possibility = []
        if len(self.bonzes) == 3:
            test = lambda route: route in self.bonzes and route not in self.blocked_routes
        else:
            test = lambda route: route not in self.blocked_routes
        for routes in self.combination_2(self.res_dices):
            dice_1, dice_2 = routes
            route = dice_1 + dice_2
            if test(route):
                possibility.append(route)
        return possibility


    def remove_pawns(self, routes, player_id):
        """
        Fonction qui permet d'enlever les pions des autres joueurs lorsqu'un joueur ferme un voie.
        """
        for route in routes:
            for idx, pawn in enumerate(self.pawns):
                if idx != player_id and route in pawn:
                    del pawn[route]
