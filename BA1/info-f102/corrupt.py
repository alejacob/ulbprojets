"""
INFO-F102
Titre : Titre : Projet Fonctionnement des ordinateurs.
Auteur : JACOBS ALEXANDRE
Matricule : 000408850
Date : 18/12/2015
"""
"""
Tous ce qui suit permet de retirer ce qui est entrée dans la ligne de 
commande par l'utilisateur, lors du lancement du programme. 
"""
import random
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("file")
parser.add_argument('-o', '--output', default='corrupted.out', help='-o, Output, \
    permet de  spécifier le nom du fichier de sortie.\
    Par défaut le fichier sera corrupted.out')
parser.add_argument('-b', '--bytes', default='0.2', help="Cela permet de spécifier \
    une probabilité de corruption de un octet, la valeur par défaut est 0.2.")
parser.add_argument('-p', '--probability', default='0.1', help="Cela permet de spécifier \
    une probabilité de corruptionun bit de plus par octet.\
    La valeur par défaut est 0.2.")
args = parser.parse_args()
fileIn = args.file
fileOut = args.output
P = float(args.probability)
B = float(args.bytes)


def read(file):
    """
    Cette fonction permet de lire le fichier en mode binaire et de renvoyer
    pour chaque caractère de celui-ci un nombre binaire sur 8 bit soit un byte.
    """

    f = open(file, 'rb')
    octets = f.readlines()[0]
    Bytes = []
    for octet in octets:
        Bytes.append(("0"*8 + bin(octet)[2:])[-8:])
    return Bytes


def written(file, param):
    """
    Cette fonction permet d'écrire dans un fichier un parèmetre en mode 'wb'.
    """

    f = open(file, 'wb')
    f.write(bytes(param))
    f.close()


def decode_binary_to_int(b):
    """
    Cette fonction permet de décoder liste de nombres binaire en une liste d'entier,
    grâce à la méthode int(x,2) qui transforme le string d'un nombre binaire en entier.
    """

    res = []
    for elem in b:
        res.append(int(elem, 2))
    return res


def corrupt(fileIn, fileOut, B, P):
    """
    Cette fonction permet de corrompre les bytes d'un fichier d'entrée après lecture
    de celui-ci par rapport à deux probilités de corruption. Après corruption de 
    chaque byte, cela est écris dans un fichier de sortie grâce aux fonctions 
    decode_binary_to_int() et written(). Cela renvoie aussi un un message à l'utilisateur
    à la fin pour informer sur le nombre de bytes corrompu et 
    le nombre d'erreur pour chaque bytes.
    """

    Bytes = read(fileIn)
    bytes = []
    res = ''
    dico_erreur = {}
    compteur_nombre_erreur = 0
    for octet in Bytes:
        n = random.random()
        compteur_nombre_erreur = 0
        if n <= B:
            indice = random.randint(0, 7)
            compteur_nombre_erreur += 1
            if compteur_nombre_erreur in dico_erreur:
                dico_erreur[compteur_nombre_erreur] += 1
            else:
                dico_erreur[compteur_nombre_erreur] = 1
            if octet[indice] == '1':
                octet = octet[:indice]+"0"+octet[indice+1:]
            else:
                octet = octet[:indice]+"1"+octet[indice+1:]
            m = random.random()
            while m <= P:
                compteur_nombre_erreur += 1
                if compteur_nombre_erreur in dico_erreur:
                    dico_erreur[compteur_nombre_erreur] += 1
                else:
                    dico_erreur[compteur_nombre_erreur] = 1

                indice = random.randint(0, 7)
                if octet[indice] == '1':
                    octet = octet[:indice]+"0"+octet[indice+1:]
                else:
                    octet = octet[:indice]+"1"+octet[indice+1:]
                m = random.random()
        bytes.append(octet)
    res = decode_binary_to_int(bytes)
    written(fileOut, res)
    keys_errors = list(dico_erreur.keys())
    sum_corrupted_bytes = 0
    for key in keys_errors:
        sum_corrupted_bytes += dico_erreur[key]
    print("Corrupted bytes: ", str(sum_corrupted_bytes))
    for key in keys_errors:
        if key == 1:
            print(str(key), ' Error: ', str(dico_erreur[key]))
        else:
            print(str(key), ' Errors: ', str(dico_erreur[key]))

corrupt(fileIn, fileOut, B, P)
