"""
INFO-F101
Titre : Projet 4 " Résolution système d'équation"
Auteur : JACOBS ALEXANDRE
Matricule : 000408850
Groupe TP : 5
Assistant TP groupe 5 : Cédric Ternon.
Date : 11/12/2015
"""

def ppcm(x, y):
    """
    Cette fonction permet le calcul du PPCM 
    de deux nombres strictement positifs.
    """

    x = abs(x)
    y = abs(y)
    PPCM = x*y
    if x == 0 or y == 0:
        res = 0
    else:
        while x != y:
            if x > y:
                x = x-y
            else:
                y = y-x
        res = int(PPCM/x)
    return res


def compute_combili(equation1, equation2, inc_eliminer):
    """
    Cette fonction à l'aide de la fonction ppcm renvoie une combinaison linéaire
    entre deux équations (equation1 et equation2) pour éliminer 
    un inconnue (inc-eliminer). 
    """

    ppcm_eq = ppcm(equation1[inc_eliminer], equation2[inc_eliminer])
    c1 = ppcm_eq/(equation1[inc_eliminer])
    c2 = ppcm_eq/(equation2[inc_eliminer])
    cle_inconnue = list(equation1.keys())  # retire les inconnues de équation1
    diff_eq1_et_eq2 = {}.fromkeys(equation1, "")  # toutes mofifications du
    # paramètre equation1 ou equation2 mais sans leurs valeurs.

    for cle in cle_inconnue:
        C1 = c1*(equation1[cle])  # multiplie chaque coefficient par c1
        C2 = c2*(equation2[cle])  # (contante issus du ppcm et du coefficient
        # de l'inconnue à éliminer')

        if C1 != 0 and C2 != 0 and (abs(C1-C2))/(abs(C1)+abs(C2)) < 10**-5:
            # mis à zéro pour éviter des nombres trop proche de zéro
            C1 = C2 = 0
        diff_eq1_et_eq2[cle] = (C1 - C2)  # mets la diiférence des coefficient
        # des inconnues C1 et  C2 dan un dictionnaire
    return diff_eq1_et_eq2

def triangulation(system):
    """
    Cette fonction permet de renvoyer une version trinagulée 
    du système d'inconnu ou None si le le système est indéterminé ou impossible. 
    Soit Systriangulaire un système vide qui contiendra la version triangulée du
    système ou None. Le sytème triangulée est calculée gràce à une suite de 
    combinaisons linéaires entre les différentes équations et toutes 
    les inconnues du système en excluant le terme indépendant. 
    """

    Systriangulaire = []
    inconnues = list(system[0].keys())
    inconnues.remove("ti")
    compteur = True # crée pour savoir si le sytème est triangulée ou non triangulée.
    for inconnue in inconnues:
        E = []  # variable étant l'ensemble E.
        for equation in system:
            if equation[inconnue] != 0:
                E.append(equation)  # Ensemble contenant toutes
                # les equations où l'inconnue apparait.

        if len(E)!= 0:
            equation_pivot = E[0]  # fait pour calculer la combinaison linéaire.

            # parcourt le sytème à partir de l'indice 1.
            for equation in E[1:]:
                system.append(compute_combili(equation_pivot, equation, inconnue))
                system.remove(equation)  # supprime l'équation utilisé
                # et la remplace par une combinaison linéaire entre elle et le
                # equation_pivot.
                
            Systriangulaire.append(equation_pivot)
            system.remove(equation_pivot)
        else: 
            compteur = False  # mis à False si système impossible ou indéterminé.
            Systriangulaire = None
    if compteur :
        return Systriangulaire


def substitution(Systriangulaire):
    """
    Cette fonction prend en paramètre un système triangulée et renvoit 
    les solutions de celui sous forme d'un dictionnaire contenant comme 
    clée une inconnue et comme valeur le réusltat sous forme 
    de float(nombre à virgule) obtenu par la substitution.
    """

    resultat = {}
    # renverse sytriangulaire car la dernière équation

    Systriangulaire.reverse()
    # contient une seule inconnue.

    inconnues = list(Systriangulaire[0].keys())
    inconnues.remove('ti')
    for equation in Systriangulaire:
        for inconnue in inconnues:
            if equation[inconnue] != 0:
                equation[inconnue] = -equation['ti']/equation[inconnue]
                # reçoit la valeur de la division du terme indépendant par
                # le coefficent de l'inconnue à isoler

                resultat[inconnue] = equation[inconnue]  # ajoute le résultat
                # dans le dictionnaire contenant les solutions

                for Equation in Systriangulaire[1:]:
                    # met à jour le terme indépendant dans les autres équations
                    Equation['ti'] += resultat[inconnue]*Equation[inconnue]
                    Equation[inconnue] = 0
                    if Equation[inconnue] != 0:
                        Equation[inconnue] = -Equation['ti']/Equation[inconnue]
                        resultat[inconnue] = Equation[inconnue]
    return resultat

def print_equation(equation):
    """ 
    Cette fonction est faite pour imprimer correctement toutes les équations
    en évitant de mettre les coefficient égaux à 0 et 1 et 
    aussi en évitant les problèmes de signe tel que "+-".
    """

    res = []
    for inconnue in equation:
        if (equation[inconnue]) == 1 and inconnue != 'ti':
            res.append(inconnue)
        elif (equation[inconnue]) < 0 and int(equation[inconnue]) != -1 and inconnue != 'ti':
            res.append(str(equation[inconnue])+inconnue)
        elif (equation[inconnue]) == -1 and inconnue != 'ti':
            res.append("-"+inconnue)
        elif (equation[inconnue]) != 0 and inconnue != 'ti':
            res.append(str(equation[inconnue])+inconnue)
    if (equation["ti"]) != 0:
        res.append(str(equation['ti']))
    out = " + ".join(res)  # crée un string avec les valeurs de res
    # dont les jonctions entre éléments sont fait gràce au +.

    out = out.replace("+ -", "- ")  # remplace les signes"+-" par -
    # pour être plus clair dans l'affichage.
    return out

def print_system(system):
    """
    Cette fonction permet l'affichage des équations encodées sous forme de système.
    """

    print("Système :")
    for equation in system:
        print_eq = print_equation(equation)
        print(print_eq, "= 0")

def print_solution_system(solution):
    """
    Cette fonction permet l'affichage des solutions au système d'équations.
    """

    print("Solution :")
    for elem in solution:
        print(elem, "=", str(solution[elem]))

def encode_inconnues():
    """ 
    Cette focntion est crée pour permettre à l'utilisateur d'encoder 
    les différentes inconnues des équations du systèmes.
    Ceux ci sont renvoyé sous forme de liste. Les inconnues que 
    l'utilisateur doit entrée, doivent être des lettres 
    entre a et z séparées par un espace et n'apparaitre qu'une fois. 
    Si ce n'est pas le cas un message d'erreur s'affiche à l'utitlisateur
    pour corriger cela.
    """

    flag = 1  # varibale crée pour voir si l'encodage est correct
    while flag == 1:
        inconnues = input(
            "Encoder les inconnues (lettres entre a et z séparées par un espace)\n")
        inconnues = inconnues.split(" ")
        flag = 0
        for j in range(len(inconnues)):
            for inc in inconnues[j+1:]:
                if inc == inconnues[j]:
                    flag = 1
        for i in inconnues:
            if len(i) != 1:
                flag = 1
            else:
                if 97 >= ord(i) <= 122:
                    flag = 1
        if flag == 1:
            print(
                "Erreur : Toutes les inconnues doivent être des caractères entre ’a’ et ’z’ séparée par un espace et n’apparaître qu’une fois")
    inconnues.append('ti')
    return inconnues

def is_integer(coefficient):
    """ 
    Cette fonction est créer pour vérifier si les coefficients encodés 
    par l'utilisateur sont des nombres entier gràce à 
    des gestions d'exceptions ( try et except).
    """

    flag = True  # variable booléenne pour vérifier si c'est un nombre entier.
    try:
        int(coefficient)
        res = flag
    except ValueError:
        flag = False
        res = flag
    return res

def encode_coefficient(inconnues):
    """ 
    Cette focntion est crée pour permettre à l'utilisateur d'encoder 
    les différent coefficents des inconnues des équations du système 
    dans le même ordre que les inconnues.
    Ceux ci sont renvoyés sous forme de liste. Les coefficents que 
    l'utilisateur doit entrer, doivent être des nombres entiers 
    séparés par un espace.
    Si ce n'est pas le cas un message d'erreur s'affiche à l'utitlisateur
    pour corriger cela.
    """

    erreurs = True
    while erreurs:
        print("Coefficients de "+" ".join(inconnues) +
              " et du terme indépendant :")
        # join permet de joindre les nom des inconnue lors de la demande de
        # leurs coefficients

        coefficients = input()
        coefficients = coefficients.split(" ")
        erreurs = False   #  variable booloéenne pour vérifier l'encodage
        if len(coefficients) != len(inconnues)+1:
            erreurs = True
        for coeff in coefficients:
            if not is_integer(coeff):
                erreurs = True
        if erreurs:
            print("Erreur : Tous les coefficients doivent être des nombres "
                  "entiers séparer par un espace.")
    return coefficients

def encode_system():
    """ 
    Cette focntion est crée pour permettre à l'utilisateur d'encoder 
    les différentes inconnues et coefficients d'inconnues à l'aide 
    des focntions encode_inconnues() et encode_coefficient() 
    des équations du systèmes.
    Le système obtenue est renvoyée sous forme de liste contenant 
    les différentes équations sous formes de dictionnaire 
    dont les clées sont les inconnues et les valeurs les coeffcients.
    """

    inconnues = encode_inconnues()
    inconnue = inconnues[:]
    inconnue.remove('ti')
    system = []
    for i in range(len(inconnue)):
        dico = {}
        print("Equation", i+1, ":")
        coefficient = encode_coefficient(inconnue)
        for inc in range(len(inconnues)):
            dico[inconnues[inc]] = int(coefficient[inc])
        equation = print_equation(dico)
        print("Equation encodée:\n"+equation+" = 0")
        system.append(dico)
    return system

def solve_system():
    """
    cette fonction est créer pour résoudre le système d'équations encodé lors de l'appel à encode_system(). 
    Cela est réalisable gràce à l'appel d'autre d'autres fontions :
    - print_system pour l'affiche du système d'équations.
    - triangulation(system) pour trouver la version trianguler du système si elle existe. 
    - substitutio(Systriangulaire) pour trouver les solutions de chaque inconnues.
    - print_solution() pour l'affiche des solutions du système si le système est résolvable.
    Si le système est résolvable, la fonction renvoie les solutions. Sinon,
    la fonction renvoit None et un message disant que le système est impossible ou bien indéterminé.
    """
    
    system = encode_system()
    print_system(system)
    systriangulaire = triangulation(system)
    if systriangulaire != None:
        solution = substitution(systriangulaire)
        res = print_solution_system(solution)
    else:
        print("Système impossible ou bien indéterminé")
        res = None
    return res

if __name__ == '__main__':
    solve_system()
